import 'package:flutter/material.dart';

import '../screens/auth/view/loading_screen.dart';
import '../screens/auth/view/login_screen.dart';
import '../screens/auth/view/register_screen.dart';

import '../screens/chat/view/chat_list_inmueble_and_users/chat_users_inmueble_screen.dart';
import '../screens/chat/view/chat_my_immovable/chat_my_list.dart';
import '../screens/chat/view/options_chat/contract/chat_contract_message.dart';
import '../screens/chat/view/options_chat/contract/chat_enabled_contract_message.dart';
import '../screens/chat/view/options_chat/settings/chat_settings_message.dart';
import '../screens/home/view/home_screen.dart';
import '../screens/home/view/widgets/filters/filter_page_immovables.dart';
import '../screens/home/view/widgets/list_city.dart';
import '../screens/property_view/view/property_view._screen.dart';
import '../screens/property_view/view/widgets/photo_view_property_immovable.dart';
import 'app_routes.dart';

final routes = <String, WidgetBuilder>{
  /**
   * RUTAS AUTENTICACIONES
   */
  Routes.initial: (_) => LoadingScreen(),
  Routes.login: (_) => LoginScreen(),
  Routes.register: (_) => RegisterScreen(),
  /**
   * RUTAS DEL INICIO 
   */
  Routes.home: (_) => HomeScreen(),
  Routes.location_city: (_) => DetailCityListScreen(),
  Routes.property_immovable: (_) => PropertyViewScreen(),
  Routes.property_immovable_view_images: (_) => PropertyPhotoViewFullScreen(),
  Routes.filter_immovable: (_) => FilterImmovablesScreen(),
 /**
  * RUTAS DENTRO DEL MENU
  */
  Routes.profile_chat_list: (_) => ChatListScreen(),
  Routes.chat_list_users: (_) => ChatListUsersScreen(),

  /**
   * RUTAS DENTRO DEL CHAT
   */
  Routes.chat_settings_message: (_) => ChatSettingMessageScreen(),
  Routes.chat_contract_message: (_) => ChatContractMsgScreen(),
  Routes.chat_enabled_contract_message: (_) => ChatEnabledContractMsgScreen()
};
