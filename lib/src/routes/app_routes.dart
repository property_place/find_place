///Routes de las vistas princiaples de todas la app
abstract class Routes {
  static const String initial = '/';
  static const String login = '/login';
  static const String register = '/register';
  static const String home = '/home';
  static const String location_city = '/location_city';
  static const String property_immovable = '/property_immovable';
  static const String property_immovable_view_images ='/property_immovable_view_images';
  static const String filter_immovable = '/filter_immovable';
  static const String chat_immovable_view_init = '/chat_immovable_view_init';
  static const String profile_chat_list = '/profile_chat_list';
  static const String chat_list_users = '/chat_list_users';
  static const String chat_message = '/chat_message';
  static const String chat_settings_message = '/chat_settings_message';
  static const String chat_enabled_contract_message = '/chat_enabled_contract_message';
  static const String chat_contract_message = '/chat_contract_message';

}
