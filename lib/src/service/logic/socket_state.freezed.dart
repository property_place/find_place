// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'socket_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$SocketStateTearOff {
  const _$SocketStateTearOff();

  _SocketState call(
      {ServeStatusConnection serveStatus = ServeStatusConnection.Conneting}) {
    return _SocketState(
      serveStatus: serveStatus,
    );
  }
}

/// @nodoc
const $SocketState = _$SocketStateTearOff();

/// @nodoc
mixin _$SocketState {
  ServeStatusConnection get serveStatus => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SocketStateCopyWith<SocketState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SocketStateCopyWith<$Res> {
  factory $SocketStateCopyWith(
          SocketState value, $Res Function(SocketState) then) =
      _$SocketStateCopyWithImpl<$Res>;
  $Res call({ServeStatusConnection serveStatus});
}

/// @nodoc
class _$SocketStateCopyWithImpl<$Res> implements $SocketStateCopyWith<$Res> {
  _$SocketStateCopyWithImpl(this._value, this._then);

  final SocketState _value;
  // ignore: unused_field
  final $Res Function(SocketState) _then;

  @override
  $Res call({
    Object? serveStatus = freezed,
  }) {
    return _then(_value.copyWith(
      serveStatus: serveStatus == freezed
          ? _value.serveStatus
          : serveStatus // ignore: cast_nullable_to_non_nullable
              as ServeStatusConnection,
    ));
  }
}

/// @nodoc
abstract class _$SocketStateCopyWith<$Res>
    implements $SocketStateCopyWith<$Res> {
  factory _$SocketStateCopyWith(
          _SocketState value, $Res Function(_SocketState) then) =
      __$SocketStateCopyWithImpl<$Res>;
  @override
  $Res call({ServeStatusConnection serveStatus});
}

/// @nodoc
class __$SocketStateCopyWithImpl<$Res> extends _$SocketStateCopyWithImpl<$Res>
    implements _$SocketStateCopyWith<$Res> {
  __$SocketStateCopyWithImpl(
      _SocketState _value, $Res Function(_SocketState) _then)
      : super(_value, (v) => _then(v as _SocketState));

  @override
  _SocketState get _value => super._value as _SocketState;

  @override
  $Res call({
    Object? serveStatus = freezed,
  }) {
    return _then(_SocketState(
      serveStatus: serveStatus == freezed
          ? _value.serveStatus
          : serveStatus // ignore: cast_nullable_to_non_nullable
              as ServeStatusConnection,
    ));
  }
}

/// @nodoc

class _$_SocketState extends _SocketState {
  const _$_SocketState({this.serveStatus = ServeStatusConnection.Conneting})
      : super._();

  @JsonKey()
  @override
  final ServeStatusConnection serveStatus;

  @override
  String toString() {
    return 'SocketState(serveStatus: $serveStatus)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SocketState &&
            const DeepCollectionEquality()
                .equals(other.serveStatus, serveStatus));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(serveStatus));

  @JsonKey(ignore: true)
  @override
  _$SocketStateCopyWith<_SocketState> get copyWith =>
      __$SocketStateCopyWithImpl<_SocketState>(this, _$identity);
}

abstract class _SocketState extends SocketState {
  const factory _SocketState({ServeStatusConnection serveStatus}) =
      _$_SocketState;
  const _SocketState._() : super._();

  @override
  ServeStatusConnection get serveStatus;
  @override
  @JsonKey(ignore: true)
  _$SocketStateCopyWith<_SocketState> get copyWith =>
      throw _privateConstructorUsedError;
}
