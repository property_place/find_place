import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import '../../core/global/environment.dart';
import '../../screens/auth/logic/auth_provider.dart';
import 'socket_state.dart';
part 'socket_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************

final resolverSocketService = StateNotifierProvider<SocketService, SocketState>(
    (ref) => SocketService(ref: ref));
