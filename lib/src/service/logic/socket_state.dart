import 'package:freezed_annotation/freezed_annotation.dart';
import 'socket_provider.dart';

part 'socket_state.freezed.dart';

@freezed
abstract class SocketState with _$SocketState {
  const factory SocketState({
    @Default(ServeStatusConnection.Conneting) ServeStatusConnection serveStatus,
  }) = _SocketState;

  const SocketState._();
}
