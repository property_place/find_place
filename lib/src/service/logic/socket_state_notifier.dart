part of 'socket_provider.dart';

enum ServeStatusConnection { Online, Offine, Conneting }

class SocketService extends StateNotifier<SocketState> {
  SocketService({required Ref ref})
      : _ref = ref,
        super(SocketState());

  final Ref _ref;
   IO.Socket? _socket;

  IO.Socket? get socket => _socket;
  Function get emit => _socket!.emit;

  void connect() async {
    final _token =  await _ref.read(resolverAuthService.notifier).getToken();

    _socket = IO.io(EnvironmentApi.linkApiSocket, <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': true,
      'forceNew': true,
      'extraHeaders': {'x-token': _token}
    });

    _socket?.onConnect((_) {
      state = state.copyWith(serveStatus: ServeStatusConnection.Online);
      print("Connection TRUE");
    });

    _socket?.onConnectError((data) {
      print(EnvironmentApi.linkApiSocket);
      print("Connection Error $data");
    });

    _socket?.onDisconnect((_) {
      state = state.copyWith(serveStatus: ServeStatusConnection.Offine);
    });
  }

  void disconnect() {
    _socket?.disconnect();
  }
}
