import 'package:flutter/material.dart';

import 'routes/app_acreens.dart';
import 'routes/app_routes.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Place',
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.initial,
      routes: routes,
    );
  }
}
