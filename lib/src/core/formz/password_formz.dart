import 'package:formz/formz.dart';

enum PasswordValidationError { empty }

class PasswordFz extends FormzInput<String, PasswordValidationError> {
  const PasswordFz.pure() : super.pure('');
  const PasswordFz.dirty(String value) : super.pure(value);

  @override
  PasswordValidationError? validator(String value) {
    if (value.isEmpty) return PasswordValidationError.empty;
    return null;
  }
}
