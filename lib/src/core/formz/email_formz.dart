import 'package:formz/formz.dart';

enum EmailValidationError { empty, invalid }

class EmailFz extends FormzInput<String, EmailValidationError> {
  const EmailFz.pure() : super.pure('');
  const EmailFz.dirty(String value) : super.pure(value);

  static final RegExp _emailRegExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  @override
  EmailValidationError? validator(String value) {
    if (value.isEmpty) return EmailValidationError.empty;
    if (!_emailRegExp.hasMatch(value)) return EmailValidationError.invalid;
    return null;
  }
}
