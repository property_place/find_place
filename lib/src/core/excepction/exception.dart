import '../../screens/auth/data/models/user_model.dart';

class CustomExceptionResponse implements Exception {
  const CustomExceptionResponse({required this.ok, required this.error});
  final ErrorModel error;
  final bool ok;
}
