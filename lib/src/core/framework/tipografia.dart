
import 'package:flutter/material.dart';
/// estilo normal de la app styleTituloNormal
// ignore: lines_longer_than_80_chars
const styleTituloNormal = TextStyle(fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Poppins');
/// estilo normal de la app styleTituloBold con negrita de 300
// ignore: lines_longer_than_80_chars
const styleTituloW300 = TextStyle(fontSize: 15, fontWeight: FontWeight.w300, fontFamily: 'PoppinsBold');
/// estilo normal de la app styleTituloItalic con negrita de 100
// ignore: lines_longer_than_80_chars
const styleTituloW100 = TextStyle(fontSize: 15, fontWeight: FontWeight.w100, fontFamily: 'PoppinsItalic');
/// estilo normal de la app styleTituloBold con negrita de 600
// ignore: lines_longer_than_80_chars
const styleTituloW600 = TextStyle(fontSize: 15, fontWeight: FontWeight.w600, fontFamily: 'PoppinsBold');
/// estilo normal de la app styleTituloItalic con negrita de 600
// ignore: lines_longer_than_80_chars
const styleTituloItalicW600 = TextStyle(fontSize: 15, fontWeight: FontWeight.w600, fontFamily: 'PoppinsItalic');