
import 'package:flutter/material.dart';
///Style
const kTitleStyle =  TextStyle(
  color: Colors.white,
  fontFamily: 'CM Sans Serif',
  fontSize: 26,
  height: 1.5,
);

///
const kSubtitleStyle = TextStyle(
  color: Colors.white,
  fontSize: 18,
  height: 1.2,
);