import 'package:flutter/material.dart';

// User Interface and Theming
/// Color primario
const primary = Color(0xFFdadee2);

const secundary = Color(0xFF71A054);

const secundary_orange = Color(0xFFEEAE2A);

/// Color secundario
const oscuro = Color(0xFF020202);

const oscuro_appbar = Color(0xFF333333);

/// Color claro
const claro = Color(0xFFFFFFFF);

const grey = Color(0xFF9E9E9E);

const colorDisabled = Color.fromRGBO(142, 142, 147, 1.2);

const activeTab = Color(0xFFEEAE2A);

// Status Inmuebles
const rentedState = Colors.red;
const activeState = Color(0xFF71A054);
const stopState = Color(0xFFEEAE2A);

// Status chat
const chatPersonActive = Color(0xFF02cd58);
const chatPersonInactive = Color.fromARGB(255, 255, 113, 103);

//
const coloPpaginationChat = Color(0xFFEEAE2A);

/* Colores extra */
/// Color de snabar predefinido
int colorShowSnackBar = 4281348144;
