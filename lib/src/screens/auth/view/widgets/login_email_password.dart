import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../routes/app_routes.dart';
import '../../../../service/logic/socket_provider.dart';
import '../../../widgets/view_alertas.dart';
import '../../logic/auth_provider.dart';
import '../../logic/auth_state.dart';

class FormLoginEmailPassword extends ConsumerWidget {
  FormLoginEmailPassword({
    Key? key,
  }) : super(key: key);

  final emailCRT = TextEditingController();
  final passCRT = TextEditingController();
  final viewAlert = ViewAlert();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<AuthState>(resolverAuthService, (oldAuthNotifier, newAuthNotifier) {
      if (newAuthNotifier.authenticated != null &&
          newAuthNotifier.authenticated!) {
        ref.read(resolverSocketService.notifier).connect();
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.home, (route) => false);
      } else if (newAuthNotifier.isError != '') {
        viewAlert.viewAlert(context, newAuthNotifier.isError);
      }
    });
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Form(
          child: Column(
            children: [
              __TextFormFielEmail(emailCRT: emailCRT),
              SizedBox(height: 20),
              _TextFormFielPassword(passCRT: passCRT),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 30),
                alignment: Alignment.centerRight,
                child: Text('Recuperación de contraseña'),
              ),
              Consumer(
                builder: (context, ref, child) {
                  final authenticating = ref.watch(authenticatingProvider);
                  return _ButtonLogin(
                    onPressed: !authenticating
                        ? () => _hanledSubmit(context, ref: ref)
                        : null,
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void _hanledSubmit(BuildContext context, {required WidgetRef ref}) {
    FocusScope.of(context).unfocus();
    ref.read(resolverAuthService.notifier).login(
          email: emailCRT.text.trim(),
          password: passCRT.text.trim(),
        );
  }
}

class __TextFormFielEmail extends ConsumerWidget {
  const __TextFormFielEmail({
    Key? key,
    required this.emailCRT,
  }) : super(key: key);

  final TextEditingController emailCRT;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return _InputForm(
      hintText: 'Correo electronico',
      controller: emailCRT,
    );
  }
}

class _TextFormFielPassword extends HookWidget {
  const _TextFormFielPassword({
    Key? key,
    required this.passCRT,
  }) : super(key: key);

  final TextEditingController passCRT;

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        final obscureText = ref.watch(obscureTextPasswordProvider);
        return _InputForm(
          hintText: 'Contraseña',
          controller: passCRT,
          iconActive: true,
          obscureText: obscureText,
          obscureTextButton: IconButton(
              onPressed: () => ref
                  .read(obscureTextPasswordProvider.state)
                  .state = !obscureText,
              icon: Icon(
                !obscureText ? Ionicons.eye_off_outline : Ionicons.eye_outline,
                color: oscuro.withOpacity(0.4),
              )),
        );
      },
    );
  }
}

class _InputForm extends StatelessWidget {
  const _InputForm(
      {Key? key,
      required this.hintText,
      this.controller,
      this.iconActive = false,
      this.obscureText = false,
      this.obscureTextButton})
      : super(key: key);

  final String hintText;
  final TextEditingController? controller;
  final bool iconActive;
  final bool obscureText;
  final Widget? obscureTextButton;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 5,
      ),
      decoration: BoxDecoration(
        color: claro.withOpacity(0.3),
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextFormField(
        controller: controller,
        cursorColor: oscuro,
        style: TextStyle(),
        obscureText: obscureText,
        decoration: InputDecoration(
          hintText: hintText,
          fillColor: grey,
          focusColor: grey,
          hintStyle: TextStyle(),
          suffixIcon: iconActive ? obscureTextButton : null,
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class _ButtonLogin extends StatelessWidget {
  const _ButtonLogin({Key? key, required this.onPressed}) : super(key: key);
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: oscuro,
          onPrimary: claro,
          elevation: 2,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50)),
          ),
        ),
        onPressed: onPressed,
        child: Container(
          height: 55,
          width: double.infinity,
          child: Center(
              child: Text(
            'Iniciar sesión',
            style: TextStyle(fontSize: 17),
          )),
        ),
      ),
    );
  }
}
