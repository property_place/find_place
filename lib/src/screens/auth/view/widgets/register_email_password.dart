import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../routes/app_routes.dart';
import '../../../../service/logic/socket_provider.dart';
import '../../../widgets/view_alertas.dart';
import '../../data/models/register_push.dart';
import '../../logic/auth_provider.dart';
import '../../logic/auth_state.dart';
import '../../utils/geolocator.dart';

class FormRegisterEmailPassword extends ConsumerWidget {
  FormRegisterEmailPassword({
    Key? key,
  }) : super(key: key);

  final emailCRT = TextEditingController();
  final nameCRT = TextEditingController();
  final pLastNameCRT = TextEditingController();
  final sLastNameCRT = TextEditingController();
  final passCRT = TextEditingController();
  final viewAlert = ViewAlert();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<AuthState>(resolverAuthService, (oldAuthNotifier, newAuthNotifier) {
      if (newAuthNotifier.authenticated != null &&
          newAuthNotifier.authenticated!) {
        ref.read(resolverSocketService.notifier).connect();

        Navigator.pushNamedAndRemoveUntil(
            context, Routes.home, (route) => false);
      } else if (newAuthNotifier.isError != '') {
        viewAlert.viewAlert(context, newAuthNotifier.isError);
      }
    });

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Form(
          child: Column(
            children: [
              _InputForm(
                hintText: 'Nombres*',
                controller: nameCRT,
              ),
              SizedBox(height: 20),
              _InputForm(
                hintText: 'Primer apellido*',
                controller: pLastNameCRT,
              ),
              SizedBox(height: 20),
              _InputForm(
                hintText: 'Segundo apellido',
                controller: sLastNameCRT,
              ),
              SizedBox(height: 20),
              _InputForm(
                hintText: 'Correo electronico*',
                controller: emailCRT,
              ),
              SizedBox(height: 20),
              _TextFormFielPassword(passCRT: passCRT),
              SizedBox(height: 20),
              _ButtonLogin(onPressed: () => _hanledSubmit(ref)),
            ],
          ),
        ),
      ),
    );
  }

  void _hanledSubmit(WidgetRef ref) async {
    final _cityName = await _nameLocality();
    final data = RegisterPush(
        name: nameCRT.text.trim(),
        pLastName: pLastNameCRT.text.trim(),
        sLastName: sLastNameCRT.text,
        city: _cityName.trim(),
        email: emailCRT.text.trim(),
        password: passCRT.text.trim());
    ref.read(resolverAuthService.notifier).register(data);
  }

  Future<String> _nameLocality() async {
    try {
      final location = await determinePosition();
      final placemarks = await placemarkFromCoordinates(
        location.latitude,
        location.longitude,
      );
      return placemarks.first.locality ?? 'Bogota';
    } on Exception {
      return 'Bogota';
    }
  }
}

class _TextFormFielPassword extends StatelessWidget {
  const _TextFormFielPassword({
    Key? key,
    required this.passCRT,
  }) : super(key: key);

  final TextEditingController passCRT;

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        final obscureText = ref.watch(obscureTextPasswordProvider);
        return _InputForm(
          hintText: 'Contraseña',
          controller: passCRT,
          iconActive: true,
          obscureText: obscureText,
          obscureTextButton: IconButton(
              onPressed: () => ref
                  .read(obscureTextPasswordProvider.state)
                  .state = !obscureText,
              icon: Icon(
                !obscureText ? Ionicons.eye_off_outline : Ionicons.eye_outline,
                color: oscuro.withOpacity(0.4),
              )),
        );
      },
    );
  }
}

class _InputForm extends StatelessWidget {
  const _InputForm(
      {Key? key,
      required this.hintText,
      this.controller,
      this.iconActive = false,
      this.obscureText = false,
      this.obscureTextButton,
      this.isEdit = true})
      : super(key: key);

  final String hintText;
  final TextEditingController? controller;
  final bool iconActive;
  final bool obscureText;
  final Widget? obscureTextButton;
  final bool isEdit;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 5,
      ),
      decoration: BoxDecoration(
        color: claro.withOpacity(0.3),
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextFormField(
        controller: controller,
        cursorColor: oscuro,
        style: TextStyle(),
        enabled: isEdit,
        obscureText: obscureText,
        decoration: InputDecoration(
          hintText: hintText,
          fillColor: grey,
          focusColor: grey,
          hintStyle: TextStyle(),
          suffixIcon: iconActive ? obscureTextButton : null,
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class _ButtonLogin extends StatelessWidget {
  const _ButtonLogin({Key? key, required this.onPressed}) : super(key: key);
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: ElevatedButton(
        style: ButtonStyle(
          fixedSize: MaterialStateProperty.all(Size(size.width, 60)),
          backgroundColor: MaterialStateProperty.all(oscuro),
          padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(vertical: 20, horizontal: 30)),
          textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 20)),
          shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
        ),
        onPressed: onPressed,
        child: Text('Crear cuenta'),
      ),
    );
  }
}
