import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../core/framework/colors.dart';

class OtherWaysToLogin extends StatelessWidget {
  const OtherWaysToLogin({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: 80,
      alignment: Alignment.center,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.all(10),
        shrinkWrap: true,
        children: [
          _OtherWaysToLoginButton(
            icon: 'assets/iconGoogle.svg',
            onTap: () {},
          ),
          _OtherWaysToLoginButton(
            icon: 'assets/iconMac.svg',
            onTap: () {},
          ),
          _OtherWaysToLoginButton(
            icon: 'assets/iconFacebook.svg',
            color: Color(0xFF1878F1),
            onTap: () {},
          )
        ],
      ),
    );
  }
}

class _OtherWaysToLoginButton extends StatelessWidget {
  const _OtherWaysToLoginButton(
      {Key? key, required this.icon, required this.onTap, this.color})
      : super(key: key);
  final String icon;
  final void Function()? onTap;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {},
      child: Container(
        width: 80,
        height: 80,
        margin: const EdgeInsets.symmetric(horizontal: 5),
        decoration: BoxDecoration(
            border: Border.all(
              color: oscuro.withOpacity(0.1),
            ),
            borderRadius: BorderRadius.circular(15)),
        child: Container(
            padding: const EdgeInsets.all(15),
            child: SvgPicture.asset(
              icon,
              width: 5,
              height: 10,
              color: color,
            )),
      ),
    );
  }
}
