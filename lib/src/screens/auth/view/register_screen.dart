import 'package:flutter/material.dart';

import '../../../core/framework/colors.dart';
import '../../../core/framework/tipografia.dart';
import 'widgets/other_ways_to_login.dart';
import 'widgets/register_email_password.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primary,
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    _TitleHeader(),
                    FormRegisterEmailPassword(),
                    Container(
                      padding: const EdgeInsets.all(20),
                      child: Text('O continua con'),
                    ),
                    OtherWaysToLogin(),
                  ],
                ),
              ),
              Positioned(
                top: 65,
                left: 20,
                child: Container(
                  width: 50,
                  height: 50,
                  child: BackButton(
                    onPressed: () => Navigator.pop(context),
                    color: oscuro,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _TitleHeader extends StatelessWidget {
  const _TitleHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      padding: const EdgeInsets.all(10),
      alignment: Alignment.center,
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 20),
            height: size.height * 0.12,
            alignment: Alignment.center,
            child: Column(
              children: [
                Text(
                  'Hola!',
                  style: styleTituloItalicW600.copyWith(fontSize: 30),
                  textAlign: TextAlign.center,
                ),
                Container(
                  width: size.width * 0.6,
                  child: Text(
                    'Bienvenido a la sesión de registro',
                    softWrap: true,
                    style: styleTituloW100.copyWith(fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
