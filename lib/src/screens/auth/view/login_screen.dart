import 'package:flutter/material.dart';

import '../../../core/framework/colors.dart';
import '../../../core/framework/tipografia.dart';
import '../../../routes/app_routes.dart';
import 'widgets/login_email_password.dart';
import 'widgets/other_ways_to_login.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primary,
      body: SafeArea(
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                _TitleHeader(),
                FormLoginEmailPassword(),
                Container(
                  padding: const EdgeInsets.all(20),
                  child: Text('O continua con'),
                ),
                OtherWaysToLogin(),
                SizedBox(height: 40),
                _NotMemberRegister()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _TitleHeader extends StatelessWidget {
  const _TitleHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      padding: const EdgeInsets.all(10),
      alignment: Alignment.center,
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 20),
            height: size.height * 0.2,
            alignment: Alignment.center,
            child: Column(
              children: [
                Text(
                  'Hola!',
                  style: styleTituloItalicW600.copyWith(fontSize: 30),
                  textAlign: TextAlign.center,
                ),
                Container(
                  width: size.width * 0.6,
                  child: Text(
                    'Bienvenido de nuevo, se te ha extrañado',
                    softWrap: true,
                    style: styleTituloW100.copyWith(fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _NotMemberRegister extends StatelessWidget {
  const _NotMemberRegister({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      child: Wrap(
        children: [
          Text(
            'No eres miembro',
            style: styleTituloW100,
          ),
          SizedBox(width: 5),
          GestureDetector(
              onTap: () => Navigator.pushNamed(context, Routes.register),
              child: Text(
                'Regístrate ahora',
                style: styleTituloItalicW600.copyWith(color: secundary),
              ))
        ],
      ),
    );
  }
}
