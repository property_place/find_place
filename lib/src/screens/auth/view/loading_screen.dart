import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../service/logic/socket_provider.dart';
import '../../home/view/home_screen.dart';
import '../data/models/user_model.dart';
import '../logic/auth_provider.dart';
import 'login_screen.dart';

class LoadingScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final infoOfTheConnectedPerson = ref.watch(resolverAuthService).user;
    final authenticated = ref.watch(resolverAuthService).authenticated;

    return Scaffold(
      body: FutureBuilder<Object?>(
          future: chetloginState(
            context,
            ref: ref,
            authenticated: authenticated,
            infoUserConnected: infoOfTheConnectedPerson,
          ),
          builder: (context, snapshot) {
            return Center(
              child: Container(
                child: Text('Espere...'),
              ),
            );
          }),
    );
  }

  Future chetloginState(
    BuildContext context, {
    required WidgetRef ref,
    required bool? authenticated,
    required DocUserModel? infoUserConnected,
  }) async {
    ref.read(resolverAuthService.notifier).isLogedIn();
    SchedulerBinding.instance!.addPostFrameCallback((_) {
      if (authenticated != null) {
        if (authenticated) {
          ref.read(resolverSocketService.notifier).connect();
          Navigator.pushReplacement(
            context,
            PageRouteBuilder(
                pageBuilder: (_, __, ___) => HomeScreen(),
                transitionDuration: Duration(milliseconds: 0)),
          );
        } else {
          Navigator.pushReplacement(
            context,
            PageRouteBuilder(
                pageBuilder: (_, __, ___) => LoginScreen(),
                transitionDuration: Duration(milliseconds: 0)),
          );
        }
      }
    });
  }
}
