import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';

import '../../chat/logic/chat_my_immovables/my_immovables_provider.dart';
import '../../home/logic/categories/category_tab_provider.dart';
import '../../home/logic/cities/cities_provider.dart';
import '../../home/logic/filters/filters_provider.dart';
import '../../home/logic/immovable_offers/immovable_offers_provider.dart';
import '../../home/logic/immovables/immovables_provider.dart';
import '../../property_view/logic/property_immovable_image_provider.dart';
import '../../property_view/logic/property_immovable_provider.dart';
import '../data/models/register_push.dart';
import '../data/models/user_model.dart';
import '../data/repositories/auth_repository.dart';
import 'auth_state.dart';

part 'auth_state_notifier.dart';

/// Dependency Injection
final _client = http.Client();
final _storage = FlutterSecureStorage();

//* Logic  / StateNotifier
final resolverAuthService = StateNotifierProvider<AuthNotifier, AuthState>((ref) {
  return AuthNotifier(
    ref: ref,
    iAuthRepository: ref.watch(_authRepositoryProvider),
    storage: _storage,
  );
});

//* Repository
final _authRepositoryProvider = Provider<IAuthRepository>(
  (ref) => AuthRepository(
    ref: ref,
    storage: _storage,
    client: _client,
  ),
);

final userProvider = StateProvider<DocUserModel?>((ref) {
  final user = ref.watch(resolverAuthService).user;
  return user;
});

final authenticatedProvider = StateProvider<bool?>((ref) {
  return ref.watch(resolverAuthService).authenticated;
});

// VARIABLES STATE
final obscureTextPasswordProvider = StateProvider((ref) => true);
final authenticatingProvider = StateProvider((ref) => false);
