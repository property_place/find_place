import 'package:freezed_annotation/freezed_annotation.dart';

import '../data/models/user_model.dart';

part 'auth_state.freezed.dart';

@freezed
abstract class AuthState with _$AuthState {
  const factory AuthState(
      {DocUserModel? user,
      @Default(true) bool isLoading,
      @Default(false) bool isFetching,
      bool? authenticated,
      @Default(false) bool isGpsPermissionGranted,
      @Default('') String isError}) = _AuthState;

  const AuthState._();
}
