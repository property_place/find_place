part of 'auth_provider.dart';

class AuthNotifier extends StateNotifier<AuthState> {
  AuthNotifier({
    required IAuthRepository iAuthRepository,
    required FlutterSecureStorage storage,
    required Ref ref,
  })  : _iAuthRepository = iAuthRepository,
        _ref = ref,
        _storage = storage,
        super(AuthState()) {
    _init();
  }

  final IAuthRepository _iAuthRepository;
  final Ref _ref;
  StreamSubscription? gpsServiceSubscription;
  final FlutterSecureStorage _storage;

  void _init() async {
    final isEnabled = await _checkGpsStatus();
    if (!isEnabled) {
      askGpsAccess();
    }
  }

  Future<String> getToken() async {
    final token = await _storage.read(key: 'token');
    return token ?? '';
  }

  Future<bool> _checkGpsStatus() async {
    var isEnabled = await Geolocator.isLocationServiceEnabled();
    gpsServiceSubscription =
        Geolocator.getServiceStatusStream().listen((event) {
      isEnabled = (event.index == 1) ? true : false;
    });
    return isEnabled;
  }

  void askGpsAccess() async {
    final status = await Permission.location.request();
    switch (status) {
      case PermissionStatus.granted:
        state = state.copyWith(isGpsPermissionGranted: true);
        break;
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
      case PermissionStatus.limited:
      case PermissionStatus.permanentlyDenied:
        state = state.copyWith(isGpsPermissionGranted: false);
        openAppSettings();
    }
  }

  void login({required String email, required String password}) async {
    final user = await _iAuthRepository.login(email, password);

    if (!user.ok && user.doc == null) {
      state = state.copyWith(
        isLoading: false,
        authenticated: false,
        isError: user.error!.msg,
      );
      return;
    }
    state = state.copyWith(
      user: user.doc!,
      isLoading: false,
      authenticated: true,
      isError: '',
    );
  }

  void register(RegisterPush data) async {
    final user = await _iAuthRepository.register(data);
    if (!user.ok && user.doc == null) {
      state = state.copyWith(
        isLoading: false,
        authenticated: false,
        isError: user.error!.msg,
      );
      return;
    }
    state = state.copyWith(
      user: user.doc!,
      isLoading: false,
      authenticated: true,
      isError: '',
    );
  }

  void isLogedIn() async {
    final user = await _iAuthRepository.isLogedIn();
    if (user.doc == null && !user.ok) {
      state = state.copyWith(
        isLoading: false,
        authenticated: false,
        isError: '',
      );
      return;
    }

    state = state.copyWith(
      user: user.doc!,
      isLoading: false,
      authenticated: true,
      isError: '',
    );
  }

  void logout() async {
    await _iAuthRepository.logout();
    state = state.copyWith(authenticated: false);
    // _ref.refresh(resolverChatListImmService);
    // _ref.refresh(categoryTabNotifierProvider);
    // _ref.refresh(citiesNotifierProvider);
    // _ref.refresh(filtersNotifierProvider);
    // _ref.refresh(immovableOffersNotifierProvider);
    // _ref.refresh(immovablesNotifierProvider);
    // _ref.refresh(controllerSlideImage);
    // _ref.refresh(resolverPropertyImm);
  }

  @override
  void dispose() {
    gpsServiceSubscription?.cancel();
    super.dispose();
  }
}
