// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'auth_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AuthStateTearOff {
  const _$AuthStateTearOff();

  _AuthState call(
      {DocUserModel? user,
      bool isLoading = true,
      bool isFetching = false,
      bool? authenticated,
      bool isGpsPermissionGranted = false,
      String isError = ''}) {
    return _AuthState(
      user: user,
      isLoading: isLoading,
      isFetching: isFetching,
      authenticated: authenticated,
      isGpsPermissionGranted: isGpsPermissionGranted,
      isError: isError,
    );
  }
}

/// @nodoc
const $AuthState = _$AuthStateTearOff();

/// @nodoc
mixin _$AuthState {
  DocUserModel? get user => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isFetching => throw _privateConstructorUsedError;
  bool? get authenticated => throw _privateConstructorUsedError;
  bool get isGpsPermissionGranted => throw _privateConstructorUsedError;
  String get isError => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AuthStateCopyWith<AuthState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
  $Res call(
      {DocUserModel? user,
      bool isLoading,
      bool isFetching,
      bool? authenticated,
      bool isGpsPermissionGranted,
      String isError});
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;

  @override
  $Res call({
    Object? user = freezed,
    Object? isLoading = freezed,
    Object? isFetching = freezed,
    Object? authenticated = freezed,
    Object? isGpsPermissionGranted = freezed,
    Object? isError = freezed,
  }) {
    return _then(_value.copyWith(
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as DocUserModel?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
      authenticated: authenticated == freezed
          ? _value.authenticated
          : authenticated // ignore: cast_nullable_to_non_nullable
              as bool?,
      isGpsPermissionGranted: isGpsPermissionGranted == freezed
          ? _value.isGpsPermissionGranted
          : isGpsPermissionGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: isError == freezed
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$AuthStateCopyWith<$Res> implements $AuthStateCopyWith<$Res> {
  factory _$AuthStateCopyWith(
          _AuthState value, $Res Function(_AuthState) then) =
      __$AuthStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {DocUserModel? user,
      bool isLoading,
      bool isFetching,
      bool? authenticated,
      bool isGpsPermissionGranted,
      String isError});
}

/// @nodoc
class __$AuthStateCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements _$AuthStateCopyWith<$Res> {
  __$AuthStateCopyWithImpl(_AuthState _value, $Res Function(_AuthState) _then)
      : super(_value, (v) => _then(v as _AuthState));

  @override
  _AuthState get _value => super._value as _AuthState;

  @override
  $Res call({
    Object? user = freezed,
    Object? isLoading = freezed,
    Object? isFetching = freezed,
    Object? authenticated = freezed,
    Object? isGpsPermissionGranted = freezed,
    Object? isError = freezed,
  }) {
    return _then(_AuthState(
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as DocUserModel?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
      authenticated: authenticated == freezed
          ? _value.authenticated
          : authenticated // ignore: cast_nullable_to_non_nullable
              as bool?,
      isGpsPermissionGranted: isGpsPermissionGranted == freezed
          ? _value.isGpsPermissionGranted
          : isGpsPermissionGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: isError == freezed
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_AuthState extends _AuthState {
  const _$_AuthState(
      {this.user,
      this.isLoading = true,
      this.isFetching = false,
      this.authenticated,
      this.isGpsPermissionGranted = false,
      this.isError = ''})
      : super._();

  @override
  final DocUserModel? user;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final bool isFetching;
  @override
  final bool? authenticated;
  @JsonKey()
  @override
  final bool isGpsPermissionGranted;
  @JsonKey()
  @override
  final String isError;

  @override
  String toString() {
    return 'AuthState(user: $user, isLoading: $isLoading, isFetching: $isFetching, authenticated: $authenticated, isGpsPermissionGranted: $isGpsPermissionGranted, isError: $isError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _AuthState &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.isFetching, isFetching) &&
            const DeepCollectionEquality()
                .equals(other.authenticated, authenticated) &&
            const DeepCollectionEquality()
                .equals(other.isGpsPermissionGranted, isGpsPermissionGranted) &&
            const DeepCollectionEquality().equals(other.isError, isError));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isFetching),
      const DeepCollectionEquality().hash(authenticated),
      const DeepCollectionEquality().hash(isGpsPermissionGranted),
      const DeepCollectionEquality().hash(isError));

  @JsonKey(ignore: true)
  @override
  _$AuthStateCopyWith<_AuthState> get copyWith =>
      __$AuthStateCopyWithImpl<_AuthState>(this, _$identity);
}

abstract class _AuthState extends AuthState {
  const factory _AuthState(
      {DocUserModel? user,
      bool isLoading,
      bool isFetching,
      bool? authenticated,
      bool isGpsPermissionGranted,
      String isError}) = _$_AuthState;
  const _AuthState._() : super._();

  @override
  DocUserModel? get user;
  @override
  bool get isLoading;
  @override
  bool get isFetching;
  @override
  bool? get authenticated;
  @override
  bool get isGpsPermissionGranted;
  @override
  String get isError;
  @override
  @JsonKey(ignore: true)
  _$AuthStateCopyWith<_AuthState> get copyWith =>
      throw _privateConstructorUsedError;
}
