class RegisterPush {
  RegisterPush(
      {required this.name,
      required this.pLastName,
      required this.sLastName,
      required this.city,
      required this.email,
      required this.password,
      this.online = true});
  final String name;
  final String pLastName;
  final String sLastName;
  final String city;
  final String email;
  final String password;
  final bool online;
}
