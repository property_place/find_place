import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

abstract class UserPModel extends Equatable {
  UserPModel(
      {required this.userId,
       this.image,
      required this.email,
      required this.name,
      required this.pLastName,
      required this.sLastName});

  @JsonKey(name: 'user_id')
  final String userId;
  final String? image;
  final String email;
  final String name;
  @JsonKey(name: 'p_lastname')
  final String pLastName;
  @JsonKey(name: 's_lastname')
  final String? sLastName;

  @override
  List<Object?> get props => [];
}
