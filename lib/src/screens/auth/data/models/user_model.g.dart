// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      ok: json['ok'] as bool,
      doc: json['doc'] == null
          ? null
          : DocUserModel.fromJson(json['doc'] as Map<String, dynamic>),
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'ok': instance.ok,
      'doc': instance.doc,
      'error': instance.error,
    };

DocUserModel _$DocUserModelFromJson(Map<String, dynamic> json) => DocUserModel(
      userId: json['user_id'] as String,
      image: json['image'] as String?,
      email: json['email'] as String,
      name: json['name'] as String,
      pLastName: json['p_lastname'] as String,
      sLastName: json['s_lastname'] as String?,
      phoneNumber: json['phone_number'] as String?,
      rol: json['rol'] as String,
      street: json['street'] as String?,
      city: json['city'] as String,
    );

Map<String, dynamic> _$DocUserModelToJson(DocUserModel instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'image': instance.image,
      'email': instance.email,
      'name': instance.name,
      'p_lastname': instance.pLastName,
      's_lastname': instance.sLastName,
      'phone_number': instance.phoneNumber,
      'rol': instance.rol,
      'street': instance.street,
      'city': instance.city,
    };

ErrorModel _$ErrorModelFromJson(Map<String, dynamic> json) => ErrorModel(
      value: json['value'] as String?,
      msg: json['msg'] as String,
      param: json['param'] as String?,
      location: json['location'] as String?,
    );

Map<String, dynamic> _$ErrorModelToJson(ErrorModel instance) =>
    <String, dynamic>{
      'value': instance.value,
      'msg': instance.msg,
      'param': instance.param,
      'location': instance.location,
    };
