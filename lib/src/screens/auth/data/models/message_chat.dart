class MessageChat {
  MessageChat(
      {required this.message,
      required this.senderID,
      required this.receiverID,
      required this.time,
      required this.userId});
  final String message;
  final String senderID;
  final String receiverID;
  final String time;
  final String userId;
}
