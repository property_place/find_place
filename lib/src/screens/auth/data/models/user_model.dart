import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'user_p_model.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel extends Equatable {
  UserModel({required this.ok, this.doc, this.error});

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  final bool ok;
  final DocUserModel? doc;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocUserModel extends UserPModel {
  DocUserModel({
    required this.userId,
    this.image,
    required this.email,
    required this.name,
    required this.pLastName,
    required this.sLastName,
    required this.phoneNumber,
    required this.rol,
    required this.street,
    required this.city,
  }) : super(
          userId: userId,
          email: email,
          image: image,
          name: name,
          pLastName: pLastName,
          sLastName: sLastName,
        );

  factory DocUserModel.fromJson(Map<String, dynamic> json) =>
      _$DocUserModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocUserModelToJson(this);

  @JsonKey(name: 'user_id')
  final String userId;
  final String? image;
  final String email;
  final String name;
  @JsonKey(name: 'p_lastname')
  final String pLastName;
  @JsonKey(name: 's_lastname')
  final String? sLastName;
  @JsonKey(name: 'phone_number')
  final String? phoneNumber;
  final String rol;
  final String? street;
  final String city;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class ErrorModel extends Equatable {
  ErrorModel({
    this.value,
    required this.msg,
    this.param,
    this.location,
  });

  factory ErrorModel.fromJson(Map<String, dynamic> json) =>
      _$ErrorModelFromJson(json);
  Map<String, dynamic> toJson() => _$ErrorModelToJson(this);

  final String? value;
  final String msg;
  final String? param;
  final String? location;

  @override
  List<Object?> get props => [];
}
