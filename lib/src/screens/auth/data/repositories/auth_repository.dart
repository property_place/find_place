import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;

import '../../../../core/excepction/exception.dart';
import '../../../../core/global/environment.dart';
import '../../logic/auth_provider.dart';
import '../models/register_push.dart';
import '../models/user_model.dart';

abstract class IAuthRepository {
  Future<UserModel> login(String email, String password);
  Future<UserModel> register(RegisterPush data);
  Future<void> saveToken(String token);
  Future<void> logout();
  Future<UserModel> isLogedIn();
}

class AuthRepository extends IAuthRepository {
  AuthRepository(
      {required this.storage, required this.ref, required this.client});
  final FlutterSecureStorage storage;
  final Ref ref;
  final http.Client client;

  @override
  Future<UserModel> login(String email, String password) async {
    ref.read(authenticatingProvider.notifier).state = true;
    final data = {"email": email, "password": password};
    final response = await client.post(
        Uri.parse('${EnvironmentApi.linkApi}/auth/signin'),
        body: jsonEncode(data),
        headers: {'Content-Type': 'application/json'});

    ref.read(authenticatingProvider.notifier).state = false;
    if (response.statusCode == 200) {
      final loginResponse = UserModel.fromJson(json.decode(response.body));
      final token = response.headers['x-token'];
      if (token != null) await saveToken(token);
      return loginResponse;
    } else {
      return UserModel.fromJson(json.decode(response.body));
    }
  }

  @override
  Future<UserModel> register(RegisterPush user) async {
    ref.read(authenticatingProvider.notifier).state = true;

    final data = {
      "name": user.name,
      "plastname": user.pLastName,
      'city': user.city,
      "email": user.email,
      "password": user.password,
      "online": user.online
    };

    final response = await http.post(
        Uri.parse('${EnvironmentApi.linkApi}/auth/signup'),
        body: jsonEncode(data),
        headers: {'Content-Type': 'application/json'});

    ref.read(authenticatingProvider.notifier).state = false;
    if (response.statusCode == 200) {
      final loginResponse = UserModel.fromJson(json.decode(response.body));
      final token = response.headers['x-token'];
      if (token != null) await saveToken(token);
      return loginResponse;
    } else {
      return UserModel.fromJson(json.decode(response.body));
    }
  }

  @override
  Future<UserModel> isLogedIn() async {
    final token = await storage.read(key: 'token');

    final url = Uri.parse('${EnvironmentApi.linkApi}/auth/renew');
    final response = await client.get(
      url,
      headers: {
        "Accept": "application/json",
        'Content-Type': 'application/json',
        'x-token': token.toString()
      },
    );
    if (response.statusCode == 200) {
      final loginResponse = UserModel.fromJson(json.decode(response.body));
      final tokenResponse = response.headers['x-token'];
      if (tokenResponse != null) {
        await saveToken(tokenResponse);
      }
      return loginResponse;
    } else {
      logout();
      final loginResponse = UserModel.fromJson(json.decode(response.body));
      return loginResponse;
    }
  }

  @override
  Future<void> logout() async {
    await storage.delete(key: 'token');
  }

  @override
  Future<void> saveToken(String token) async {
    return await storage.write(key: 'token', value: token);
  }
}
