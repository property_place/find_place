// import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../core/framework/colors.dart';
import '../../core/framework/tipografia.dart';
// import '../../../../data/models/enum_type_global.dart';

class TextFieldSearch extends StatelessWidget {
  const TextFieldSearch(
      {Key? key,
      required this.controller,
      required this.name,
      required this.onChanged,
      this.focusNode,
      this.maxLines,
      this.type,
      this.padding = 20.0,
      this.radius = 10.0,
      this.color = primary,
      this.colorOP = 0.2,
      this.styleLabel = styleTituloW600,
      required this.prefixIcon,
      this.height = 80})
      : super(key: key);

  ///Variables de controller
  final TextEditingController controller;
  final String name;
  final TextStyle styleLabel;
  final FocusNode? focusNode;
  final int? maxLines;
  final TextInputType? type;
  final double padding;
  final double radius;
  final Color color;
  final double colorOP;
  final Widget prefixIcon;
  final Function(String)? onChanged;
  final double height;
  @override
  Widget build(BuildContext context) {
    OutlineInputBorder border;
    border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(radius),
        borderSide: BorderSide.merge(const BorderSide(), const BorderSide()));
    return Container(
      height: height,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: type,
        maxLines: maxLines,
        controller: controller,
        textInputAction: TextInputAction.search,
        onChanged: onChanged,
        focusNode: focusNode,
        style: TextStyle(color: oscuro, fontSize: 18),
        decoration: InputDecoration(
          counterText: '',
          prefixIcon: prefixIcon,
          prefixStyle: const TextStyle(color: oscuro),
          suffixStyle: const TextStyle(color: oscuro),
          border: InputBorder.none,
          fillColor: primary,
          filled: true,
          floatingLabelBehavior: FloatingLabelBehavior.never,
          hintText: name,
          labelStyle: TextStyle(color: oscuro),
          focusedErrorBorder: border,
          focusedBorder: border,
          enabledBorder: border,
          errorBorder: border,
        ),
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(ColorProperty('color', color));
  }
}
