import 'package:flutter/material.dart';

import '../../core/framework/colors.dart';
import '../../core/framework/tipografia.dart';

class TextFieldChat extends StatelessWidget {
  const TextFieldChat({
    Key? key,
    required this.controller,
    required this.name,
    required this.onChanged,
    this.prefixIcon,
    this.onSubmitted,
    this.focusNode,
    this.maxLines,
    this.type,
    this.styleLabel = styleTituloW600,
  }) : super(key: key);

  final TextEditingController controller;
  final String name;
  final TextStyle styleLabel;
  final FocusNode? focusNode;
  final int? maxLines;
  final TextInputType? type;
  final Function(String)? onChanged;
  final void Function(String)? onSubmitted;
  final Widget? prefixIcon;

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: type,
      maxLines: maxLines,
      controller: controller,
      textInputAction: TextInputAction.send,
      onChanged: onChanged,
      focusNode: focusNode,
      style: TextStyle(color: oscuro, fontSize: 18),
      onSubmitted: onSubmitted,
      decoration: InputDecoration(
        border: InputBorder.none,
        fillColor: primary,
        floatingLabelBehavior: FloatingLabelBehavior.never,
        hintText: name,
        prefixIcon: prefixIcon,
      ),
    );
  }
}
