import 'package:flutter/material.dart';
import '../../core/framework/colors.dart';

class CenterIndicator extends StatelessWidget {
  const CenterIndicator({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: const Center(
            child: CircularProgressIndicator(
          color: oscuro,
          strokeWidth: 2,
        )));
  }
}
