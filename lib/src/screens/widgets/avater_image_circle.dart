import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'center_indicator.dart';

class AvaterCircleImage extends StatelessWidget {
  const AvaterCircleImage(
      {Key? key, required this.imageUrl, this.childAlternative})
      : super(key: key);
  final String imageUrl;
  final Widget? childAlternative;
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      imageBuilder: (context, imageProvider) => CircleAvatar(
        backgroundImage: imageProvider,
      ),
      placeholder: (context, url) => CenterIndicator(),
      errorWidget: (context, url, error) =>
          childAlternative ?? Icon(Icons.error),
    );
  }
}
