import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../core/framework/colors.dart';
import '../../core/framework/tipografia.dart';

class ViewAlert {
  void viewAlert(BuildContext context, String title) {
    if (Platform.isAndroid) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            title,
            style: styleTituloNormal,
          ),
        ),
      );
    } else {
      showCupertinoDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
                title: Text(
                  title,
                  style: styleTituloNormal,
                ),
                actions: [
                  CupertinoDialogAction(
                    isDestructiveAction: true,
                    child: Text(
                      'SALIR',
                      style: styleTituloItalicW600.copyWith(color: oscuro),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
              ));
    }
  }
}
