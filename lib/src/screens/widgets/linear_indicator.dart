import 'package:flutter/material.dart';

import '../../core/framework/colors.dart';

class LinearIndicator extends StatelessWidget {
  const LinearIndicator({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      color: oscuro,
      backgroundColor: grey,
      minHeight: 2,
    );
  }
}
