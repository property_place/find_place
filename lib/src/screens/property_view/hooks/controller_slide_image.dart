import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../logic/property_immovable_image_provider.dart';

class ControllerPageImage {
  ControllerPageImage(
      {required this.pageViewController,
      required this.transformationController});
  final PageController pageViewController;
  final TransformationController transformationController;
}

ControllerPageImage useControllerSlideImage(WidgetRef ref,
    {int? seletedIndex}) {
  return use(_ScrollControllerHook(ref, seletedIndex: seletedIndex));
}

class _ScrollControllerHook extends Hook<ControllerPageImage> {
  const _ScrollControllerHook(this.ref, {this.seletedIndex});
  final WidgetRef ref;
  final int? seletedIndex;

  @override
  _ScrollControllerHookState createState() => _ScrollControllerHookState();
}

class _ScrollControllerHookState
    extends HookState<ControllerPageImage, _ScrollControllerHook> {
  late final PageController _pageViewController;
  late final TransformationController _transformationController;
  @override
  void initHook() {
    _pageViewController = PageController(initialPage: hook.seletedIndex ?? 0);
    _transformationController = TransformationController();
    _pageViewController.addListener(() async {
      hook.ref.read(controllerSlideImage).currentPage =
          _pageViewController.page ?? 0;
      ;
    });
  }

  @override
  ControllerPageImage build(BuildContext context) => ControllerPageImage(
      pageViewController: _pageViewController,
      transformationController: _transformationController);

  @override
  void dispose() => _pageViewController.dispose();
}
