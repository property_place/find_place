import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../routes/app_routes.dart';
import '../../../home/logic/immovables/immovables_provider.dart';
import '../../../home/utils/price_format.dart';
import '../../../utils/type_hero_image.dart';
import '../../data/models/photo_view_full_screen_model.dart';
import '../../data/models/property_immovable_model.dart';
import '../../logic/property_immovable_image_provider.dart';
import '../../logic/property_immovable_provider.dart';

class ImagesViewPropertyImmovable extends ConsumerWidget {
  const ImagesViewPropertyImmovable(ImmovableModel immovable,
      {Key? key, required TabController controller})
      : _controller = controller,
        _immovable = immovable,
        super(key: key);
  final ImmovableModel _immovable;
  final TabController _controller;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      height: MediaQuery.of(context).size.height * .5,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Stack(
        children: [
          Consumer(
            builder: (context, ref, child) {
              final image = ref.watch(resolverPropertyImm).partsHouseImageView;
              final index = ref.watch(indexHero.notifier).state;

              return GestureDetector(
                onTap: () => navigateViewImage(context, ref, image),
                child: Hero(
                  tag: "${TypeHero.image_product.name + index.toString()}",
                  child: CachedNetworkImage(
                    imageUrl: image!.partsHouseImage,
                    imageBuilder: (context, imageProvider) => Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 4), // changes position of shadow
                          ),
                        ],
                      ),
                    ),
                    placeholder: (context, url) =>
                        Center(child: CircularProgressIndicator(color: oscuro)),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              );
            },
          ),
          Positioned(
              top: 20,
              left: 20,
              child: Container(
                width: 150,
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                decoration: BoxDecoration(
                    color: secundary, borderRadius: BorderRadius.circular(5)),
                child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      '\$ ${convertPrice(_immovable.price)} / pesos',
                      style: styleTituloItalicW600.copyWith(color: claro),
                    )),
              )),
          Positioned(
              top: 20,
              right: 20,
              child: Container(
                child: Row(
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      margin: const EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                          color: claro.withOpacity(0.9),
                          borderRadius: BorderRadius.circular(5)),
                      child: IconButton(
                        onPressed: () {},
                        icon: Icon(Ionicons.share_social_outline),
                      ),
                    ),
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                          color: claro.withOpacity(0.9),
                          borderRadius: BorderRadius.circular(5)),
                      child: IconButton(
                        onPressed: () {},
                        icon: Icon(Ionicons.heart_outline),
                      ),
                    ),
                  ],
                ),
              )),
          Positioned(
            bottom: 25,
            child: _ImagesPropertyImmovable(
                controller: _controller,
                partsHouseImages: _immovable.immovableImages.partsHouseImages),
          )
        ],
      ),
    );
  }

  void navigateViewImage(
      BuildContext context, WidgetRef ref, PartsHouseImageModel? image) {
    final seletedDefaultIndex =
        _immovable.immovableImages.partsHouseImages.indexWhere(
      (st) => st.partsHouseImageId == image!.partsHouseImageId,
    );

    ref.read(controllerSlideImage).currentPage = seletedDefaultIndex.toDouble();

    Navigator.pushNamed(
      context,
      Routes.property_immovable_view_images,
      arguments: ScreenArgumentsPhotoViewFullScreen(
        seletedDefaultIndex: seletedDefaultIndex,
        partsHouseImages: _immovable.immovableImages.partsHouseImages,
      ),
    );
  }
}

class _ImagesPropertyImmovable extends StatelessWidget {
  const _ImagesPropertyImmovable({
    Key? key,
    required List<PartsHouseImageModel> partsHouseImages,
    required TabController controller,
  })  : _controller = controller,
        _partsHouseImages = partsHouseImages,
        super(key: key);
  final TabController _controller;
  final List<PartsHouseImageModel> _partsHouseImages;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowIndicator();
            return true;
          },
          child: TabBar(
              controller: _controller,
              isScrollable: true,
              indicatorColor: Colors.transparent,
              physics: ClampingScrollPhysics(),
              labelPadding: EdgeInsets.symmetric(horizontal: 8.0),
              padding: const EdgeInsets.only(right: 40),
              tabs: _partsHouseImages
                  .asMap()
                  .entries
                  .map((item) => AnimatedBuilder(
                        animation: _controller,
                        builder: (context, child) {
                          return Consumer(
                            builder: (context, ref, child) {
                              final selectedImage = ref
                                  .watch(resolverPropertyImm)
                                  .partsHouseImageView;
                              return GestureDetector(
                                onTap: () => itemSelectedImage(item, ref),
                                child: Container(
                                  width: 80,
                                  height: 80,
                                  color: claro,
                                  child: CachedNetworkImage(
                                    imageUrl: item.value.partsHouseImage,
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                      width: 65,
                                      height: 65,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        border: item.value.partsHouseImageId ==
                                                selectedImage!.partsHouseImageId
                                            ? Border.all(color: claro, width: 3)
                                            : Border.all(
                                                color: claro, width: 0),
                                        image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    placeholder: (context, url) => Center(
                                        child: CircularProgressIndicator(
                                            color: oscuro)),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      ))
                  .toList()),
        ));
  }

  void itemSelectedImage(
      MapEntry<int, PartsHouseImageModel> item, WidgetRef ref) {
    _controller.animateTo(item.key);
    ref
        .read(resolverPropertyImm.notifier)
        .selectedImageView(item.value.partsHouseImageId);
  }
}
