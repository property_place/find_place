import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../utils/type_hero_image.dart';
import '../../data/models/photo_view_full_screen_model.dart';
import '../../data/models/property_immovable_model.dart';
import '../../logic/property_immovable_image_provider.dart';
import 'slideshow.dart';

class PropertyPhotoViewFullScreen extends StatelessWidget {
  const PropertyPhotoViewFullScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)?.settings.arguments
        as ScreenArgumentsPhotoViewFullScreen;

    return Scaffold(
      backgroundColor: primary,
      body: SafeArea(
        child: Stack(
          children: [
            _SlideImageImmovable(
              partsHouseImages: args.partsHouseImages,
              seleteddefault: args.seletedDefaultIndex,
            ),
            Positioned(
              top: 20,
              left: 10,
              right: 10,
              child: _AppBarSlideImage(
                partsHouseImages: args.partsHouseImages,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _SlideImageImmovable extends StatelessWidget {
  const _SlideImageImmovable({
    Key? key,
    required this.partsHouseImages,
    required this.seleteddefault,
  }) : super(key: key);

  final List<PartsHouseImageModel> partsHouseImages;
  final int? seleteddefault;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
        width: size.width,
        height: size.height,
        child: SlideShow(
            seletedDefaultIndex: seleteddefault,
            slides: partsHouseImages
                .map((image) => CachedNetworkImage(
                      imageUrl: image.partsHouseImage,
                      imageBuilder: (context, imageProvider) => Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Center(
                          child: CircularProgressIndicator(color: oscuro)),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ))
                .toList()));
  }
}

class _AppBarSlideImage extends StatelessWidget {
  const _AppBarSlideImage({
    Key? key,
    required this.partsHouseImages,
  }) : super(key: key);

  final List<PartsHouseImageModel> partsHouseImages;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            icon: Icon(
              Ionicons.close_outline,
              size: 30,
            ),
            onPressed: () => Navigator.pop(context),
            color: oscuro,
          ),
          Consumer(
            builder: (context, ref, child) {
              final page = ref.watch(controllerSlideImage);
              return Container(
                margin: const EdgeInsets.only(right: 10),
                child: Text(
                  partsHouseImages[page.currentPage.toInt()]
                      .partsHouseDescription,
                  style: styleTituloItalicW600.copyWith(fontSize: 20),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
