import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../data/models/property_immovable_model.dart';
import '../../logic/property_immovable_provider.dart';

class IndicatorImagePropertyPanel extends StatelessWidget {
  const IndicatorImagePropertyPanel(
    this.partsHouseImages, {
    Key? key,
    required TabController controller,
  })  : _controller = controller,
        super(key: key);
  final List<PartsHouseImageModel> partsHouseImages;
  final TabController _controller;

  void animateAndScrollTo(
    MapEntry<int, PartsHouseImageModel> item,
    WidgetRef ref,
  ) {
    _controller.animateTo(item.key);

    ref
        .read(resolverPropertyImm.notifier)
        .selectedImageView(item.value.partsHouseImageId);
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (overscroll) {
        overscroll.disallowIndicator();
        return true;
      },
      child: Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            margin: const EdgeInsets.only(left: 50),
            child: TabBar(
                controller: _controller,
                isScrollable: true,
                physics: ClampingScrollPhysics(),
                labelPadding: EdgeInsets.symmetric(horizontal: 5.0),
                indicatorColor: Colors.transparent,
                tabs: partsHouseImages.asMap().entries.map((item) {
                  return AnimatedBuilder(
                    animation: _controller,
                    builder: (_, __) => Consumer(
                      builder: (context, ref, child) {
                        final selectedpartsHouseImage =
                            ref.watch(resolverPropertyImm).partsHouseImageView;
                        return GestureDetector(
                          onTap: () => animateAndScrollTo(item, ref),
                          child: Container(
                            height: 40,
                            alignment: Alignment.center,
                            margin: const EdgeInsets.all(0),
                            decoration: BoxDecoration(
                              color: item.value.partsHouseImageId ==
                                      selectedpartsHouseImage!.partsHouseImageId
                                  ? activeTab
                                  : grey.withOpacity(0.3),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Text(
                                item.value.partsHouseDescription,
                                style: styleTituloNormal.copyWith(
                                  color: item.value.partsHouseImageId ==
                                          selectedpartsHouseImage
                                              .partsHouseImageId
                                      ? claro
                                      : oscuro.withOpacity(0.7),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }).toList()),
          ),
          Positioned(
              top: 15,
              left: 20,
              child: BackButton(onPressed: () => Navigator.pop(context)))
        ],
      ),
    );
  }
}
