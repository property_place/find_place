import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/tipografia.dart';
import '../../data/models/property_immovable_model.dart';

class DescriptionPropertyImmovable extends StatelessWidget {
  const DescriptionPropertyImmovable(
    this.immovable, {
    Key? key,
  }) : super(key: key);
  final ImmovableModel immovable;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: ListTile(
        title: Text(immovable.description,
            style: styleTituloItalicW600.copyWith(fontSize: 20)),
        subtitle: Row(
          children: [
            Icon(
              Ionicons.location,
              size: 20,
              color: Colors.red,
            ),
            Text('${immovable.street} ${immovable.city}'),
          ],
        ),
      ),
    );
  }
}
