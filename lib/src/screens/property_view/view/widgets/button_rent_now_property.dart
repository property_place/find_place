import 'package:flutter/material.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';

class ButtonRentNowProperty extends StatelessWidget {
  const ButtonRentNowProperty(
      {Key? key, required this.title, required this.onPressed})
      : super(key: key);
  final String title;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          height: 50,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Text(
            title,
            style: styleTituloNormal.copyWith(fontSize: 18),
          ),
          color: oscuro,
          textColor: claro,
          onPressed: onPressed),
    );
  }
}
