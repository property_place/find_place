// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '../../../../core/framework/colors.dart';
import '../../hooks/controller_slide_image.dart';
import '../../logic/property_immovable_image_provider.dart';

class SlideShow extends StatelessWidget {
  const SlideShow(
      {Key? key,
      required this.slides,
      this.indicatorUp = false,
      this.primaryColor = oscuro,
      this.secundaryColor = secundary,
      this.bulletPrimary = 12,
      this.bulletSecundary = 12,
      this.seletedDefaultIndex})
      : super(key: key);
  final List<Widget> slides;
  final bool indicatorUp;
  final Color primaryColor;
  final Color secundaryColor;
  final double bulletPrimary;
  final double bulletSecundary;
  final int? seletedDefaultIndex;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(child: Consumer(
        builder: (context, ref, child) {
          ref.watch(controllerSlideImage).primaryColor = primaryColor;
          // ref.watch(controllerSlideImage).secundaryColor = secundaryColor;
          ref.watch(controllerSlideImage).bulletPrimary = bulletPrimary;
          ref.watch(controllerSlideImage).bulletSecundary = bulletSecundary;
       
          return Column(
            children: [
              if (indicatorUp) _Dots(slides.length),
              Expanded(
                  child: _Slides(slides, seletedIndex: seletedDefaultIndex)),
              if (!indicatorUp) _Dots(slides.length),
            ],
          );
        },
      )),
    );
  }
}

class _Dots extends StatelessWidget {
  const _Dots(this.allSlide, {Key? key}) : super(key: key);
  final int allSlide;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 70,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(allSlide, _Dot.new)));
  }
}

class _Dot extends ConsumerWidget {
  const _Dot(this.index, {Key? key}) : super(key: key);
  final int index;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pageViewIndex = ref.watch(controllerSlideImage);

    double tam;
    Color colorTemp;

    if (pageViewIndex.currentPage >= index - 0.5 &&
        pageViewIndex.currentPage < index + 0.5) {
      tam = pageViewIndex.bulletPrimary;
      colorTemp = pageViewIndex.primaryColor;
    } else {
      tam = pageViewIndex.bulletSecundary;
      colorTemp = pageViewIndex.secundaryColor;
    }

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      width: tam,
      height: tam,
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(color: colorTemp, shape: BoxShape.circle),
    );
  }
}

class _Slides extends HookConsumerWidget {
  _Slides(this.slides, {Key? key, this.seletedIndex}) : super(key: key);
  final List<Widget> slides;
  final int? seletedIndex;

  late ControllerPageImage _controller;
  late AnimationController _controllerReset;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    _controller = useControllerSlideImage(ref, seletedIndex: seletedIndex);
    _controllerReset =
        useAnimationController(duration: Duration(milliseconds: 200));
    return Container(
      padding: const EdgeInsets.only(),
      child: GestureDetector(
        onTap: reset,
        child: InteractiveViewer(
          transformationController: _controller.transformationController,
          maxScale: 5,
          alignPanAxis: true,
          child: PageView(
            physics: BouncingScrollPhysics(),
            controller: _controller.pageViewController,
            children: slides.map(_Slide.new).toList(),
          ),
        ),
      ),
    );
  }

  void reset() {
    final animationReset = Matrix4Tween(
      begin: _controller.transformationController.value,
      end: Matrix4.identity(),
    ).animate(_controllerReset);

    animationReset.addListener(() {
      _controller.transformationController.value = animationReset.value;
    });

    _controllerReset.reset();
    _controllerReset.forward();
  }
}

class _Slide extends StatelessWidget {
  const _Slide(this.slide, {Key? key}) : super(key: key);
  final Widget slide;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: double.infinity,
        padding: const EdgeInsets.all(30),
        child: slide);
  }
}
