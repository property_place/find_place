import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../core/global/environment.dart';
import '../../data/models/property_immovable_model.dart';

class PartsHouseCharacteristicPropertyImmovable extends StatelessWidget {
  const PartsHouseCharacteristicPropertyImmovable(
    this._characteristicImage, {
    Key? key,
  }) : super(key: key);

  final List<PartsHouseCharacteristicImageModel>? _characteristicImage;

  @override
  Widget build(BuildContext context) {
    return _characteristicImage == null || _characteristicImage!.isEmpty
        ? Container()
        : Container(
            height: 70,
            margin: const EdgeInsets.only(top: 20, left: 10, right: 10),
            child: NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowIndicator();
                return true;
              },
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  physics: ClampingScrollPhysics(),
                  itemCount: _characteristicImage!.length,
                  itemBuilder: (_, index) => Container(
                        width: 80,
                        height: 30,
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: oscuro.withOpacity(0.1),
                            ),
                            borderRadius: BorderRadius.circular(5)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.network(
                              '${EnvironmentApi.imageHttp}${_characteristicImage![index].partsHouseCharacteristicImage}',
                              width: 30,
                              height: 30,
                            ),
                            Text(
                              _characteristicImage![index]
                                  .partsHouseCharacteristic,
                              style: styleTituloNormal.copyWith(fontSize: 15),
                            )
                          ],
                        ),
                      )),
            ),
          );
  }
}
