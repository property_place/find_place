import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../core/framework/colors.dart';
import '../../auth/logic/auth_provider.dart';
import '../../chat/data/models/chat_users/chat_users_model.dart';
import '../../chat/logic/chat_provider.dart';
import '../../chat/view/Widgets/error_load.dart';
import '../../chat/view/chat_my_immovable/chat_my_list.dart';
import '../../chat/view/chat_screen.dart';
import '../../widgets/center_indicator.dart';
import '../data/models/property_immovable_model.dart';
import '../logic/property_immovable_provider.dart';
import 'widgets/button_rent_now_property.dart';
import 'widgets/description_immovable_property.dart';
import 'widgets/image_view_property_immovable.dart';
import 'widgets/indicator_image_property_immovable.dart';
import 'widgets/opacity_tween_effects.dart';
import 'widgets/parts_house_characteristic_property_immovable.dart';
import 'widgets/slideUp_tween_effects.dart';

class PropertyViewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primary,
      body: SafeArea(
          child: Container(
        height: MediaQuery.of(context).size.height,
        child: _PropertyViewDetail(),
      )),
    );
  }
}

class _PropertyViewDetail extends HookConsumerWidget {
  const _PropertyViewDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _propertyImmovable = ref.watch(resolverPropertyImm);
    final _rCreatorImmovable = ref.watch(rCreatorImmovable);
    final _partsHouseImages = ref.watch(rPartsHouseImages);
    final _characteristicImages = ref.watch(rCharacteristicImages);

    final user = ref.watch(userProvider);

    if (_propertyImmovable.immovable == null &&
        _rCreatorImmovable == null &&
        _partsHouseImages == null) {
      // error case
      if (_propertyImmovable.isLoading == false) {
        return Center(
          child: ErrorChtListLoad('Error',
              onPressed: () => ref
                  .read(resolverPropertyImm.notifier)
                  .initPropertyImmovable(delay: true)),
        );
      }
      return const CenterIndicator();
    }

    final _controller = useTabController(
      initialLength: _partsHouseImages!.length,
    );

    return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (overscroll) {
        overscroll.disallowIndicator();
        return true;
      },
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            IndicatorImagePropertyPanel(_partsHouseImages,
                controller: _controller),
            ImagesViewPropertyImmovable(_propertyImmovable.immovable!,
                controller: _controller),
            DescriptionPropertyImmovable(_propertyImmovable.immovable!),
            PartsHouseCharacteristicPropertyImmovable(_characteristicImages),
            _rCreatorImmovable?.userId != user?.userId
                ? OpacityTween(
                    child: SlideUpTween(
                        begin: const Offset(-30, 60),
                        child: ButtonRentNowProperty(
                          title: 'Hablar para Alquilar',
                          onPressed: () => _hanledSubmit(
                              context, ref, _propertyImmovable.immovable!),
                        )),
                  )
                : OpacityTween(
                    child: SlideUpTween(
                    begin: const Offset(-30, 60),
                    child: ButtonRentNowProperty(
                      title: 'Ver los chat',
                      onPressed: () => _sendToCreator(context),
                    ),
                  ))
          ],
        ),
      ),
    );
  }

  void _hanledSubmit(
      BuildContext context, WidgetRef ref, ImmovableModel immovable) {
    /**
      * Obtenemos los datos del usuario, junto con el id del inmueble,
      * @_immovableId, por el cual se creo el chat
     */
    ref.read(rSelectedUser.state).state = DocChatUsersModel(
        userId: immovable.creator.userId,
        email: immovable.creator.email,
        name: immovable.creator.name,
        pLastName: immovable.creator.pLastName,
        online: immovable.creator.online,
        lastMessage: immovable.creator.lastMessage,
        immovableId: immovable.creator.immovableId,
        chatId: '',
        time: '',
        rol: immovable.creator.rol);

    const transitionDuration = Duration(milliseconds: 200);
    Navigator.of(context).push(
      PageRouteBuilder(
        transitionDuration: transitionDuration,
        reverseTransitionDuration: transitionDuration,
        pageBuilder: (_, animation, __) {
          return FadeTransition(
            opacity: animation,
            child: ChatScreen(),
          );
        },
      ),
    );
  }

  void _sendToCreator(BuildContext context) {
    const transitionDuration = Duration(milliseconds: 200);

    Navigator.of(context).push(
      PageRouteBuilder(
        transitionDuration: transitionDuration,
        reverseTransitionDuration: transitionDuration,
        pageBuilder: (_, animation, __) {
          return FadeTransition(
              opacity: animation, child: ChatListScreen() //ChatListScreen(),
              );
        },
      ),
    );
  }
}
