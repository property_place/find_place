import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../../core/global/environment.dart';
import '../models/property_immovable_model.dart';

abstract class IPropertyImmovableRepository {
  Future<GetImmovableResponseModel> getPropertyImmovables(String immovableId);
}

class PropertyImmovableRepository extends IPropertyImmovableRepository {
  PropertyImmovableRepository(this.client);
  final http.Client client;

  @override
  Future<GetImmovableResponseModel> getPropertyImmovables(String immovableId) async {
    final url = Uri.parse('${EnvironmentApi.linkApi}/immovables/getImmovable/$immovableId');
    final response = await client.get(url);
    if (response.statusCode == 200) {
      final d = GetImmovableResponseModel.fromJson(json.decode(response.body));
      print(d.toJson());
      return d;
    } else {
      final data = GetImmovableResponseModel.fromJson(json.decode(response.body));
      throw Exception(data);
    }
  }
}
