import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../auth/data/models/user_model.dart';
import '../../../auth/data/models/user_p_model.dart';

part 'property_immovable_model.g.dart';

@JsonSerializable()
class GetImmovableResponseModel extends Equatable {
  GetImmovableResponseModel({required this.ok, this.doc, this.error});

  factory GetImmovableResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetImmovableResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$GetImmovableResponseModelToJson(this);

  final bool ok;
  final DocImmovableModel? doc;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocImmovableModel extends Equatable {
  DocImmovableModel({
    required this.immovable,
  });

  factory DocImmovableModel.fromJson(Map<String, dynamic> json) =>
      _$DocImmovableModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocImmovableModelToJson(this);

  final ImmovableModel immovable;
  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class ImmovableModel extends Equatable {
  ImmovableModel({
    required this.immovableId,
    required this.description,
    required this.characteristic,
    required this.detail,
    required this.state,
    required this.price,
    required this.publishedDate,
    required this.street,
    required this.city,
    required this.immovableImages,
    required this.creator,
  });

  factory ImmovableModel.fromJson(Map<String, dynamic> json) =>
      _$ImmovableModelFromJson(json);
  Map<String, dynamic> toJson() => _$ImmovableModelToJson(this);

  @JsonKey(name: 'immovable_id')
  final String immovableId;
  final String description;
  final String characteristic;
  final String detail;
  final String state;
  final int price;
  @JsonKey(name: 'published_date')
  final DateTime publishedDate;
  final String street;
  final String city;
  @JsonKey(name: 'immovable_images')
  final ImmovableimageModel immovableImages;
  final DocChatUserImmovableModel creator;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocChatUserImmovableModel extends UserPModel {
  DocChatUserImmovableModel({
    required this.userId,
    required this.email,
    this.image,
    required this.name,
    required this.pLastName,
    this.sLastName,
    required this.online,
    required this.lastMessage,
    required this.immovableId,
    required this.rol
  }) : super(
          userId: userId,
          email: email,
          image: image,
          name: name,
          pLastName: pLastName,
          sLastName: sLastName,
        );

  factory DocChatUserImmovableModel.fromJson(Map<String, dynamic> json) =>
      _$DocChatUserImmovableModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocChatUserImmovableModelToJson(this);

  @JsonKey(name: 'user_id')
  final String userId;
  final String email;
  final String? image;
  final String name;
  @JsonKey(name: 'p_lastname')
  final String pLastName;
  @JsonKey(name: 's_lastname')
  final String? sLastName;
  final bool online;
  @JsonKey(name: 'last_message')
  final String? lastMessage;
  @JsonKey(name: 'immovable_id')
  final String immovableId;
  final String rol;
  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class ImmovableimageModel extends Equatable {
  ImmovableimageModel({
    required this.immovableImageId,
    required this.outsideFrontImage,
    required this.partsHouseImages,
    required this.partsHouseCharacteristicImages,
  });

  factory ImmovableimageModel.fromJson(Map<String, dynamic> json) =>
      _$ImmovableimageModelFromJson(json);
  Map<String, dynamic> toJson() => _$ImmovableimageModelToJson(this);

  @JsonKey(name: 'immovable_image_id')
  final String immovableImageId;
  @JsonKey(name: 'outside_front_image')
  final String outsideFrontImage;
  @JsonKey(name: 'parts_house_images')
  final List<PartsHouseImageModel> partsHouseImages;
  @JsonKey(name: 'parts_house_characteristic_images')
  final List<PartsHouseCharacteristicImageModel> partsHouseCharacteristicImages;
  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class PartsHouseCharacteristicImageModel extends Equatable {
  PartsHouseCharacteristicImageModel({
    required this.partsHouseCharacteristic,
    required this.partsHouseCharacteristicImage,
    required this.partsHouseCharacteristicImageId,
  });

  factory PartsHouseCharacteristicImageModel.fromJson(
          Map<String, dynamic> json) =>
      _$PartsHouseCharacteristicImageModelFromJson(json);
  Map<String, dynamic> toJson() =>
      _$PartsHouseCharacteristicImageModelToJson(this);

  @JsonKey(name: 'parts_house_characteristic')
  final String partsHouseCharacteristic;
  @JsonKey(name: 'parts_house_characteristic_image')
  final String partsHouseCharacteristicImage;
  @JsonKey(name: 'parts_house_characteristic_image_id')
  final String partsHouseCharacteristicImageId;
  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class PartsHouseImageModel extends Equatable {
  PartsHouseImageModel({
    required this.partsHouseImage,
    required this.partsHouseImageId,
    required this.partsHouseDescription,
  });

  factory PartsHouseImageModel.fromJson(Map<String, dynamic> json) =>
      _$PartsHouseImageModelFromJson(json);
  Map<String, dynamic> toJson() => _$PartsHouseImageModelToJson(this);

  @JsonKey(name: 'parts_house_image')
  final String partsHouseImage;
  @JsonKey(name: 'parts_house_image_id')
  final String partsHouseImageId;
  @JsonKey(name: 'parts_house_description')
  final String partsHouseDescription;

  @override
  List<Object?> get props => [];
}
