import 'property_immovable_model.dart';

class ScreenArgumentsPhotoViewFullScreen {
  ScreenArgumentsPhotoViewFullScreen(
      {required this.seletedDefaultIndex, required this.partsHouseImages});
  final int seletedDefaultIndex;
  final List<PartsHouseImageModel> partsHouseImages;
}
