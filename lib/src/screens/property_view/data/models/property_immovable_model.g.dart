// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'property_immovable_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetImmovableResponseModel _$GetImmovableResponseModelFromJson(
        Map<String, dynamic> json) =>
    GetImmovableResponseModel(
      ok: json['ok'] as bool,
      doc: json['doc'] == null
          ? null
          : DocImmovableModel.fromJson(json['doc'] as Map<String, dynamic>),
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GetImmovableResponseModelToJson(
        GetImmovableResponseModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'doc': instance.doc,
      'error': instance.error,
    };

DocImmovableModel _$DocImmovableModelFromJson(Map<String, dynamic> json) =>
    DocImmovableModel(
      immovable:
          ImmovableModel.fromJson(json['immovable'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DocImmovableModelToJson(DocImmovableModel instance) =>
    <String, dynamic>{
      'immovable': instance.immovable,
    };

ImmovableModel _$ImmovableModelFromJson(Map<String, dynamic> json) =>
    ImmovableModel(
      immovableId: json['immovable_id'] as String,
      description: json['description'] as String,
      characteristic: json['characteristic'] as String,
      detail: json['detail'] as String,
      state: json['state'] as String,
      price: json['price'] as int,
      publishedDate: DateTime.parse(json['published_date'] as String),
      street: json['street'] as String,
      city: json['city'] as String,
      immovableImages: ImmovableimageModel.fromJson(
          json['immovable_images'] as Map<String, dynamic>),
      creator: DocChatUserImmovableModel.fromJson(
          json['creator'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ImmovableModelToJson(ImmovableModel instance) =>
    <String, dynamic>{
      'immovable_id': instance.immovableId,
      'description': instance.description,
      'characteristic': instance.characteristic,
      'detail': instance.detail,
      'state': instance.state,
      'price': instance.price,
      'published_date': instance.publishedDate.toIso8601String(),
      'street': instance.street,
      'city': instance.city,
      'immovable_images': instance.immovableImages,
      'creator': instance.creator,
    };

DocChatUserImmovableModel _$DocChatUserImmovableModelFromJson(
        Map<String, dynamic> json) =>
    DocChatUserImmovableModel(
      userId: json['user_id'] as String,
      email: json['email'] as String,
      image: json['image'] as String?,
      name: json['name'] as String,
      pLastName: json['p_lastname'] as String,
      sLastName: json['s_lastname'] as String?,
      online: json['online'] as bool,
      lastMessage: json['last_message'] as String?,
      immovableId: json['immovable_id'] as String,
      rol: json['rol'] as String,
    );

Map<String, dynamic> _$DocChatUserImmovableModelToJson(
        DocChatUserImmovableModel instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'email': instance.email,
      'image': instance.image,
      'name': instance.name,
      'p_lastname': instance.pLastName,
      's_lastname': instance.sLastName,
      'online': instance.online,
      'last_message': instance.lastMessage,
      'immovable_id': instance.immovableId,
      'rol': instance.rol,
    };

ImmovableimageModel _$ImmovableimageModelFromJson(Map<String, dynamic> json) =>
    ImmovableimageModel(
      immovableImageId: json['immovable_image_id'] as String,
      outsideFrontImage: json['outside_front_image'] as String,
      partsHouseImages: (json['parts_house_images'] as List<dynamic>)
          .map((e) => PartsHouseImageModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      partsHouseCharacteristicImages:
          (json['parts_house_characteristic_images'] as List<dynamic>)
              .map((e) => PartsHouseCharacteristicImageModel.fromJson(
                  e as Map<String, dynamic>))
              .toList(),
    );

Map<String, dynamic> _$ImmovableimageModelToJson(
        ImmovableimageModel instance) =>
    <String, dynamic>{
      'immovable_image_id': instance.immovableImageId,
      'outside_front_image': instance.outsideFrontImage,
      'parts_house_images': instance.partsHouseImages,
      'parts_house_characteristic_images':
          instance.partsHouseCharacteristicImages,
    };

PartsHouseCharacteristicImageModel _$PartsHouseCharacteristicImageModelFromJson(
        Map<String, dynamic> json) =>
    PartsHouseCharacteristicImageModel(
      partsHouseCharacteristic: json['parts_house_characteristic'] as String,
      partsHouseCharacteristicImage:
          json['parts_house_characteristic_image'] as String,
      partsHouseCharacteristicImageId:
          json['parts_house_characteristic_image_id'] as String,
    );

Map<String, dynamic> _$PartsHouseCharacteristicImageModelToJson(
        PartsHouseCharacteristicImageModel instance) =>
    <String, dynamic>{
      'parts_house_characteristic': instance.partsHouseCharacteristic,
      'parts_house_characteristic_image':
          instance.partsHouseCharacteristicImage,
      'parts_house_characteristic_image_id':
          instance.partsHouseCharacteristicImageId,
    };

PartsHouseImageModel _$PartsHouseImageModelFromJson(
        Map<String, dynamic> json) =>
    PartsHouseImageModel(
      partsHouseImage: json['parts_house_image'] as String,
      partsHouseImageId: json['parts_house_image_id'] as String,
      partsHouseDescription: json['parts_house_description'] as String,
    );

Map<String, dynamic> _$PartsHouseImageModelToJson(
        PartsHouseImageModel instance) =>
    <String, dynamic>{
      'parts_house_image': instance.partsHouseImage,
      'parts_house_image_id': instance.partsHouseImageId,
      'parts_house_description': instance.partsHouseDescription,
    };
