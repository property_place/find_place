import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;

import '../data/models/property_immovable_model.dart';
import '../data/repositories/immovable_repository.dart';
import 'property_immovable_state.dart';

part 'property_immovable_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************
final client = http.Client();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************
final resolverPropertyImm =
    StateNotifierProvider<PropertyImmovableNotifier, PropertyImmovableState>(
  (ref) => PropertyImmovableNotifier(
      propertyImmovableRepository: ref.watch(_propertyImmovableRepository)),
);

// *********************************************************************
// => Repository
// *********************************************************************

final _propertyImmovableRepository = Provider<IPropertyImmovableRepository>(
  (ref) => PropertyImmovableRepository(client),
);

/*
 * Informacion del Creador del Inmueble
 */

final rCreatorImmovable = StateProvider<DocChatUserImmovableModel?>((ref) {
  return ref.watch(
    resolverPropertyImm.select(
      (value) => value.immovable?.creator,
    ),
  );
});

/*
 * Imagesnes de Inmueble
 */
final rPartsHouseImages = StateProvider<List<PartsHouseImageModel>?>((ref) {
  return ref.watch(
    resolverPropertyImm.select(
      (value) => value.immovable?.immovableImages.partsHouseImages,
    ),
  );
});

final rCharacteristicImages =
    StateProvider<List<PartsHouseCharacteristicImageModel>?>((ref) {
  return ref.watch(
    resolverPropertyImm.select(
      (value) =>
          value.immovable?.immovableImages.partsHouseCharacteristicImages,
    ),
  );
});

// * Imagen Principal

final rOutsideFrontImage = StateProvider<String>((ref) {
  return ref.watch(
    resolverPropertyImm.select(
      (value) => value.immovable!.immovableImages.outsideFrontImage,
    ),
  );
});
