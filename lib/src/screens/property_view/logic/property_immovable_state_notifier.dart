part of 'property_immovable_provider.dart';

class PropertyImmovableNotifier extends StateNotifier<PropertyImmovableState> {
  PropertyImmovableNotifier({
    required IPropertyImmovableRepository propertyImmovableRepository,
  })  : _propertyImmovableRepository = propertyImmovableRepository,
        super(PropertyImmovableState()) {
    initPropertyImmovable();
  }

  final IPropertyImmovableRepository _propertyImmovableRepository;

  void initPropertyImmovable({bool? delay}) async {
    try {
      state = state.copyWith(isLoading: true);
      final immovable = await _propertyImmovableRepository.getPropertyImmovables('f1851e0e-31d0-415f-9ff8-153c00848ef4');
      state = state.copyWith(
        isLoading: false,
        immovable: immovable.doc?.immovable,
        isError: false,
        partsHouseImageView: immovable.doc?.immovable.immovableImages.partsHouseImages.first
      );
    } on Exception {
      if (delay != null) await Future.delayed(Duration(milliseconds: 2000));
      state = state.copyWith(
        isLoading: false,
        isError: true,
      );
    }
  }

  void selectedImageView([String? code]) {
    final partsHouseImage = state.immovable?.immovableImages.partsHouseImages
        .where((element) => element.partsHouseImageId == code);
    if (partsHouseImage != null) {
      state = state.copyWith(partsHouseImageView: partsHouseImage.first);
    }
  }

}
