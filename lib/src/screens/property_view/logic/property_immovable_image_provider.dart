import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../core/framework/colors.dart';

part 'property_immovable_image_notifier.dart';

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************
final controllerSlideImage = ChangeNotifierProvider<ControllerSlideImage>(
  (ref) => ControllerSlideImage(),
);

