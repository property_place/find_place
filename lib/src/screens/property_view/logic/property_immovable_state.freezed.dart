// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'property_immovable_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$PropertyImmovableStateTearOff {
  const _$PropertyImmovableStateTearOff();

  _PropertyImmovableState call(
      {ImmovableModel? immovable,
      bool isLoading = true,
      bool isError = false,
      PartsHouseImageModel? partsHouseImageView}) {
    return _PropertyImmovableState(
      immovable: immovable,
      isLoading: isLoading,
      isError: isError,
      partsHouseImageView: partsHouseImageView,
    );
  }
}

/// @nodoc
const $PropertyImmovableState = _$PropertyImmovableStateTearOff();

/// @nodoc
mixin _$PropertyImmovableState {
  ImmovableModel? get immovable => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isError => throw _privateConstructorUsedError;
  PartsHouseImageModel? get partsHouseImageView =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PropertyImmovableStateCopyWith<PropertyImmovableState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PropertyImmovableStateCopyWith<$Res> {
  factory $PropertyImmovableStateCopyWith(PropertyImmovableState value,
          $Res Function(PropertyImmovableState) then) =
      _$PropertyImmovableStateCopyWithImpl<$Res>;
  $Res call(
      {ImmovableModel? immovable,
      bool isLoading,
      bool isError,
      PartsHouseImageModel? partsHouseImageView});
}

/// @nodoc
class _$PropertyImmovableStateCopyWithImpl<$Res>
    implements $PropertyImmovableStateCopyWith<$Res> {
  _$PropertyImmovableStateCopyWithImpl(this._value, this._then);

  final PropertyImmovableState _value;
  // ignore: unused_field
  final $Res Function(PropertyImmovableState) _then;

  @override
  $Res call({
    Object? immovable = freezed,
    Object? isLoading = freezed,
    Object? isError = freezed,
    Object? partsHouseImageView = freezed,
  }) {
    return _then(_value.copyWith(
      immovable: immovable == freezed
          ? _value.immovable
          : immovable // ignore: cast_nullable_to_non_nullable
              as ImmovableModel?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: isError == freezed
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      partsHouseImageView: partsHouseImageView == freezed
          ? _value.partsHouseImageView
          : partsHouseImageView // ignore: cast_nullable_to_non_nullable
              as PartsHouseImageModel?,
    ));
  }
}

/// @nodoc
abstract class _$PropertyImmovableStateCopyWith<$Res>
    implements $PropertyImmovableStateCopyWith<$Res> {
  factory _$PropertyImmovableStateCopyWith(_PropertyImmovableState value,
          $Res Function(_PropertyImmovableState) then) =
      __$PropertyImmovableStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {ImmovableModel? immovable,
      bool isLoading,
      bool isError,
      PartsHouseImageModel? partsHouseImageView});
}

/// @nodoc
class __$PropertyImmovableStateCopyWithImpl<$Res>
    extends _$PropertyImmovableStateCopyWithImpl<$Res>
    implements _$PropertyImmovableStateCopyWith<$Res> {
  __$PropertyImmovableStateCopyWithImpl(_PropertyImmovableState _value,
      $Res Function(_PropertyImmovableState) _then)
      : super(_value, (v) => _then(v as _PropertyImmovableState));

  @override
  _PropertyImmovableState get _value => super._value as _PropertyImmovableState;

  @override
  $Res call({
    Object? immovable = freezed,
    Object? isLoading = freezed,
    Object? isError = freezed,
    Object? partsHouseImageView = freezed,
  }) {
    return _then(_PropertyImmovableState(
      immovable: immovable == freezed
          ? _value.immovable
          : immovable // ignore: cast_nullable_to_non_nullable
              as ImmovableModel?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: isError == freezed
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      partsHouseImageView: partsHouseImageView == freezed
          ? _value.partsHouseImageView
          : partsHouseImageView // ignore: cast_nullable_to_non_nullable
              as PartsHouseImageModel?,
    ));
  }
}

/// @nodoc

class _$_PropertyImmovableState extends _PropertyImmovableState {
  const _$_PropertyImmovableState(
      {this.immovable,
      this.isLoading = true,
      this.isError = false,
      this.partsHouseImageView})
      : super._();

  @override
  final ImmovableModel? immovable;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final bool isError;
  @override
  final PartsHouseImageModel? partsHouseImageView;

  @override
  String toString() {
    return 'PropertyImmovableState(immovable: $immovable, isLoading: $isLoading, isError: $isError, partsHouseImageView: $partsHouseImageView)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _PropertyImmovableState &&
            const DeepCollectionEquality().equals(other.immovable, immovable) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality().equals(other.isError, isError) &&
            const DeepCollectionEquality()
                .equals(other.partsHouseImageView, partsHouseImageView));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(immovable),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isError),
      const DeepCollectionEquality().hash(partsHouseImageView));

  @JsonKey(ignore: true)
  @override
  _$PropertyImmovableStateCopyWith<_PropertyImmovableState> get copyWith =>
      __$PropertyImmovableStateCopyWithImpl<_PropertyImmovableState>(
          this, _$identity);
}

abstract class _PropertyImmovableState extends PropertyImmovableState {
  const factory _PropertyImmovableState(
      {ImmovableModel? immovable,
      bool isLoading,
      bool isError,
      PartsHouseImageModel? partsHouseImageView}) = _$_PropertyImmovableState;
  const _PropertyImmovableState._() : super._();

  @override
  ImmovableModel? get immovable;
  @override
  bool get isLoading;
  @override
  bool get isError;
  @override
  PartsHouseImageModel? get partsHouseImageView;
  @override
  @JsonKey(ignore: true)
  _$PropertyImmovableStateCopyWith<_PropertyImmovableState> get copyWith =>
      throw _privateConstructorUsedError;
}
