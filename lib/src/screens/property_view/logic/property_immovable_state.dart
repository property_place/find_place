import 'package:freezed_annotation/freezed_annotation.dart';

import '../data/models/property_immovable_model.dart';

part 'property_immovable_state.freezed.dart';

@freezed
abstract class PropertyImmovableState with _$PropertyImmovableState {
  const factory PropertyImmovableState({
    ImmovableModel? immovable,
    @Default(true) bool isLoading,
    @Default(false) bool isError,
    PartsHouseImageModel? partsHouseImageView,
  }) = _PropertyImmovableState;

  const PropertyImmovableState._();
}
