part of 'property_immovable_image_provider.dart';

class ControllerSlideImage extends ChangeNotifier {
  double _currentPage = 0;

  double get currentPage => _currentPage;

  set currentPage(double currentPage) {
    _currentPage = currentPage;
    notifyListeners();
  }

  Color _primaryColor = primary;
  // ignore: unnecessary_getters_setters
  Color get primaryColor => _primaryColor;
  // ignore: unnecessary_getters_setters
  set primaryColor(Color primaryColor) {
    _primaryColor = primaryColor;
  }

  Color _secundaryColor = Colors.grey;
  // ignore: unnecessary_getters_setters
  Color get secundaryColor => _secundaryColor;
  // ignore: unnecessary_getters_setters
  set secundaryColor(Color secundaryColor) {
    _secundaryColor = secundaryColor;
  }

  double _bulletPrimary = 12;
  // ignore: unnecessary_getters_setters
  double get bulletPrimary => _bulletPrimary;
  // ignore: unnecessary_getters_setters
  set bulletPrimary(double bulletPrimary) {
    _bulletPrimary = bulletPrimary;
  }

  double _bulletSecundary = 12;
  // ignore: unnecessary_getters_setters
  double get bulletSecundary => _bulletSecundary;
  // ignore: unnecessary_getters_setters
  set bulletSecundary(double bulletSecundary) {
    _bulletSecundary = bulletSecundary;
  }
}
