import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../widgets/center_indicator.dart';
import '../../../widgets/custom_imput_search.dart';
import '../../data/models/cities/api_cities_model.dart';
import '../../hooks/cities_scroll_controller.dart';
import '../../logic/cities/cities_provider.dart';

class DetailCityListScreen extends StatelessWidget {
  const DetailCityListScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primary,
      body: SafeArea(
          child: Column(
        children: [
          ViewAppBarStoreSearchDetail(),
          Expanded(child: _ListDetailCities()),
        ],
      )),
    );
  }
}

class _ListDetailCities extends HookConsumerWidget {
  const _ListDetailCities({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //OBTENEMOS LA CATEGORIA ATRAVES DE LA CATEGORIA SELECCIONADA
    final _cities = ref.watch(citiesProvider.notifier).state;
    final _isLoading = ref.watch(citiesNotifierProvider).isLoading;

    final _controller = useCitiesScrollController(ref, oldLength: _cities.length);
    final isLoadMoreError = ref.watch(citiesNotifierProvider).isLoadMoreError;
    final isLoadMoreDone = ref.watch(citiesNotifierProvider).isLoadMoreDone;

    late Widget detail;
    if (_cities.isEmpty) {
      // error case
      if (_isLoading == false) {
        detail = const Center(
          child: Text('error'),
        );
      }
      detail = const CenterIndicator();
    }
    detail = RefreshIndicator(
      color: oscuro,
      strokeWidth: 2,
      onRefresh: () {
        return ref.read(citiesNotifierProvider.notifier).refresh();
      },
      child: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowIndicator();
          return true;
        },
        child: ListView.separated(
            controller: _controller,
            physics: ClampingScrollPhysics(),
            itemBuilder: (_, index) {
              if (index == _cities.length) {
                // load more and get error
                if (isLoadMoreError.isNotEmpty) {
                  return _ErrorCityLoad(isLoadMoreError);
                }
                // load more but reached to the last element
                if (isLoadMoreDone) {
                  return const Center(
                    child: Text(
                      'Done!',
                      style: TextStyle(color: Colors.green, fontSize: 20),
                    ),
                  );
                }
                return _isLoading
                    ? Container(
                        height: _cities.isEmpty
                            ? MediaQuery.of(context).size.height * 0.7
                            : 50,
                        alignment: Alignment.center,
                        child: const CenterIndicator())
                    : Container();
              }
              return _CityDetail(cities: _cities[index]);
            },
            separatorBuilder: (context, index) => Divider(
                  color: Colors.black,
                ),
            itemCount: _cities.length + 1),
      ),
    );

    return detail;
  }
}

class _CityDetail extends StatelessWidget {
  const _CityDetail({
    Key? key,
    required DocApiCitiesModel cities,
  })  : _cities = cities,
        super(key: key);

  final DocApiCitiesModel _cities;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
          title:
              Container(alignment: Alignment.center, child: Text(_cities.city)),
          onTap: () => Navigator.pop(context, _cities.city)),
    );
  }
}

class ViewAppBarStoreSearchDetail extends StatelessWidget {
  const ViewAppBarStoreSearchDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primary,
      height: 100,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          BackButton(
            onPressed: () => Navigator.pop(context),
          ),
          Expanded(child: SearchViewCityProduct())
        ],
      ),
    );
  }
}

class SearchViewCityProduct extends StatelessWidget {
  SearchViewCityProduct({
    Key? key,
  }) : super(key: key);

  final emailCRTL = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (ctx, ref, child) {
      return Container(
        height: 100,
        padding: const EdgeInsets.only(left: 20, top: 15),
        child: TextFieldSearch(
          controller: emailCRTL,
          name: 'Ciudades',
          prefixIcon: const Icon(Ionicons.search, color: oscuro),
          maxLines: 1,
          type: TextInputType.text,
          padding: 5,
          color: oscuro,
          colorOP: 1,
          radius: 50,
          onChanged: (value) =>
              ref.read(citiesNotifierProvider.notifier).loadSearchCities(value),
        ),
      );
    });
  }
}

class _ErrorCityLoad extends StatelessWidget {
  const _ErrorCityLoad(this._error, {Key? key}) : super(key: key);
  final String _error;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.7,
        alignment: Alignment.center,
        child: Text(
          _error.toString(),
          style: styleTituloItalicW600.copyWith(fontSize: 13),
        ));
  }
}
