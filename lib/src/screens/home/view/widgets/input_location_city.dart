import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../routes/app_routes.dart';
import '../../logic/cities/cities_provider.dart';

class InputLocationCity extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _cityCRT = ref.watch(seletedCityCRT);
    // TODO initialValue del usuario por defecto city
    return TextFormField(
      readOnly: true,
      controller: _cityCRT,
      onTap: () async {
        final result = await Navigator.pushNamed(context, Routes.location_city);
        if(result != null ) _cityCRT.text = result as String;
      },
      decoration: InputDecoration(
        hintText: 'Selecciona tu ubicacion?',
        labelText: 'Ubicación',
        labelStyle: styleTituloNormal.copyWith(color: oscuro.withOpacity(0.4)),
        focusColor: oscuro,
        suffixIcon: const Icon(
          Ionicons.location_outline,
          color: oscuro,
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: oscuro.withOpacity(0.5)),
        ),
      ),
    );
  }
}
