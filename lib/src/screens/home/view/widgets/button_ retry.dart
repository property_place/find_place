import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';

class ButtonRetryLoad extends StatelessWidget {
  const ButtonRetryLoad(
      {Key? key, required this.onPressed, this.btnTitle = 'Reintentar'})
      : super(key: key);
  final void Function() onPressed;
  final String btnTitle;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
          overlayColor:
              MaterialStateProperty.all<Color>(oscuro.withOpacity(0.1)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: primary)))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Ionicons.reload_outline, color: oscuro),
          SizedBox(width: 10.0),
          Text('Reintentar',
              style: styleTituloItalicW600.copyWith(color: oscuro)),
        ],
      ),
      onPressed: onPressed,
    );
  }
}
