import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../../core/framework/colors.dart';
import '../../../../../core/framework/tipografia.dart';
import '../../../logic/filters/filters_provider.dart';
import '../../../utils/type_filter.dart';

class FilterListItemOrder extends ConsumerWidget {
  const FilterListItemOrder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    ref.listen<DateTimeRange>(dataRangeProvider, (previousDataRange, newDataRange) {
      final start = newDataRange.start;
      final end = newDataRange.end;
      final startDate = '${start.year}/${start.month}/${start.day}';
      final endDate = '${end.year}/${end.month}/${end.day}';
      if (end.isAfter(start)) {
        ref.watch(filtersNotifierProvider.notifier).filterDateStart(startDate);
        ref.watch(filtersNotifierProvider.notifier).filterDateEnd(endDate);
      }
    });

 
    return Consumer(
      builder: (context, ref, child) {
        final order = ref.watch(orderNotifierProvider.notifier).state;
        return Container(
            padding: const EdgeInsets.only(top: 20.0),
            child: Container(
              width: size.width,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'order',
                    style: styleTituloNormal,
                  ),
                  Wrap(
                      children: order
                          .map((e) => Padding(
                                padding: const EdgeInsets.only(right: 5.0),
                                child: FilterChip(
                                    backgroundColor: e.active ? oscuro : claro,
                                    label: Text(
                                      '${e.name}',
                                      style: styleTituloNormal.copyWith(
                                          color: e.active ? claro : oscuro,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    onSelected: (value) => ref
                                        .read(filtersNotifierProvider.notifier)
                                        .filterOrder(
                                            type: TypeFilter.order.name,
                                            name: e.name!)),
                              ))
                          .toList()),
                  SizedBox(height: 20.0),
                ],
              ),
            ));
      },
    );
  }
}
