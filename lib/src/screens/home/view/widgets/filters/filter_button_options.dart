import 'package:flutter/material.dart';

import '../../../../../core/framework/colors.dart';
import '../../../../../core/framework/tipografia.dart';

class FilterButtonOptionStyle extends StatelessWidget {
  const FilterButtonOptionStyle({
    Key? key,
    required this.title,
    required this.onPressed,
  }) : super(key: key);

  final String title;
  final void Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: ButtonStyle(
            overlayColor:
                MaterialStateProperty.all<Color>(oscuro.withOpacity(0.1)),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ))),
        onPressed: onPressed,
        child: Text(
          title,
          style: styleTituloNormal.copyWith(color: oscuro),
        ));
  }
}
