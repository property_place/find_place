import 'package:flutter/material.dart';

import '../../../../../core/framework/colors.dart';
import '../../../../../core/framework/tipografia.dart';

class FilterInput extends StatelessWidget {
  const FilterInput(
      {Key? key,
      required this.hintText,
      required this.labelText,
      this.width = 0,
      this.onChanged,
      this.controller,
      this.enabled = true,
      this.readOnly = false})
      : super(key: key);

  final String hintText;
  final String labelText;
  final Function(String)? onChanged;
  final TextEditingController? controller;
  final double width;
  final bool? enabled;
  final bool? readOnly;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final widthSize = width == 0 ? size.width * 0.4 : width;
    return Container(
      width: widthSize,
      child: TextFormField(
        readOnly: readOnly!,
        enabled: enabled,
        controller: controller,
        onChanged: onChanged,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: TextStyle(fontSize: 7),
          labelText: labelText,
          contentPadding: EdgeInsets.all(2.0),
          labelStyle: styleTituloNormal.copyWith(
            color: oscuro.withOpacity(0.4),
            fontSize: 12,
          ),
          focusColor: oscuro,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: oscuro.withOpacity(0.5)),
          ),
        ),
      ),
    );
  }
}
