import 'package:flutter/material.dart';

import '../../../../../core/framework/colors.dart';
import '../../../data/models/filters/filters_model.dart';
import 'filter_appbar.dart';
import 'filter_button_optionss.dart';
import 'filter_date_range.dart';
import 'filter_list_item_order.dart';
import 'filter_price_input.dart';

class FilterImmovablesScreen extends StatelessWidget {
  FilterImmovablesScreen({Key? key}) : super(key: key);
  final List<FilterImmovables>? selectedUserList = [];
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: primary,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0), // here the desired height
        child: FilterAppBar(),
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
          width: size.width,
          child: Column(
            children: [
              Expanded(
                child: Container(
                  width: size.width,
                  child: NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overscroll) {
                      overscroll.disallowIndicator();
                      return true;
                    },
                    child: ListView(
                      children: [
                        Column(
                          children: [
                            FilterListItemOrder(),
                            FilterPriceImput(),
                            SizedBox(height: 20.0),
                            FilterDateRangeImput()
                          ],
                        ),
                        SizedBox(height: 20.0)
                      ],
                    ),
                  ),
                ),
              ),
              FilterOptions()
            ],
          ),
        ),
      ),
    );
  }
}
