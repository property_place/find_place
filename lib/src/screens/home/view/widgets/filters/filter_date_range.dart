import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../../core/framework/colors.dart';
import '../../../../../core/framework/tipografia.dart';
import '../../../logic/filters/filters_provider.dart';
import 'filter_input_price.dart';

// ignore: public_member_api_docs, must_be_immutable
class FilterDateRangeImput extends ConsumerWidget {
  FilterDateRangeImput({
    Key? key,
  }) : super(key: key);

  String oldDateStart = '';
  String oldDateEnd = '';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final dataRange = ref.watch(dataRangeProvider..notifier);
    final start = dataRange.start;
    final end = dataRange.end;
    final startDate = '${start.year}-${start.month}-${start.day}';
    final endDate = '${end.year}-${end.month}-${end.day}';

    final startCRT = ref.watch(startProvider(startDate).notifier).state;
    final endCRT = ref.watch(endProvider(endDate).notifier).state;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Fechas',
            style: styleTituloNormal.copyWith(
                color: oscuro, fontWeight: FontWeight.w500),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FilterInput(
                labelText: 'Fecha inicial',
                hintText: '',
                controller: startCRT,
                width: MediaQuery.of(context).size.width * 0.3,
                enabled: false,
              ),
              FilterInput(
                labelText: 'Fecha final',
                hintText: '',
                controller: endCRT,
                enabled: false,
                width: MediaQuery.of(context).size.width * 0.3,
              ),
              Container(
                margin: const EdgeInsets.only(top: 5),
                decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(5),
                      bottomRight: Radius.circular(5),
                    )),
                child: IconButton(
                    onPressed: () => pickDateRange(context, ref, dataRange),
                    icon: Icon(Ionicons.calculator_outline)),
              )
            ],
          ),
        ],
      ),
    );
  }

  void pickDateRange(
      BuildContext context, WidgetRef ref, DateTimeRange? dataRange) async {
    final newRange = await showDateRangePicker(
      context: context,
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
      initialDateRange: dataRange,
      saveText: 'Guardar',
      builder: (context, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColorLight: primary,
            // Color de fondo del encabezado
            primaryColor: primary,
            // Color de fondo
            scaffoldBackgroundColor: primary,
            //Divider color
            dividerColor: grey,
            //Non selected days of the month color
            textTheme: TextTheme(
              bodyText2: TextStyle(color: oscuro),
            ),
            colorScheme: ColorScheme.fromSwatch().copyWith(
              //Selected dates background color
              primary: oscuro,
              //Month title and week days color
              onSurface: Colors.black,
              //Header elements and selected dates text color
              //onPrimary: Colors.white,
            ),
          ),
          child: child!,
        );
      },
    );

    if (newRange == null) return;
    ref.read(dataRangeProvider.notifier).state = newRange;
  }
}
