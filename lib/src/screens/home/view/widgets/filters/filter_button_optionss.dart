import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../../core/framework/colors.dart';
import '../../../logic/filters/filters_provider.dart';
import '../../../logic/filters/filters_state.dart';
import '../../../logic/immovables/immovables_provider.dart';
import 'filter_button_options.dart';

class FilterOptions extends StatelessWidget {
  const FilterOptions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 5),
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          border: Border.all(color: oscuro.withOpacity(0.1))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Consumer(
            builder: (context, ref, child) {
              return FilterButtonOptionStyle(
                  title: 'Limpiar', onPressed: () => filterReset(ref));
            },
          ),
          Consumer(
            builder: (context, ref, child) {
              final filters = ref.watch(filtersNotifierProvider);
              return FilterButtonOptionStyle(
                  title: 'Aplicar',
                  onPressed: () => filterApply(
                        context: context,
                        ref: ref,
                        filters: filters,
                      ));
            },
          ),
        ],
      ),
    );
  }

  void filterReset(
    WidgetRef ref,
  ) {
    ref.read(filtersNotifierProvider.notifier).filterReset();
    ref.read(immovablesNotifierProvider.notifier).filterReset();
    ref.read(maximumPriceCRTProvider).clear();
    ref.read(minimumPriceCRTProvider).clear();
    ref.read(dateStartDateCRTProvider).clear();
    ref.read(dateEndDateCRTProvider).clear();

    final now = DateTime.now();
    final dateDefault = DateTimeRange(
        start: DateTime(now.year, now.month, now.day),
        end: DateTime(now.year, now.month, now.day));
    ref.read(dataRangeProvider.notifier).state = dateDefault;
  }

  void filterApply(
      {required BuildContext context,
      required WidgetRef ref,
      required FiltersState filters}) {
        
    final isFilterActive = filters.order.map((e) => e.active == true);
    final isFilterPriceMax = filters.betweenDates?.maximum.isNotEmpty;
    final isFilterPriceMin = filters.betweenDates?.maximum.isNotEmpty;
    final isFilterDateMax = filters.betweenDates?.maximum.isNotEmpty;
    final isFilterDateMin = filters.betweenDates?.minimum.isNotEmpty;

    if (isFilterActive.isNotEmpty ||
        isFilterPriceMax! ||
        isFilterPriceMin! ||
        isFilterDateMax! ||
        isFilterDateMin!) {
      ref
          .read(immovablesNotifierProvider.notifier)
          .initImmovables(filters: filters);
    }
    Navigator.pop(context);
  }
}
