import 'package:flutter/material.dart';

import '../../../../../core/framework/colors.dart';
import '../../../../../core/framework/tipografia.dart';

class FilterAppBar extends StatelessWidget {
  const FilterAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: oscuro,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            BackButton(
              onPressed: () => Navigator.pop(context),
              color: claro,
            ),
            Text(
              'Filtrar por',
              style: styleTituloItalicW600.copyWith(color: claro),
            ),
          ],
        ),
      ),
    );
  }
}
