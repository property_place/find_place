import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../../core/framework/colors.dart';
import '../../../../../core/framework/tipografia.dart';
import '../../../logic/filters/filters_provider.dart';
import 'filter_input_price.dart';

class FilterPriceImput extends StatelessWidget {
  const FilterPriceImput({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Precio',
            style: styleTituloNormal.copyWith(
                color: oscuro, fontWeight: FontWeight.w500),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Consumer(
                builder: (context, ref, child) {
                  final minimumCRT = ref.watch(minimumPriceCRTProvider);
                  return FilterInput(
                    labelText: 'Minimo',
                    hintText: '10.000.000',
                    controller: minimumCRT,
                    onChanged: (value) => ref
                        .read(filtersNotifierProvider.notifier)
                        .filterPriceMinimum(value),
                  );
                },
              ),
              Consumer(
                builder: (context, ref, child) {
                  final maximumCRT = ref.watch(maximumPriceCRTProvider);
                  return FilterInput(
                    labelText: 'Maximo',
                    hintText: '30.000.000',
                    controller: maximumCRT,
                    onChanged: (value) => ref
                        .read(filtersNotifierProvider.notifier)
                        .filterPriceMaximum(value),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
