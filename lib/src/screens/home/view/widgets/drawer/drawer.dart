import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../../core/framework/colors.dart';
import '../../../../../core/framework/tipografia.dart';
import '../../../../../routes/app_routes.dart';
import '../../../../../service/logic/socket_provider.dart';
import '../../../../auth/logic/auth_provider.dart';
import '../../../../widgets/avater_image_circle.dart';
import '../../../../widgets/center_indicator.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        child: Padding(
          padding: EdgeInsets.only(top: 50.0),
          child: Consumer(
            builder: (context, ref, child) {
              final _userService = ref.watch(userProvider);

              //TODO: VALIDAR => infoOfTheConnectedPersonUser
              if (_userService == null) {
                return CenterIndicator();
              }

              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      width: 80,
                      height: 80,
                      child: _userService.image == null
                          ? CircleAvatar(
                              backgroundColor: secundary,
                              child: Text(
                                _userService.name[0].toUpperCase(),
                                style: styleTituloItalicW600.copyWith(
                                    color: claro, fontSize: 20),
                              ),
                            )
                          : AvaterCircleImage(
                              imageUrl: _userService.image!,
                              childAlternative: CircleAvatar(
                                backgroundColor: secundary,
                                child: Text(
                                  _userService.name[0].toUpperCase(),
                                  style: styleTituloItalicW600.copyWith(
                                      color: claro, fontSize: 20),
                                ),
                              ),
                            )),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    '${_userService.name} ${_userService.pLastName}',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    _userService.rol,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
      SizedBox(
        height: 20.0,
      ),
      //Now let's Add the button for the Menu
      //and let's copy that and modify it
      ListTile(
        onTap: () {},
        leading: Icon(
          Icons.person_outlined,
          color: Colors.black,
        ),
        title: Text("Tu perfil", style: styleTituloNormal),
      ),

      ListTile(
        onTap: () => Navigator.pushNamed(context, Routes.profile_chat_list),
        leading: Icon(
          Icons.shopping_bag_outlined,
          color: Colors.black,
        ),
        title: Text("Mis chats", style: styleTituloNormal),
      ),

      // ListTile(
      //   onTap: () {},
      //   leading: Icon(
      //     Icons.assessment,
      //     color: Colors.black,
      //   ),
      //   title: Text("Your Dashboard"),
      // ),

      ListTile(
        onTap: () {},
        leading: Icon(
          Icons.person_add_outlined,
          color: Colors.black,
        ),
        title: Text("Cuentas", style: styleTituloNormal),
      ),

      ListTile(
        onTap: () {},
        leading: Icon(
          Icons.help_outline_rounded,
          color: Colors.black,
        ),
        title: Text("Ayuda", style: styleTituloNormal),
      ),

      ListTile(
        onTap: () {},
        leading: Icon(
          Icons.settings_outlined,
          color: Colors.black,
        ),
        title: Text("Configuraciones", style: styleTituloNormal),
      ),
      Spacer(),
      _Logged(),
      SizedBox(
        height: 40.0,
      ),
    ]);
  }
}

class _Logged extends StatelessWidget {
  const _Logged({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
        builder: (context, ref, child) => ListTile(
              onTap: () => logout(
                context,
                ref: ref,
              ),
              trailing: Container(
                padding: const EdgeInsets.only(right: 20),
                child: Icon(
                  Ionicons.log_out_outline,
                  color: Colors.black,
                ),
              ),
              title: Text("Cerar sesión", style: styleTituloNormal),
            ));
  }

  void logout(BuildContext context, {required WidgetRef ref}) {
    ref.read(resolverSocketService.notifier).disconnect();
    Navigator.pushReplacementNamed(context, Routes.login);
    ref.read(resolverAuthService.notifier).logout();
  }
}
