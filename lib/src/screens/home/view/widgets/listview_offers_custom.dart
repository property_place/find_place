import 'package:animate_do/animate_do.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../widgets/center_indicator.dart';
import '../../data/models/immovable_offers/immovable_offers_model.dart';
import '../../utils/price_format.dart';

class ListViewOffersItem extends StatelessWidget {
  const ListViewOffersItem(
      {Key? key,
      required List<DocOffersModel> offers,
      required ScrollController controller,
      required bool isLoading,
      required String isLoadMoreError,
      required this.onItemSelected})
      : _offers = offers,
        _controller = controller,
        _isLoading = isLoading,
        _isLoadMoreError = isLoadMoreError,
        super(key: key);
  final ValueChanged<DocOffersModel> onItemSelected;
  final List<DocOffersModel> _offers;
  final ScrollController _controller;
  final bool _isLoading;
  final String _isLoadMoreError;

  @override
  Widget build(BuildContext context) {
    late Widget detail;
    if (_offers.isEmpty) {
      // error case
      if (_isLoading == false) {
        detail = Center(
          child: Text('$_isLoadMoreError'),
        );
      }
      detail = const CenterIndicator();
    } else {
      detail = Expanded(
        child: NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowIndicator();
            return true;
          },
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            controller: _controller,
            itemCount: _offers.length,
            physics: ClampingScrollPhysics(),
            itemBuilder: (_, i) => SlideInLeft(
              child: _OfferItem(
                offer: _offers[i],
                onItemSelected: onItemSelected,
              ),
            ),
          ),
        ),
      );
    }

    return detail;
  }
}

class _OfferItem extends StatelessWidget {
  const _OfferItem({
    Key? key,
    required this.offer,
    required this.onItemSelected,
  }) : super(key: key);

  final DocOffersModel offer;
  final ValueChanged<DocOffersModel> onItemSelected;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => onItemSelected(offer),
      behavior: HitTestBehavior.translucent,
      child: Container(
        width: size.width * 0.5,
        margin: const EdgeInsets.only(right: 10),
        child: _CardOffersDetail(offer: offer),
      ),
    );
  }
}

class _CardOffersDetail extends StatelessWidget {
  const _CardOffersDetail({
    Key? key,
    required this.offer,
  }) : super(key: key);

  final DocOffersModel offer;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              width: size.width * 0.5,
              margin: const EdgeInsets.only(bottom: 0),
              alignment: Alignment.center,
              child: CachedNetworkImage(
                imageUrl: offer.outsideFrontImage,
                imageBuilder: (context, imageProvider) => Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(10)),
                ),
                placeholder: (context, url) =>
                    Center(child: CircularProgressIndicator(color: oscuro)),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          ),
          Container(
            height: 70,
            child: ListTile(
              title: Text(offer.description),
              subtitle: Text(
                '\$ ${convertPrice(offer.newPrice)}',
                style: styleTituloW600,
              ),
            ),
          )
        ],
      ),
    );
  }
}
