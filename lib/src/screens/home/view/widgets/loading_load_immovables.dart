import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../widgets/center_indicator.dart';
import '../../logic/categories/category_tab_provider.dart';
import '../../logic/immovables/immovables_provider.dart';

class LoadingLoad extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    final _stateImmovable = ref.watch(immovablesNotifierProvider);
    final _stateCategory = ref.watch(categoriesNotifierProvider);
    final _immovables = ref.watch(immovablesProvider.notifier).state;

    return _immovables.isEmpty
        ? _stateImmovable.isLoading || _stateCategory.isLoading
            ? Container(
                height: size.height - 390,
                alignment: Alignment.center,
                child: CenterIndicator(),
              )
            : Container()
        : Container();
  }
}

class LoadingLoadImmovables extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    final _stateCategory = ref.watch(categoriesNotifierProvider);

    return _stateCategory.isLoading
        ? Container(
            height: size.height - 280,
            alignment: Alignment.center,
            child: CenterIndicator(),
          )
        : Container();
  }
}
