import 'package:flutter/material.dart';

class OptionMenu extends StatelessWidget {
  const OptionMenu({Key? key, required bool isPinned})
      : _isPinned = isPinned,
        super(key: key);
  final bool _isPinned;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.topLeft,
      margin: const EdgeInsets.only(top: 30),
      child: GestureDetector(
        onTap: () => Scaffold.of(context).openDrawer(),
        behavior: HitTestBehavior.translucent,
        child: AnimatedContainer(
            width: _isPinned ? 0 : size.width * 0.12,
            height: _isPinned ? 0 : size.height * 0.1,
            duration: Duration(milliseconds: 200),
            curve: Curves.easeInToLinear,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(50),
                  bottomRight: Radius.circular(50)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2.5,
                  blurRadius: 6,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: const Icon(Icons.menu)),
      ),
    );
  }
}
