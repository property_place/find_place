import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../core/global/environment.dart';
import '../../data/models/categories/categories_model.dart';
import '../../logic/categories/category_tab_provider.dart';

class ListViewTabCUstom extends ConsumerWidget {
  const ListViewTabCUstom(
      {Key? key,
      required int selectedIndex,
      required final List<DocCategoryModel> categories})
      : _selectedIndex = selectedIndex,
        _categories = categories,
        super(key: key);
  final int _selectedIndex;
  final List<DocCategoryModel> _categories;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      height: 80,
      margin: const EdgeInsets.only(top: 10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 10.0,
            color: Colors.black12,
            offset: Offset(0, 3),
          )
        ],
      ),
      child: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowIndicator();
          return true;
        },
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _categories.length,
            physics: ClampingScrollPhysics(),
            itemBuilder: (_, i) {
              return FadeIn(
                duration: Duration(milliseconds: 200),
                child: GestureDetector(
                  onTap: () => ref
                      .read(categoryTabNotifierProvider.notifier)
                      .itemSelected(i),
                  child: _CardCategoriesDetail(
                      index: i,
                      selectedIndex: _selectedIndex,
                      categories: _categories[i]),
                ),
              );
            }),
      ),
    );
  }
}

class _CardCategoriesDetail extends StatelessWidget {
  const _CardCategoriesDetail(
      {Key? key,
      required this.selectedIndex,
      required this.categories,
      required this.index})
      : super(key: key);

  final int selectedIndex;
  final DocCategoryModel categories;
  final int index;
  @override
  Widget build(BuildContext context) {
    final color = selectedIndex == index ? claro : oscuro;

    return Card(
      color: selectedIndex == index ? oscuro : claro,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                child: SvgPicture.network(
              "${EnvironmentApi.imageHttp}${categories.image}",
              width: MediaQuery.of(context).size.width * 0.025,
              height: MediaQuery.of(context).size.height * 0.025,
              color: color,
            )),
            const SizedBox(height: 5),
            Text(
              categories.name,
              style: styleTituloNormal.copyWith(color: color, fontSize: MediaQuery.of(context).size.width * 0.035),
            )
          ],
        ),
      ),
    );
  }
}
