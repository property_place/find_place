import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../routes/app_routes.dart';
import '../../../utils/type_hero_image.dart';
import '../../../widgets/center_indicator.dart';
import '../../data/models/categories/categories_model.dart';
import '../../data/models/immovables/immovables_model.dart';
import '../../logic/immovable_offers/immovable_offers_provider.dart';
import '../../logic/immovables/immovables_provider.dart';
import '../../utils/price_format.dart';
import 'button_ retry.dart';

class ListDetailImmovableAppBar extends StatelessWidget {
  const ListDetailImmovableAppBar(
      {Key? key,
      required DocCategoryModel? selectedCategory,
      required bool isPinned})
      : _selectedCategory = selectedCategory,
        _isPinned = isPinned,
        super(key: key);

  final DocCategoryModel? _selectedCategory;
  final bool _isPinned;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      backgroundColor: _isPinned ? oscuro : primary,
      titleSpacing: 0,
      leadingWidth: !_isPinned ? 0 : 56,
      leading: _isPinned
          ? FadeIn(
              child: IconButton(
                icon: Icon(
                  Ionicons.menu,
                  color: _isPinned ? claro : oscuro,
                ),
                onPressed: () => Scaffold.of(context).openDrawer(),
              ),
            )
          : Container(
              margin: EdgeInsets.all(0),
            ),
      title: FadeIn(
        child: Container(
          height: 40,
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Text(
            'Listado de: ${_selectedCategory?.name.toLowerCase()}',
            style: styleTituloNormal.copyWith(
                color: _isPinned ? claro : oscuro,
                fontSize: 15,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
      actions: [
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, Routes.filter_immovable),
          behavior: HitTestBehavior.translucent,
          child: Container(
            width: 40,
            height: 40,
            margin: const EdgeInsets.only(right: 10),
            child: Icon(
              Ionicons.filter_outline,
              color: _isPinned ? claro : oscuro,
            ),
          ),
        )
      ],
    );
  }
}

class ListDetailImmovableBody extends ConsumerWidget {
  const ListDetailImmovableBody({
    Key? key,
    required List<DocImmovableExhibitionModel> immovables,
  })  : _immovables = immovables,
        super(key: key);

  final List<DocImmovableExhibitionModel> _immovables;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _isLoading = ref.watch(immovablesNotifierProvider).isLoading;
    final _isLoadMoreError =
        ref.watch(immovablesNotifierProvider).isLoadMoreError;
    final _isLoadMoreDone =
        ref.watch(immovablesNotifierProvider).isLoadMoreDone;

    late Widget detail;
    if (_immovables.isEmpty) {
      // error case
      if (_isLoading == false) {
        detail = SliverToBoxAdapter(
          child: Center(
            child: Text('$_isLoadMoreError'),
          ),
        );
      }
      detail = SliverToBoxAdapter(
        child: const CenterIndicator(),
      );
    }

    detail = SliverGrid(
      delegate: SliverChildBuilderDelegate((context, index) {
        if (index == _immovables.length) {
          // CARGAR O CARGAR MAS, PERO HUBO UN ERROR.
          if (!_isLoading && _isLoadMoreError.isNotEmpty) {
            return _ErrorImmovationsLoad(_isLoadMoreError);
          }

          // CARGAR O CARGAR MAS PERO LLEGO VACIO.
          if (!_isLoading && _isLoadMoreError.isEmpty && _immovables.isEmpty) {
            return _EmptyImmovationsLoad();
          }

          // CARGAR O CARGAR MAS PERO LLEGO AL ULTIMO ELEMENTO.
          WidgetsBinding.instance?.addPostFrameCallback((_) =>
              _immovables.isNotEmpty
                  ? ref
                      .read(immovablesNotifierProvider.notifier)
                      .activeLoadingLoadMore()
                  : null);

          return _immovables.isEmpty
              ? Container(
                  child: const CenterIndicator(),
                )
              : null;
        }

        return _ImmovableItem(immovable: _immovables[index], index: index);
      },
          childCount:
              _isLoadMoreDone ? _immovables.length : _immovables.length + 1),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: _isLoadMoreError.isNotEmpty ||
                  (_isLoadMoreError.isEmpty && _immovables.isEmpty)
              ? 1
              : 2,
          mainAxisSpacing: 0,
          crossAxisSpacing: 0,
          childAspectRatio: 2.0,
          mainAxisExtent: 300),
    );

    return detail;
  }
}

class _ImmovableItem extends StatelessWidget {
  const _ImmovableItem({Key? key, required this.immovable, required this.index})
      : super(key: key);

  final DocImmovableExhibitionModel immovable;
  final int index;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Consumer(
      builder: (context, ref, child) {
        return GestureDetector(
          onTap: () {
            ref.read(indexHero.state).state = index;
            Navigator.pushNamed(context, Routes.property_immovable);
          },
          behavior: HitTestBehavior.translucent,
          child: Container(
            width: size.width * 0.6,
            child: _CardImmovableDetail(immovable: immovable, index: index),
          ),
        );
      },
    );
  }
}

class _CardImmovableDetail extends StatelessWidget {
  const _CardImmovableDetail(
      {Key? key, required this.immovable, required this.index})
      : super(key: key);

  final DocImmovableExhibitionModel immovable;
  final int index;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              width: size.width * 0.7,
              margin: const EdgeInsets.only(bottom: 0),
              alignment: Alignment.center,
              child: Hero(
                tag: "${TypeHero.image_product.name + index.toString()}",
                child: CachedNetworkImage(
                  imageUrl: immovable.outsideFrontImage,
                  imageBuilder: (context, imageProvider) => Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  placeholder: (context, url) =>
                      Center(child: CircularProgressIndicator(color: oscuro)),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(left: 10, top: 5),
            child: Container(
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(8)),
                child: Text(
                  immovable.state,
                  style: styleTituloW600.copyWith(color: claro, fontSize: 10),
                )),
          ),
          Container(
            height: 70,
            child: ListTile(
              title: Text(
                immovable.description,
                style: styleTituloItalicW600,
              ),
              subtitle: Text(
                '\$ ${convertPrice(immovable.price)}',
                style: styleTituloW600.copyWith(fontSize: 13),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class _ErrorImmovationsLoad extends ConsumerWidget {
  const _ErrorImmovationsLoad(this._error, {Key? key}) : super(key: key);
  final String _error;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    final _offers = ref.watch(immovableOffersNotifierProvider).offers;

    return Container(
      margin: const EdgeInsets.only(bottom: 10.0),
      width: size.width * 0.5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/404Error.svg',
            width: 100,
            height: 100,
          ),
          AutoSizeText(_error,
              style: styleTituloItalicW600.copyWith(
                  fontSize: 12, fontWeight: FontWeight.w700),
              maxLines: 2,
              stepGranularity: 3,
              textAlign: TextAlign.center),
          SizedBox(width: 20.0),
          ButtonRetryLoad(onPressed: () {
            if (_offers.isEmpty) {
              ref
                  .read(immovableOffersNotifierProvider.notifier)
                  .initImmovableOffers(delay: true);
            }
            ref
                .read(immovablesNotifierProvider.notifier)
                .initImmovables(delay: true);
          })
        ],
      ),
    );
  }
}

class DoneImmovationsLoad extends ConsumerWidget {
  const DoneImmovationsLoad({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    final _isLoadMoreDone =
        ref.watch(immovablesNotifierProvider).isLoadMoreDone;
    final _indexActiveLoading =
        ref.watch(immovablesNotifierProvider).isActiveLoadingLoadMore;
    final _immovables = ref.watch(immovablesProvider.notifier).state;

    return _immovables.isNotEmpty
        ? _indexActiveLoading
            ? Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: CenterIndicator())
            : !_indexActiveLoading && _isLoadMoreDone
                ? Container(
                    width: size.width,
                    height: 100,
                    alignment: Alignment.bottomCenter,
                    margin: const EdgeInsets.only(bottom: 20),
                    child: Text(
                      'Eso es todo!',
                      style: styleTituloItalicW600.copyWith(
                        color: Colors.green,
                        fontSize: 13,
                      ),
                    ),
                  )
                : Container()
        : Container();
  }
}

class _EmptyImmovationsLoad extends StatelessWidget {
  const _EmptyImmovationsLoad({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FadeIn(
      child: Container(
          width: size.width,
          height: size.height * 0.7,
          alignment: Alignment.center,
          child: SvgPicture.asset(
            'assets/empty.svg',
            height: size.height * .2,
          )),
    );
  }
}
