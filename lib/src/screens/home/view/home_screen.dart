import 'package:animate_do/animate_do.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../core/framework/colors.dart';
import '../../../core/framework/tipografia.dart';
import '../../../core/global/environment.dart';
import '../../../service/logic/socket_provider.dart';
import '../../chat/logic/chat_provider.dart';
import '../../chat/logic/chat_users_my_immovables/users_my_imm_provider.dart';
import '../../widgets/center_indicator.dart';
import '../../widgets/linear_indicator.dart';
import '../hooks/global_scroll_controller.dart';
import '../hooks/scroll_controller.dart';
import '../logic/categories/category_tab_provider.dart';
import '../logic/cities/cities_provider.dart';
import '../logic/immovable_offers/immovable_offers_provider.dart';
import '../logic/immovables/immovables_provider.dart';
import 'widgets/buttom_menu_options.dart';
import 'widgets/button_ retry.dart';
import 'widgets/drawer/drawer.dart';
import 'widgets/input_location_city.dart';
import 'widgets/list_detail_iimovables.dart';
import 'widgets/listview_offers_custom.dart';
import 'widgets/listview_tab_custom.dart';
import 'widgets/loading_load_immovables.dart';

class HomeScreen extends ConsumerWidget {
  HomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      backgroundColor: Color(0xFFdadee2),
      drawer: Drawer(
        child: MainDrawer(),
      ),
      body: SafeArea(
        child: _HomeDetail(),
      ),
    );
  }
}

class _HomeDetail extends StatefulHookConsumerWidget {
  _HomeDetail({
    Key? key,
  }) : super(key: key);

 _HomeDetailState createState() => _HomeDetailState();
}

class _HomeDetailState extends ConsumerState<_HomeDetail> {
  String oldCity = '';
  late final SocketService _socketService;

  @override
  void initState() {
    /**
     * Inicializamos las diferentes variables 
     * @_socketService => servicio de socket, permite enviar y escuchar chat
     * @_authService => obtiene la informacion del usuario conectado
    */
    _socketService = ref.read(resolverSocketService.notifier);

   /**
     * Escucho los mensajes de los usuarios chat
    */
    
    _socketService.socket
        ?.on(EnvironmentApi.onMessageReceibed, _onChangeMessage);

    super.initState();
  }

   void _onChangeMessage(dynamic data) {
    ref.read(resolverChatService.notifier).playAudioMessage();
  }

   @override
  void dispose() {
    _socketService.socket?.off(EnvironmentApi.onMessageReceibed);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size              = MediaQuery.of(context).size;
    // OBTENEMOS LA CATEGORIA SELECCIONADA
    final _selectedcategory = ref.watch(selectedCategory.notifier).state;
    // OBTENEMOS LOS PRODUCTOS IMMUEBLES
    final _immovables       = ref.watch(immovablesProvider.notifier).state;
    final _loading          = ref.watch(immovablesNotifierProvider).isLoading;
    final _controller       = useGlobalScrollController(ref, oldLength: _immovables.length);
    // VERIFICAMOS SI ESTA ASTIVO EL SLIVERAPPBAR
    final _isPinned         = ref.watch(isPinnedProvider.state).state;
    // OBTENEMOS EL TEXTEDITINGCONTROLLER
    // final searchCRT = ref.watch(seletedCityCRT);

    ref.listen<TextEditingController>(seletedCityCRT, (prevSeletedCityCRT, newSeletedCityCRT) {
        ref
            .read(immovableOffersNotifierProvider.notifier)
            .onChangeCity(newSeletedCityCRT.text);
        ref
            .read(immovablesNotifierProvider.notifier)
            .onChangeCity(newSeletedCityCRT.text);
      
     });

    // searchCRT.addListener(() {
    //   if (oldCity != searchCRT.text) {
    //     oldCity = searchCRT.text;
    //     ref
    //         .read(immovableOffersNotifierProvider.notifier)
    //         .onChangeCity(searchCRT.text);
    //     ref
    //         .read(immovablesNotifierProvider.notifier)
    //         .onChangeCity(searchCRT.text);
    //   }
    // });

    return RefreshIndicator(
      color: oscuro,
      strokeWidth: 2,
      onRefresh: () {
        return ref.read(immovablesNotifierProvider.notifier).refresh();
      },
      child: Container(
        width: size.width,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          behavior: HitTestBehavior.translucent,
          child: Stack(
            children: [
              NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  overscroll.disallowIndicator();
                  return true;
                },
                child: CustomScrollView(
                  controller: _controller,
                  physics: ClampingScrollPhysics(),
                  slivers: [
                    SliverToBoxAdapter(
                      child: Container(
                        margin: const EdgeInsets.only(left: 40, top: 30),
                        child: const _TitleHome(),
                      ),
                    ),
                    SliverToBoxAdapter(child: const _InputLoaction()),
                    SliverToBoxAdapter(child: _CategoriesActionDetail()),
                    SliverToBoxAdapter(child: _OffersActionDetail()),

                    if (_selectedcategory?.name != null)
                      ListDetailImmovableAppBar(
                        selectedCategory: _selectedcategory,
                        isPinned: _isPinned,
                      ),

                    if (!_loading && _selectedcategory?.name == null)
                      SliverToBoxAdapter(
                        child: FadeIn(
                          child: SizedBox(
                            height: size.height / 8,
                          ),
                        ),
                      ),

                    ListDetailImmovableBody(immovables: _immovables),

                    // CARGAR O CARGAR MAS PERO LLEGO AL ULTIMO ELEMENTO.
                    SliverToBoxAdapter(child: DoneImmovationsLoad())
                  ],
                ),
              ),
              FadeIn(child: OptionMenu(isPinned: _isPinned)),
            ],
          ),
        ),
      ),
    );
  }
}

class _CategoriesActionDetail extends ConsumerWidget {
  _CategoriesActionDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //OBTENEMOS LA CATEGORIA ATRAVES DE LA CATEGORIA SELECCIONADA
    final _selectedIndex =
        ref.watch(selecteCategoryTabNotifierProvider.notifier).state;
    final _state = ref.watch(categoriesNotifierProvider);

    if (_state.isLoading) {
      return LoadingLoadImmovables();
    }

    if (_state.isError) {
      return _ErrorCategory();
    }

    return _state.categories.isNotEmpty
        ? Container(
            margin: const EdgeInsets.only(top: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Text(
                    'Categorias',
                    style: styleTituloNormal.copyWith(
                        fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                ),
                ListViewTabCUstom(
                    selectedIndex: _selectedIndex,
                    categories: _state.categories)
              ],
            ),
          )
        : Container();
  }
}

class _TitleHome extends StatelessWidget {
  const _TitleHome({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        margin: const EdgeInsets.only(left: 20, right: 10),
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: AutoSizeText('Casa de sueño: encuentra la tuya',
            style: styleTituloItalicW600.copyWith(fontSize: 25),
            maxLines: 2,
            stepGranularity: 3));
  }
}

class _InputLoaction extends StatelessWidget {
  const _InputLoaction({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 20),
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: InputLocationCity());
  }
}

class _OffersActionDetail extends HookConsumerWidget {
  _OffersActionDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _offers = ref.watch(offersProvider.notifier).state;
    final _controller =
        useScrollControllerCustom(ref, oldLength: _offers.length);
    final _isLoading = ref.watch(immovableOffersNotifierProvider).isLoading;
    final _isLoadingPagination =
        ref.watch(immovableOffersNotifierProvider).isLoadingPagination;
    final _isLoadMoreErrorOffers =
        ref.watch(immovableOffersNotifierProvider).isLoadMoreError;
    final _isLoadMoreErrorImmovables =
        ref.watch(immovablesNotifierProvider).isLoadMoreError;

    if (_isLoading) {
      return _LoadingLoadOffers();
    }
    if (_isLoadMoreErrorImmovables == '' &&
        !_isLoading &&
        _isLoadMoreErrorOffers != '') {
      return _ErrorOffers();
    }
    return AnimatedContainer(
      duration: Duration(microseconds: 1000),
      curve: Curves.elasticInOut,
      height: _offers.isNotEmpty ? 370 : 0,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(left: 10),
              child: AnimatedContainer(
                duration: Duration(microseconds: 1000),
                height: _offers.isNotEmpty ? 40 : 0,
                child: Text(
                  'Mejores ofertas',
                  style: styleTituloNormal.copyWith(
                      fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            if (_isLoadingPagination) LinearIndicator(),
            ListViewOffersItem(
                controller: _controller,
                onItemSelected: (offer) {},
                offers: _offers,
                isLoading: _isLoading,
                isLoadMoreError: _isLoadMoreErrorOffers)
          ],
        ),
      ),
    );
  }
}

class _LoadingLoadOffers extends StatelessWidget {
  const _LoadingLoadOffers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16.0),
      child: CenterIndicator(),
    );
  }
}

class _ErrorCategory extends ConsumerWidget {
  const _ErrorCategory({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height - 300,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  SvgPicture.asset(
                    'assets/404Error.svg',
                    width: 120,
                    height: 120,
                  ),
                  AutoSizeText('Ocurrio un problema !',
                      style: styleTituloItalicW600.copyWith(fontSize: 15),
                      maxLines: 2,
                      stepGranularity: 3),
                ],
              )),
          SizedBox(height: 20.0),
          ButtonRetryLoad(
              onPressed: () => ref
                  .read(categoriesNotifierProvider.notifier)
                  .initCategories(delay: true))
        ],
      ),
    );
  }
}

class _ErrorOffers extends ConsumerWidget {
  const _ErrorOffers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText('Ocurrio un problema !',
              style: styleTituloItalicW600.copyWith(
                  fontSize: 12, fontWeight: FontWeight.w700),
              maxLines: 2,
              stepGranularity: 3),
          SizedBox(width: 20.0),
          ButtonRetryLoad(
              onPressed: () => ref
                  .read(immovableOffersNotifierProvider.notifier)
                  .initImmovableOffers(delay: true, isLoading: true))
        ],
      ),
    );
  }
}
