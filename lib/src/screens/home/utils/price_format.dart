import 'package:intl/intl.dart';

final f = NumberFormat("#,##0", "es_AR");

String convertPrice(int price) {
  return f.format(price);
}
