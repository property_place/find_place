import 'package:freezed_annotation/freezed_annotation.dart';
import '../../data/models/cities/api_cities_model.dart';

part 'cities_state.freezed.dart';

@freezed
abstract class CitiesState with _$CitiesState {
  const factory CitiesState({
      @Default(1) int page,
      @Default([]) List<DocApiCitiesModel> cities,
      @Default(true) bool isLoading,
      @Default('') String isLoadMoreError,
      @Default(false) bool isLoadMoreDone,
      @Default(false) bool isFetching
  }) = _CitiesState;

  const CitiesState._();
}
