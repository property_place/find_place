part of 'cities_provider.dart';

class CitiesNotifier extends StateNotifier<CitiesState> {
  CitiesNotifier({
    required ICitiesRepository citiesRepository,
  })  : _citiesRepository = citiesRepository,
        super(CitiesState()) {
    _initCities();
  }

  final ICitiesRepository _citiesRepository;
  final int _limitPage = 10;

  _initCities({int? initPage, bool? delay}) async {
    try {
      final page = initPage ?? state.page;
      final cities = await _citiesRepository.getCities(page, _limitPage);
      if (!cities.ok) {
        state = state.copyWith(
          isLoading: false,
          isLoadMoreError: cities.error!.msg,
        );
      }

      state = state.copyWith(
          page: page,
          isLoading: false,
          cities: cities.docs!,
          isFetching: false,
          isLoadMoreError: '');
    } on Exception catch (e) {
      if (delay != null) await Future.delayed(Duration(milliseconds: 2000));
      state = state.copyWith(
        isLoading: false,
        isLoadMoreError: e.toString(),
      );
    }
  }

  void loadMoreStores() async {
    if (state.isLoadMoreDone) return;
    if (state.isLoading) return;

    state = state.copyWith(
      isLoading: true,
      isLoadMoreDone: false,
      isLoadMoreError: '',
    );
    final cities =
        await _citiesRepository.getCities(state.page + 1, _limitPage);

    await Future.delayed(Duration(milliseconds: 2000));
    if (!cities.ok && cities.docs == null) {
      state = state.copyWith(
        isLoadMoreError: cities.error!.msg,
        isLoading: false,
      );
      return;
    }
    if (cities.docs!.isNotEmpty) {
      state = state.copyWith(
        page: state.page + 1,
        isLoading: false,
        isLoadMoreDone: !(cities.page! < cities.totalPages!),
        cities: [...state.cities, ...cities.docs!],
        isFetching: false,
      );
    } else {
      //TODO: PENDIENTE DE REVISION
      state = state.copyWith(
        isLoading: false,
        // isLoadMoreDone: !(cities.page < cities.totalPages),
        isFetching: false,
      );
    }
  }

  void loadSearchCities(String valueSearch) async {
    await Future.delayed(Duration(milliseconds: 600));
    if (valueSearch.isEmpty) {
      _initCities(initPage: 1);
      return;
    }

    final cities = await _citiesRepository.getSearchCities(valueSearch);
    if (!cities.ok && cities.docs == null) {
      state = state.copyWith(
        isLoadMoreError: cities.error!.msg,
        isLoading: false,
      );
      return;
    }
    if (cities.docs!.isNotEmpty) {
      state = state.copyWith(
        isLoading: false,
        cities: cities.docs!,
        isFetching: false,
      );
    } else {
      state = state.copyWith(
        isLoading: false,
        isFetching: false,
      );
    }
  }

  Future<void> refresh() async {
    _initCities(initPage: 1);
  }

  void isPaginate() {
    state = state.copyWith(isFetching: true);
  }
}
