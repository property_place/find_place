// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'cities_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CitiesStateTearOff {
  const _$CitiesStateTearOff();

  _CitiesState call(
      {int page = 1,
      List<DocApiCitiesModel> cities = const [],
      bool isLoading = true,
      String isLoadMoreError = '',
      bool isLoadMoreDone = false,
      bool isFetching = false}) {
    return _CitiesState(
      page: page,
      cities: cities,
      isLoading: isLoading,
      isLoadMoreError: isLoadMoreError,
      isLoadMoreDone: isLoadMoreDone,
      isFetching: isFetching,
    );
  }
}

/// @nodoc
const $CitiesState = _$CitiesStateTearOff();

/// @nodoc
mixin _$CitiesState {
  int get page => throw _privateConstructorUsedError;
  List<DocApiCitiesModel> get cities => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  String get isLoadMoreError => throw _privateConstructorUsedError;
  bool get isLoadMoreDone => throw _privateConstructorUsedError;
  bool get isFetching => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CitiesStateCopyWith<CitiesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CitiesStateCopyWith<$Res> {
  factory $CitiesStateCopyWith(
          CitiesState value, $Res Function(CitiesState) then) =
      _$CitiesStateCopyWithImpl<$Res>;
  $Res call(
      {int page,
      List<DocApiCitiesModel> cities,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class _$CitiesStateCopyWithImpl<$Res> implements $CitiesStateCopyWith<$Res> {
  _$CitiesStateCopyWithImpl(this._value, this._then);

  final CitiesState _value;
  // ignore: unused_field
  final $Res Function(CitiesState) _then;

  @override
  $Res call({
    Object? page = freezed,
    Object? cities = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_value.copyWith(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      cities: cities == freezed
          ? _value.cities
          : cities // ignore: cast_nullable_to_non_nullable
              as List<DocApiCitiesModel>,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$CitiesStateCopyWith<$Res>
    implements $CitiesStateCopyWith<$Res> {
  factory _$CitiesStateCopyWith(
          _CitiesState value, $Res Function(_CitiesState) then) =
      __$CitiesStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int page,
      List<DocApiCitiesModel> cities,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class __$CitiesStateCopyWithImpl<$Res> extends _$CitiesStateCopyWithImpl<$Res>
    implements _$CitiesStateCopyWith<$Res> {
  __$CitiesStateCopyWithImpl(
      _CitiesState _value, $Res Function(_CitiesState) _then)
      : super(_value, (v) => _then(v as _CitiesState));

  @override
  _CitiesState get _value => super._value as _CitiesState;

  @override
  $Res call({
    Object? page = freezed,
    Object? cities = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_CitiesState(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      cities: cities == freezed
          ? _value.cities
          : cities // ignore: cast_nullable_to_non_nullable
              as List<DocApiCitiesModel>,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_CitiesState extends _CitiesState {
  const _$_CitiesState(
      {this.page = 1,
      this.cities = const [],
      this.isLoading = true,
      this.isLoadMoreError = '',
      this.isLoadMoreDone = false,
      this.isFetching = false})
      : super._();

  @JsonKey()
  @override
  final int page;
  @JsonKey()
  @override
  final List<DocApiCitiesModel> cities;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final String isLoadMoreError;
  @JsonKey()
  @override
  final bool isLoadMoreDone;
  @JsonKey()
  @override
  final bool isFetching;

  @override
  String toString() {
    return 'CitiesState(page: $page, cities: $cities, isLoading: $isLoading, isLoadMoreError: $isLoadMoreError, isLoadMoreDone: $isLoadMoreDone, isFetching: $isFetching)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CitiesState &&
            const DeepCollectionEquality().equals(other.page, page) &&
            const DeepCollectionEquality().equals(other.cities, cities) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreError, isLoadMoreError) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreDone, isLoadMoreDone) &&
            const DeepCollectionEquality()
                .equals(other.isFetching, isFetching));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(page),
      const DeepCollectionEquality().hash(cities),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isLoadMoreError),
      const DeepCollectionEquality().hash(isLoadMoreDone),
      const DeepCollectionEquality().hash(isFetching));

  @JsonKey(ignore: true)
  @override
  _$CitiesStateCopyWith<_CitiesState> get copyWith =>
      __$CitiesStateCopyWithImpl<_CitiesState>(this, _$identity);
}

abstract class _CitiesState extends CitiesState {
  const factory _CitiesState(
      {int page,
      List<DocApiCitiesModel> cities,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching}) = _$_CitiesState;
  const _CitiesState._() : super._();

  @override
  int get page;
  @override
  List<DocApiCitiesModel> get cities;
  @override
  bool get isLoading;
  @override
  String get isLoadMoreError;
  @override
  bool get isLoadMoreDone;
  @override
  bool get isFetching;
  @override
  @JsonKey(ignore: true)
  _$CitiesStateCopyWith<_CitiesState> get copyWith =>
      throw _privateConstructorUsedError;
}
