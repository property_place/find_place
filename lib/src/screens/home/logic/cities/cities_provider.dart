import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;
import '../../data/models/cities/api_cities_model.dart';
import '../../data/repositories/cities_repository.dart';
import 'cities_state.dart';

part 'cities_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************
final client = http.Client();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************

final citiesNotifierProvider =
    StateNotifierProvider.autoDispose<CitiesNotifier, CitiesState>(
  (ref) =>
      CitiesNotifier(citiesRepository: ref.watch(_citiesRepositoryProvider)),
);

// *********************************************************************
// => Repository
// *********************************************************************

final _citiesRepositoryProvider = Provider<ICitiesRepository>(
  (ref) => CitiesRepository(client),
);

// *********************************************************************
// => Data
// *********************************************************************

final keyProvider = StateProvider<String>((ref) {
  return '';
});

final citiesProvider = StateProvider.autoDispose<List<DocApiCitiesModel>>((ref) {
  final citiesState = ref.watch(citiesNotifierProvider);
  final key = ref.watch(keyProvider);
  return citiesState.cities
      .where(
        (element) => element.city.toLowerCase().contains(key.toLowerCase()),
      )
      .toList();
});

final seletedCityCRT = StateProvider((ref) => TextEditingController());
