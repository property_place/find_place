part of 'immovable_offers_provider.dart';

class ImmovableOffersNotifier extends StateNotifier<ImmovableOffersState> {
  ImmovableOffersNotifier({
    required IImmovablesRepository iImmovablesRepository,
    required String? categoryId,
    required String city,
  })  : _categoryId = categoryId,
        _iImmovablesRepository = iImmovablesRepository,
        _city = city,
        super(ImmovableOffersState()) {
    _initImmovableOffers();
  }

  final IImmovablesRepository _iImmovablesRepository;
  final String? _categoryId;
  final String _city;
  final int _limitPage = 2;

  _initImmovableOffers({
    int? initPage,
    bool? delay,
    String? city,
    bool? isLoading,
  }) async {
    try {
      if (_categoryId == null) return;
      final page = initPage ?? state.page;

      if (isLoading != null) {
        state = state.copyWith(
          isLoading: isLoading,
          searchCity: city != null ? city : _city,
        );
      }

      final offers = await _iImmovablesRepository.getImmovableOffers(
          page, _limitPage, _categoryId!, city != null ? city : _city);

      if (!offers.ok && offers.docs == null) {
        state = state.copyWith(
          isLoading: false,
          isLoadMoreError: offers.error!.msg,
        );
      }

      state = state.copyWith(
          page: page,
          isLoading: false,
          offers: offers.docs!,
          isFetching: false,
          isLoadMoreError: '');
    } on Exception catch (e) {
      if (delay != null) await Future.delayed(Duration(milliseconds: 2000));
      state = state.copyWith(
        isLoading: false,
        isLoadMoreError: e.toString(),
      );
    }
  }

  void loadMoreStores() async {
    if (_categoryId == null) return;
    if (state.isLoadMoreDone) return;
    if (state.isLoading && state.isLoadingPagination) return;

    state = state.copyWith(
      isLoadingPagination: true,
      isLoadMoreDone: false,
      isLoadMoreError: '',
    );
    final offers = await _iImmovablesRepository.getImmovableOffers(
      state.page + 1,
      _limitPage,
      _categoryId!,
      state.searchCity != null ? state.searchCity! : _city,
    );

    await Future.delayed(Duration(milliseconds: 2000));

    //TODO: REVISAR ELSE IF
    if (!offers.ok &&
        offers.docs == null &&
        offers.totalPages == null &&
        offers.page == null) {
      state = state.copyWith(
        isLoadMoreError: offers.error!.msg,
        isLoadingPagination: false,
      );
      return;
    } else if (offers.docs!.isNotEmpty) {
      state = state.copyWith(
        page: state.page + 1,
        isLoadingPagination: false,
        isLoadMoreDone: !(offers.page! < offers.totalPages!),
        offers: [...state.offers, ...offers.docs!],
        isFetching: false,
      );
    } else {
      state = state.copyWith(
        isLoadingPagination: false,
        isLoadMoreDone: !(offers.page! < offers.totalPages!),
        isFetching: false,
      );
    }
  }

  Future<void> refresh() async {
    _initImmovableOffers(initPage: 1);
  }

  void isPaginate() {
    state = state.copyWith(isFetching: true);
  }

  Future<void> onChangeCity(String city) async {
    state = state.copyWith(
      offers: [],
      isLoadMoreDone: false,
      isFetching: false,
      isLoadMoreError: '',
    );
    _initImmovableOffers(initPage: 1, city: city);
  }

  void initImmovableOffers({bool? delay, bool? isLoading}) async {
    _initImmovableOffers(initPage: 1, delay: delay, isLoading: isLoading);
  }
}
