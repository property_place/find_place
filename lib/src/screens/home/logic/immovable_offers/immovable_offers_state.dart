import 'package:freezed_annotation/freezed_annotation.dart';
import '../../data/models/immovable_offers/immovable_offers_model.dart';

part 'immovable_offers_state.freezed.dart';

@freezed
abstract class ImmovableOffersState with _$ImmovableOffersState {
  const factory ImmovableOffersState(
      {@Default(1) int page,
      @Default([]) List<DocOffersModel> offers,
      String? searchCity,
      @Default(false) bool isLoading,
      @Default(false) bool isLoadingPagination,
      @Default('') String isLoadMoreError,
      @Default(false) bool isLoadMoreDone,
      @Default(false) bool isFetching}) = _ImmovableOffersState;

  const ImmovableOffersState._();
}
