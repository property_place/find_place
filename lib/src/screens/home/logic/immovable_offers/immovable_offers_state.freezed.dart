// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'immovable_offers_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ImmovableOffersStateTearOff {
  const _$ImmovableOffersStateTearOff();

  _ImmovableOffersState call(
      {int page = 1,
      List<DocOffersModel> offers = const [],
      String? searchCity,
      bool isLoading = false,
      bool isLoadingPagination = false,
      String isLoadMoreError = '',
      bool isLoadMoreDone = false,
      bool isFetching = false}) {
    return _ImmovableOffersState(
      page: page,
      offers: offers,
      searchCity: searchCity,
      isLoading: isLoading,
      isLoadingPagination: isLoadingPagination,
      isLoadMoreError: isLoadMoreError,
      isLoadMoreDone: isLoadMoreDone,
      isFetching: isFetching,
    );
  }
}

/// @nodoc
const $ImmovableOffersState = _$ImmovableOffersStateTearOff();

/// @nodoc
mixin _$ImmovableOffersState {
  int get page => throw _privateConstructorUsedError;
  List<DocOffersModel> get offers => throw _privateConstructorUsedError;
  String? get searchCity => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isLoadingPagination => throw _privateConstructorUsedError;
  String get isLoadMoreError => throw _privateConstructorUsedError;
  bool get isLoadMoreDone => throw _privateConstructorUsedError;
  bool get isFetching => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ImmovableOffersStateCopyWith<ImmovableOffersState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImmovableOffersStateCopyWith<$Res> {
  factory $ImmovableOffersStateCopyWith(ImmovableOffersState value,
          $Res Function(ImmovableOffersState) then) =
      _$ImmovableOffersStateCopyWithImpl<$Res>;
  $Res call(
      {int page,
      List<DocOffersModel> offers,
      String? searchCity,
      bool isLoading,
      bool isLoadingPagination,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class _$ImmovableOffersStateCopyWithImpl<$Res>
    implements $ImmovableOffersStateCopyWith<$Res> {
  _$ImmovableOffersStateCopyWithImpl(this._value, this._then);

  final ImmovableOffersState _value;
  // ignore: unused_field
  final $Res Function(ImmovableOffersState) _then;

  @override
  $Res call({
    Object? page = freezed,
    Object? offers = freezed,
    Object? searchCity = freezed,
    Object? isLoading = freezed,
    Object? isLoadingPagination = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_value.copyWith(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      offers: offers == freezed
          ? _value.offers
          : offers // ignore: cast_nullable_to_non_nullable
              as List<DocOffersModel>,
      searchCity: searchCity == freezed
          ? _value.searchCity
          : searchCity // ignore: cast_nullable_to_non_nullable
              as String?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingPagination: isLoadingPagination == freezed
          ? _value.isLoadingPagination
          : isLoadingPagination // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$ImmovableOffersStateCopyWith<$Res>
    implements $ImmovableOffersStateCopyWith<$Res> {
  factory _$ImmovableOffersStateCopyWith(_ImmovableOffersState value,
          $Res Function(_ImmovableOffersState) then) =
      __$ImmovableOffersStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int page,
      List<DocOffersModel> offers,
      String? searchCity,
      bool isLoading,
      bool isLoadingPagination,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class __$ImmovableOffersStateCopyWithImpl<$Res>
    extends _$ImmovableOffersStateCopyWithImpl<$Res>
    implements _$ImmovableOffersStateCopyWith<$Res> {
  __$ImmovableOffersStateCopyWithImpl(
      _ImmovableOffersState _value, $Res Function(_ImmovableOffersState) _then)
      : super(_value, (v) => _then(v as _ImmovableOffersState));

  @override
  _ImmovableOffersState get _value => super._value as _ImmovableOffersState;

  @override
  $Res call({
    Object? page = freezed,
    Object? offers = freezed,
    Object? searchCity = freezed,
    Object? isLoading = freezed,
    Object? isLoadingPagination = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_ImmovableOffersState(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      offers: offers == freezed
          ? _value.offers
          : offers // ignore: cast_nullable_to_non_nullable
              as List<DocOffersModel>,
      searchCity: searchCity == freezed
          ? _value.searchCity
          : searchCity // ignore: cast_nullable_to_non_nullable
              as String?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadingPagination: isLoadingPagination == freezed
          ? _value.isLoadingPagination
          : isLoadingPagination // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ImmovableOffersState extends _ImmovableOffersState {
  const _$_ImmovableOffersState(
      {this.page = 1,
      this.offers = const [],
      this.searchCity,
      this.isLoading = false,
      this.isLoadingPagination = false,
      this.isLoadMoreError = '',
      this.isLoadMoreDone = false,
      this.isFetching = false})
      : super._();

  @JsonKey()
  @override
  final int page;
  @JsonKey()
  @override
  final List<DocOffersModel> offers;
  @override
  final String? searchCity;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final bool isLoadingPagination;
  @JsonKey()
  @override
  final String isLoadMoreError;
  @JsonKey()
  @override
  final bool isLoadMoreDone;
  @JsonKey()
  @override
  final bool isFetching;

  @override
  String toString() {
    return 'ImmovableOffersState(page: $page, offers: $offers, searchCity: $searchCity, isLoading: $isLoading, isLoadingPagination: $isLoadingPagination, isLoadMoreError: $isLoadMoreError, isLoadMoreDone: $isLoadMoreDone, isFetching: $isFetching)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ImmovableOffersState &&
            const DeepCollectionEquality().equals(other.page, page) &&
            const DeepCollectionEquality().equals(other.offers, offers) &&
            const DeepCollectionEquality()
                .equals(other.searchCity, searchCity) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.isLoadingPagination, isLoadingPagination) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreError, isLoadMoreError) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreDone, isLoadMoreDone) &&
            const DeepCollectionEquality()
                .equals(other.isFetching, isFetching));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(page),
      const DeepCollectionEquality().hash(offers),
      const DeepCollectionEquality().hash(searchCity),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isLoadingPagination),
      const DeepCollectionEquality().hash(isLoadMoreError),
      const DeepCollectionEquality().hash(isLoadMoreDone),
      const DeepCollectionEquality().hash(isFetching));

  @JsonKey(ignore: true)
  @override
  _$ImmovableOffersStateCopyWith<_ImmovableOffersState> get copyWith =>
      __$ImmovableOffersStateCopyWithImpl<_ImmovableOffersState>(
          this, _$identity);
}

abstract class _ImmovableOffersState extends ImmovableOffersState {
  const factory _ImmovableOffersState(
      {int page,
      List<DocOffersModel> offers,
      String? searchCity,
      bool isLoading,
      bool isLoadingPagination,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching}) = _$_ImmovableOffersState;
  const _ImmovableOffersState._() : super._();

  @override
  int get page;
  @override
  List<DocOffersModel> get offers;
  @override
  String? get searchCity;
  @override
  bool get isLoading;
  @override
  bool get isLoadingPagination;
  @override
  String get isLoadMoreError;
  @override
  bool get isLoadMoreDone;
  @override
  bool get isFetching;
  @override
  @JsonKey(ignore: true)
  _$ImmovableOffersStateCopyWith<_ImmovableOffersState> get copyWith =>
      throw _privateConstructorUsedError;
}
