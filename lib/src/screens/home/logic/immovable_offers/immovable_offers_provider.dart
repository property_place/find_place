import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;
import '../../../auth/logic/auth_provider.dart';
import '../../data/models/immovable_offers/immovable_offers_model.dart';
import '../../data/repositories/immovables_repository.dart';
import '../categories/category_tab_provider.dart';
import '../cities/cities_provider.dart';
import 'immovable_offers_state.dart';

part 'immovable_offers_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************
final client = http.Client();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************

final immovableOffersNotifierProvider =
    StateNotifierProvider<ImmovableOffersNotifier, ImmovableOffersState>((ref) {
  final stateCategory = ref.watch(selectedCategory.notifier).state;
  final _cityCRT = ref.watch(seletedCityCRT);
  final _infoOfTheConnectedPerson = ref.watch(userProvider);
  final _city = _cityCRT.text;

  if (_infoOfTheConnectedPerson != null && _city.isEmpty) {
    _cityCRT.text = _infoOfTheConnectedPerson.city;
  }

  return ImmovableOffersNotifier(
      iImmovablesRepository: ref.watch(_immovablesRepositoryProvider),
      categoryId: stateCategory?.categoryId,
      city: _cityCRT.text);
});

// *********************************************************************
// => Repository
// *********************************************************************

final _immovablesRepositoryProvider = Provider<IImmovablesRepository>(
  (ref) => ImmovablesRepository(client),
);

// *********************************************************************
// => Data
// *********************************************************************

final offersProvider = StateProvider<List<DocOffersModel>>((ref) {
  final storeState = ref.watch(immovableOffersNotifierProvider);
  return storeState.offers;
});
