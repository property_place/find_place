import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_tab_state.freezed.dart';

@freezed
abstract class CategoryTabState with _$CategoryTabState {
  const factory CategoryTabState.selectedTabChanged(int selectedIndex) = SelectedTabChanged;
}
