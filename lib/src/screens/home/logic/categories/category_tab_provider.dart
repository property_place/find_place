import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;

import '../../data/models/categories/categories_model.dart';
import '../../data/repositories/categories_repository.dart';
import 'categories_state.dart';
import 'category_tab_state.dart';

part 'categories_state_notifier.dart';
part 'category_tab_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************
final client = http.Client();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************
final categoryTabNotifierProvider =
    StateNotifierProvider<CategoryTabNotifier, CategoryTabState>(
  (ref) => CategoryTabNotifier(),
);

final selecteCategoryTabNotifierProvider = StateProvider<int>((ref) {
  final state = ref.watch(categoryTabNotifierProvider);
  return state.selectedIndex;
});

final categoriesNotifierProvider =
    StateNotifierProvider<CategoriesNotifier, CategoriesState>(
  (ref) => CategoriesNotifier(
    categoriesRepository: ref.watch(_categoriesRepositoryProvider),
  ),
);

// *********************************************************************
// => Repository
// *********************************************************************

final _categoriesRepositoryProvider = Provider<ICategoriesRepository>(
  (ref) => CategoriesRepository(client),
);


// *********************************************************************
// => Data
// *********************************************************************

final selectedCategory = StateProvider<DocCategoryModel?>((ref) {
  final state = ref.watch(categoryTabNotifierProvider);
  final stateCategories = ref.watch(categoriesNotifierProvider);
  
  if (!stateCategories.isError && !stateCategories.isLoading) {
    return stateCategories.categories[state.selectedIndex];
  } else if (stateCategories.categories.isNotEmpty) {
    return stateCategories.categories[0];
  } else {
    return null;
  }
});
