import 'package:freezed_annotation/freezed_annotation.dart';
import '../../data/models/categories/categories_model.dart';

part 'categories_state.freezed.dart';

@freezed
abstract class CategoriesState with _$CategoriesState {
  const factory CategoriesState({
    @Default([]) List<DocCategoryModel> categories,
    @Default(true) bool isLoading,
    @Default(false) bool isError,
  }) = _CategoriesState;

  const CategoriesState._();
}
