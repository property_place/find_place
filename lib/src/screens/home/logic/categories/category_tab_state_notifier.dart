part of 'category_tab_provider.dart';

class CategoryTabNotifier extends StateNotifier<CategoryTabState> {
  CategoryTabNotifier()
      : super(CategoryTabState.selectedTabChanged(0));

  void itemSelected(int currentScreenIndex) {
    state = state.copyWith(selectedIndex: currentScreenIndex);
  }
}
