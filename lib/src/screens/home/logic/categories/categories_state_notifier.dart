part of 'category_tab_provider.dart';

class CategoriesNotifier extends StateNotifier<CategoriesState> {
  CategoriesNotifier({
    required ICategoriesRepository categoriesRepository,
  })  : _categoriesRepository = categoriesRepository,
        super(CategoriesState()) {
    initCategories();
  }

  final ICategoriesRepository _categoriesRepository;

  void initCategories({bool? delay}) async {
    try {
      state = state.copyWith(isLoading: true);
      final categories = await _categoriesRepository.getCategories();

      state = state.copyWith(
        isLoading: false,
        categories: categories.docs,
        isError: false
      );

    } on Exception {
      if (delay != null) await Future.delayed(Duration(milliseconds: 2000));
      state = state.copyWith(
        isLoading: false,
        isError: true,
      );
    }
  }
}
