// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'category_tab_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CategoryTabStateTearOff {
  const _$CategoryTabStateTearOff();

  SelectedTabChanged selectedTabChanged(int selectedIndex) {
    return SelectedTabChanged(
      selectedIndex,
    );
  }
}

/// @nodoc
const $CategoryTabState = _$CategoryTabStateTearOff();

/// @nodoc
mixin _$CategoryTabState {
  int get selectedIndex => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int selectedIndex) selectedTabChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int selectedIndex)? selectedTabChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int selectedIndex)? selectedTabChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SelectedTabChanged value) selectedTabChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SelectedTabChanged value)? selectedTabChanged,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SelectedTabChanged value)? selectedTabChanged,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CategoryTabStateCopyWith<CategoryTabState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoryTabStateCopyWith<$Res> {
  factory $CategoryTabStateCopyWith(
          CategoryTabState value, $Res Function(CategoryTabState) then) =
      _$CategoryTabStateCopyWithImpl<$Res>;
  $Res call({int selectedIndex});
}

/// @nodoc
class _$CategoryTabStateCopyWithImpl<$Res>
    implements $CategoryTabStateCopyWith<$Res> {
  _$CategoryTabStateCopyWithImpl(this._value, this._then);

  final CategoryTabState _value;
  // ignore: unused_field
  final $Res Function(CategoryTabState) _then;

  @override
  $Res call({
    Object? selectedIndex = freezed,
  }) {
    return _then(_value.copyWith(
      selectedIndex: selectedIndex == freezed
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class $SelectedTabChangedCopyWith<$Res>
    implements $CategoryTabStateCopyWith<$Res> {
  factory $SelectedTabChangedCopyWith(
          SelectedTabChanged value, $Res Function(SelectedTabChanged) then) =
      _$SelectedTabChangedCopyWithImpl<$Res>;
  @override
  $Res call({int selectedIndex});
}

/// @nodoc
class _$SelectedTabChangedCopyWithImpl<$Res>
    extends _$CategoryTabStateCopyWithImpl<$Res>
    implements $SelectedTabChangedCopyWith<$Res> {
  _$SelectedTabChangedCopyWithImpl(
      SelectedTabChanged _value, $Res Function(SelectedTabChanged) _then)
      : super(_value, (v) => _then(v as SelectedTabChanged));

  @override
  SelectedTabChanged get _value => super._value as SelectedTabChanged;

  @override
  $Res call({
    Object? selectedIndex = freezed,
  }) {
    return _then(SelectedTabChanged(
      selectedIndex == freezed
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$SelectedTabChanged implements SelectedTabChanged {
  const _$SelectedTabChanged(this.selectedIndex);

  @override
  final int selectedIndex;

  @override
  String toString() {
    return 'CategoryTabState.selectedTabChanged(selectedIndex: $selectedIndex)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is SelectedTabChanged &&
            const DeepCollectionEquality()
                .equals(other.selectedIndex, selectedIndex));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(selectedIndex));

  @JsonKey(ignore: true)
  @override
  $SelectedTabChangedCopyWith<SelectedTabChanged> get copyWith =>
      _$SelectedTabChangedCopyWithImpl<SelectedTabChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int selectedIndex) selectedTabChanged,
  }) {
    return selectedTabChanged(selectedIndex);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int selectedIndex)? selectedTabChanged,
  }) {
    return selectedTabChanged?.call(selectedIndex);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int selectedIndex)? selectedTabChanged,
    required TResult orElse(),
  }) {
    if (selectedTabChanged != null) {
      return selectedTabChanged(selectedIndex);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SelectedTabChanged value) selectedTabChanged,
  }) {
    return selectedTabChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SelectedTabChanged value)? selectedTabChanged,
  }) {
    return selectedTabChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SelectedTabChanged value)? selectedTabChanged,
    required TResult orElse(),
  }) {
    if (selectedTabChanged != null) {
      return selectedTabChanged(this);
    }
    return orElse();
  }
}

abstract class SelectedTabChanged implements CategoryTabState {
  const factory SelectedTabChanged(int selectedIndex) = _$SelectedTabChanged;

  @override
  int get selectedIndex;
  @override
  @JsonKey(ignore: true)
  $SelectedTabChangedCopyWith<SelectedTabChanged> get copyWith =>
      throw _privateConstructorUsedError;
}
