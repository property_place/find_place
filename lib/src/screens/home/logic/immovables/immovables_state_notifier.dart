part of 'immovables_provider.dart';

class ImmovablesNotifier extends StateNotifier<ImmovablesState> {
  ImmovablesNotifier({
    required IImmovablesRepository iImmovablesRepository,
    required String? categoryId,
    required String city,
  })  : _categoryId = categoryId,
        _iImmovablesRepository = iImmovablesRepository,
        _city = city,
        super(ImmovablesState()) {
    _initImmovables();
  }

  final IImmovablesRepository _iImmovablesRepository;
  final String? _categoryId;
  final String _city;
  final int _limitPage = 50;

  _initImmovables(
      {int? initPage, bool? delay, String? city, FiltersState? filters}) async {
    try {
      if (_categoryId == null) return;
      final page = initPage ?? state.page;

      state = state.copyWith(
          isLoading: true,
          searchCity: city != null ? city : _city,
          filters: []);

      final filterOrder = filters != null
          ? filters.order.where((obj) => obj.active == true)
          : null;
      if (filterOrder != null && filterOrder.isNotEmpty) {
        filter(filterOrder.first, TypeFilter.order.name);
      }

      if (filters?.betweenDates != null) {
        filterInput(filters?.betweenDates, TypeFilter.date.name,
            FilterSort.published_date.name);
      }

      if (filters?.betweenPrice != null) {
        filterInput(filters?.betweenPrice, TypeFilter.price.name,
            FilterSort.price.name);
      }

      final immovables = await _iImmovablesRepository.getImmovables(page,
          _limitPage, _categoryId!, city != null ? city : _city, state.filters);

      if (!immovables.ok &&
          immovables.docs == null &&
          immovables.page == null &&
          immovables.totalPages == null) {
        state = state.copyWith(
          isLoading: false,
          isLoadMoreError: immovables.error!.msg,
        );
        return;
      }

      state = state.copyWith(
          page: page,
          isLoading: false,
          immovables: immovables.docs!,
          isFetching: false,
          isLoadMoreError: '');
    } on Exception catch (e) {
      if (delay != null) await Future.delayed(Duration(milliseconds: 2000));
      state = state.copyWith(
        isLoading: false,
        isLoadMoreError: e.toString(),
      );
    }
  }

  void filter(FilterImmovables filter, String key) {
    final result = filter;
    final data = {"sort": result.sort, "type": result.type};
    state = state.copyWith(filters: [
      {"key": key, "data": data},
      ...state.filters
    ]);
  }

  void filterInput(FilterBetweenImmovables? filter, String key, String sort) {
    if (filter != null && (filter.maximum != '' && filter.minimum != '')) {
      final maximum = filter.maximum;
      final minimum = filter.minimum;
      final data = {'sort': sort, 'maximum': maximum, 'minimum': minimum};
      state = state.copyWith(filters: [
        {'key': key, 'data': data},
        ...state.filters
      ]);
    }
  }

  void loadMoreStores() async {
    if (_categoryId == null) return;
    if (state.isLoadMoreDone) return;
    if (state.isLoading) return;

    state = state.copyWith(
      isLoading: true,
      isLoadMoreDone: false,
      isLoadMoreError: '',
    );

    final immovables = await _iImmovablesRepository.getImmovables(
        state.page + 1,
        _limitPage,
        _categoryId!,
        state.searchCity != null ? state.searchCity! : _city,
        state.filters);

    if (!immovables.ok &&
        immovables.docs == null &&
        immovables.page == null &&
        immovables.totalPages == null) {
      state = state.copyWith(
        isLoading: false,
        isLoadMoreError: immovables.error!.msg,
      );
      return;
    }
    if (immovables.docs!.isNotEmpty) {
      state = state.copyWith(
          page: state.page + 1,
          isLoading: false,
          isLoadMoreDone: !(immovables.page! < immovables.totalPages!),
          immovables: [...state.immovables, ...immovables.docs!],
          isFetching: false);
    } else {
      state = state.copyWith(
        isLoading: false,
        isLoadMoreDone: !(immovables.page! < immovables.totalPages!),
        isFetching: false,
      );
    }
  }

  Future<void> refresh() async {
    _initImmovables(initPage: 1);
  }

  void isPaginate() {
    state = state.copyWith(isFetching: true);
  }

  void activeLoadingLoadMore() async {
    state = state.copyWith(
      isActiveLoadingLoadMore: true,
    );

    await Future.delayed(Duration(milliseconds: 2000));

    state = state.copyWith(
      isActiveLoadingLoadMore: false,
    );
  }

  Future<void> onChangeCity(String city) async {
    state = state.copyWith(
      immovables: [],
      isLoadMoreDone: false,
      isFetching: false,
      isLoadMoreError: '',
      isActiveLoadingLoadMore: false,
    );
    _initImmovables(initPage: 1, city: city);
  }

  void initImmovables({bool? delay, FiltersState? filters}) async {
    state = state.copyWith(
      immovables: [],
      isLoadMoreDone: false,
      isFetching: false,
      isLoadMoreError: '',
      isActiveLoadingLoadMore: false,
    );
    _initImmovables(initPage: 1, delay: delay, filters: filters);
  }

  void filterReset() {
    state = state.copyWith(filters: []);
  }
}
