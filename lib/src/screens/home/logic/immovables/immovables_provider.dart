import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;
import '../../../auth/logic/auth_provider.dart';
import '../../data/models/filters/filters_model.dart';
import '../../data/models/immovables/immovables_model.dart';
import '../../data/repositories/immovables_repository.dart';
import '../../utils/type_filter.dart';
import '../categories/category_tab_provider.dart';
import '../cities/cities_provider.dart';
import '../filters/filters_state.dart';
import 'immovables_state.dart';

part 'immovables_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************

final client = http.Client();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************

final immovablesNotifierProvider =
    StateNotifierProvider<ImmovablesNotifier, ImmovablesState>((ref) {
  /*
   ==> Obtenemos la ciudad por defeto del usuario, en el primer arranque de la aplicacion
  */
  final __infoOfTheConnectedPerson = ref.watch(userProvider);
  /*
   ==> Obtenemos la categoria si, el usuario a seleccionado otra diferente
  */
  final stateCategory = ref.watch(selectedCategory.notifier).state;
  /*
   ==> Obtenemos la ciudad si, el usuario a seleccionado otra diferente
  */
  final _cityCRT = ref.watch(seletedCityCRT);
  final _city = _cityCRT.text;

  if (__infoOfTheConnectedPerson != null && _city.isEmpty) {
    _cityCRT.text = __infoOfTheConnectedPerson.city;
  }
  /*
   ==> Obtenemos los diferentes si, el usuario a seleccionado otra diferente
  */

  return ImmovablesNotifier(
      iImmovablesRepository: ref.watch(_immovablesRepositoryProvider),
      categoryId: stateCategory?.categoryId,
      city: _cityCRT.text);
});

// *********************************************************************
// => Repository
// *********************************************************************

final _immovablesRepositoryProvider = Provider<IImmovablesRepository>(
  (ref) => ImmovablesRepository(client),
);

// *********************************************************************
// => Data
// *********************************************************************

final immovablesProvider =
    StateProvider<List<DocImmovableExhibitionModel>>((ref) {
  final storeState = ref.watch(immovablesNotifierProvider);
  return storeState.immovables;
});

final isPinnedProvider = StateProvider((ref) => false);

final indexHero = StateProvider((ref) => 0);
