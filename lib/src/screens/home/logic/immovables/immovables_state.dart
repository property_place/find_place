import 'package:freezed_annotation/freezed_annotation.dart';
import '../../data/models/filters/filters_model.dart';
import '../../data/models/immovables/immovables_model.dart';

part 'immovables_state.freezed.dart';

@freezed
abstract class ImmovablesState with _$ImmovablesState {
  const factory ImmovablesState(
      {@Default(1) int page,
      @Default([]) List<DocImmovableExhibitionModel> immovables,
      @Default(true) bool isLoading,
      @Default('') String isLoadMoreError,
      @Default(false) bool isLoadMoreDone,
      @Default(false) bool isFetching,
      String? searchCity,
      @Default([]) List<Map<String, dynamic>>  filters,
      @Default(false) bool isActiveLoadingLoadMore,

      }) = _ImmovablesState;

  const ImmovablesState._();
}
