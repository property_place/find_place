// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'immovables_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ImmovablesStateTearOff {
  const _$ImmovablesStateTearOff();

  _ImmovablesState call(
      {int page = 1,
      List<DocImmovableExhibitionModel> immovables = const [],
      bool isLoading = true,
      String isLoadMoreError = '',
      bool isLoadMoreDone = false,
      bool isFetching = false,
      String? searchCity,
      List<Map<String, dynamic>> filters = const [],
      bool isActiveLoadingLoadMore = false}) {
    return _ImmovablesState(
      page: page,
      immovables: immovables,
      isLoading: isLoading,
      isLoadMoreError: isLoadMoreError,
      isLoadMoreDone: isLoadMoreDone,
      isFetching: isFetching,
      searchCity: searchCity,
      filters: filters,
      isActiveLoadingLoadMore: isActiveLoadingLoadMore,
    );
  }
}

/// @nodoc
const $ImmovablesState = _$ImmovablesStateTearOff();

/// @nodoc
mixin _$ImmovablesState {
  int get page => throw _privateConstructorUsedError;
  List<DocImmovableExhibitionModel> get immovables =>
      throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  String get isLoadMoreError => throw _privateConstructorUsedError;
  bool get isLoadMoreDone => throw _privateConstructorUsedError;
  bool get isFetching => throw _privateConstructorUsedError;
  String? get searchCity => throw _privateConstructorUsedError;
  List<Map<String, dynamic>> get filters => throw _privateConstructorUsedError;
  bool get isActiveLoadingLoadMore => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ImmovablesStateCopyWith<ImmovablesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImmovablesStateCopyWith<$Res> {
  factory $ImmovablesStateCopyWith(
          ImmovablesState value, $Res Function(ImmovablesState) then) =
      _$ImmovablesStateCopyWithImpl<$Res>;
  $Res call(
      {int page,
      List<DocImmovableExhibitionModel> immovables,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching,
      String? searchCity,
      List<Map<String, dynamic>> filters,
      bool isActiveLoadingLoadMore});
}

/// @nodoc
class _$ImmovablesStateCopyWithImpl<$Res>
    implements $ImmovablesStateCopyWith<$Res> {
  _$ImmovablesStateCopyWithImpl(this._value, this._then);

  final ImmovablesState _value;
  // ignore: unused_field
  final $Res Function(ImmovablesState) _then;

  @override
  $Res call({
    Object? page = freezed,
    Object? immovables = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
    Object? searchCity = freezed,
    Object? filters = freezed,
    Object? isActiveLoadingLoadMore = freezed,
  }) {
    return _then(_value.copyWith(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      immovables: immovables == freezed
          ? _value.immovables
          : immovables // ignore: cast_nullable_to_non_nullable
              as List<DocImmovableExhibitionModel>,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
      searchCity: searchCity == freezed
          ? _value.searchCity
          : searchCity // ignore: cast_nullable_to_non_nullable
              as String?,
      filters: filters == freezed
          ? _value.filters
          : filters // ignore: cast_nullable_to_non_nullable
              as List<Map<String, dynamic>>,
      isActiveLoadingLoadMore: isActiveLoadingLoadMore == freezed
          ? _value.isActiveLoadingLoadMore
          : isActiveLoadingLoadMore // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$ImmovablesStateCopyWith<$Res>
    implements $ImmovablesStateCopyWith<$Res> {
  factory _$ImmovablesStateCopyWith(
          _ImmovablesState value, $Res Function(_ImmovablesState) then) =
      __$ImmovablesStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int page,
      List<DocImmovableExhibitionModel> immovables,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching,
      String? searchCity,
      List<Map<String, dynamic>> filters,
      bool isActiveLoadingLoadMore});
}

/// @nodoc
class __$ImmovablesStateCopyWithImpl<$Res>
    extends _$ImmovablesStateCopyWithImpl<$Res>
    implements _$ImmovablesStateCopyWith<$Res> {
  __$ImmovablesStateCopyWithImpl(
      _ImmovablesState _value, $Res Function(_ImmovablesState) _then)
      : super(_value, (v) => _then(v as _ImmovablesState));

  @override
  _ImmovablesState get _value => super._value as _ImmovablesState;

  @override
  $Res call({
    Object? page = freezed,
    Object? immovables = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
    Object? searchCity = freezed,
    Object? filters = freezed,
    Object? isActiveLoadingLoadMore = freezed,
  }) {
    return _then(_ImmovablesState(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      immovables: immovables == freezed
          ? _value.immovables
          : immovables // ignore: cast_nullable_to_non_nullable
              as List<DocImmovableExhibitionModel>,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
      searchCity: searchCity == freezed
          ? _value.searchCity
          : searchCity // ignore: cast_nullable_to_non_nullable
              as String?,
      filters: filters == freezed
          ? _value.filters
          : filters // ignore: cast_nullable_to_non_nullable
              as List<Map<String, dynamic>>,
      isActiveLoadingLoadMore: isActiveLoadingLoadMore == freezed
          ? _value.isActiveLoadingLoadMore
          : isActiveLoadingLoadMore // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ImmovablesState extends _ImmovablesState {
  const _$_ImmovablesState(
      {this.page = 1,
      this.immovables = const [],
      this.isLoading = true,
      this.isLoadMoreError = '',
      this.isLoadMoreDone = false,
      this.isFetching = false,
      this.searchCity,
      this.filters = const [],
      this.isActiveLoadingLoadMore = false})
      : super._();

  @JsonKey()
  @override
  final int page;
  @JsonKey()
  @override
  final List<DocImmovableExhibitionModel> immovables;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final String isLoadMoreError;
  @JsonKey()
  @override
  final bool isLoadMoreDone;
  @JsonKey()
  @override
  final bool isFetching;
  @override
  final String? searchCity;
  @JsonKey()
  @override
  final List<Map<String, dynamic>> filters;
  @JsonKey()
  @override
  final bool isActiveLoadingLoadMore;

  @override
  String toString() {
    return 'ImmovablesState(page: $page, immovables: $immovables, isLoading: $isLoading, isLoadMoreError: $isLoadMoreError, isLoadMoreDone: $isLoadMoreDone, isFetching: $isFetching, searchCity: $searchCity, filters: $filters, isActiveLoadingLoadMore: $isActiveLoadingLoadMore)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ImmovablesState &&
            const DeepCollectionEquality().equals(other.page, page) &&
            const DeepCollectionEquality()
                .equals(other.immovables, immovables) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreError, isLoadMoreError) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreDone, isLoadMoreDone) &&
            const DeepCollectionEquality()
                .equals(other.isFetching, isFetching) &&
            const DeepCollectionEquality()
                .equals(other.searchCity, searchCity) &&
            const DeepCollectionEquality().equals(other.filters, filters) &&
            const DeepCollectionEquality().equals(
                other.isActiveLoadingLoadMore, isActiveLoadingLoadMore));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(page),
      const DeepCollectionEquality().hash(immovables),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isLoadMoreError),
      const DeepCollectionEquality().hash(isLoadMoreDone),
      const DeepCollectionEquality().hash(isFetching),
      const DeepCollectionEquality().hash(searchCity),
      const DeepCollectionEquality().hash(filters),
      const DeepCollectionEquality().hash(isActiveLoadingLoadMore));

  @JsonKey(ignore: true)
  @override
  _$ImmovablesStateCopyWith<_ImmovablesState> get copyWith =>
      __$ImmovablesStateCopyWithImpl<_ImmovablesState>(this, _$identity);
}

abstract class _ImmovablesState extends ImmovablesState {
  const factory _ImmovablesState(
      {int page,
      List<DocImmovableExhibitionModel> immovables,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching,
      String? searchCity,
      List<Map<String, dynamic>> filters,
      bool isActiveLoadingLoadMore}) = _$_ImmovablesState;
  const _ImmovablesState._() : super._();

  @override
  int get page;
  @override
  List<DocImmovableExhibitionModel> get immovables;
  @override
  bool get isLoading;
  @override
  String get isLoadMoreError;
  @override
  bool get isLoadMoreDone;
  @override
  bool get isFetching;
  @override
  String? get searchCity;
  @override
  List<Map<String, dynamic>> get filters;
  @override
  bool get isActiveLoadingLoadMore;
  @override
  @JsonKey(ignore: true)
  _$ImmovablesStateCopyWith<_ImmovablesState> get copyWith =>
      throw _privateConstructorUsedError;
}
