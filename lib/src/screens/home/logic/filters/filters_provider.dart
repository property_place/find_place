import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../data/models/filters/filters_model.dart';
import '../../utils/type_filter.dart';
import 'filters_state.dart';

part 'filters_state_notifier.dart';

final filtersNotifierProvider =
    StateNotifierProvider<FiltersNotifier, FiltersState>(
        (ref) => FiltersNotifier());

final orderNotifierProvider = StateProvider<List<FilterImmovables>>((ref) {
  final filterState = ref.watch(filtersNotifierProvider);
  return filterState.order;
});

final maximumPriceCRTProvider = Provider((ref) => TextEditingController());
final minimumPriceCRTProvider = Provider((ref) => TextEditingController());

final dateStartDateCRTProvider = Provider((ref) => TextEditingController());
final dateEndDateCRTProvider = Provider((ref) => TextEditingController());

final dataRangeProvider = StateProvider((ref) {
  final now = DateTime.now();
  return DateTimeRange(
      start: DateTime(now.year, now.month, now.day),
      end: DateTime(now.year, now.month, now.day));
});

final startProvider =
    StateProvider.family<TextEditingController, String>((ref, start) {
  final date = ref.watch(dataRangeProvider);
  if (date.end.isAfter(date.start)) ref.read(dateStartDateCRTProvider).text = start;
  return ref.watch(dateStartDateCRTProvider);
});

final endProvider =
    StateProvider.family<TextEditingController, String>((ref, end) {
  final date = ref.watch(dataRangeProvider);
  if (date.end.isAfter(date.start)) ref.read(dateEndDateCRTProvider).text = end;
  
  return ref.watch(dateEndDateCRTProvider);
});
