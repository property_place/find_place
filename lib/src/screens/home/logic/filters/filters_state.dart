import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/models/filters/filters_model.dart';

part 'filters_state.freezed.dart';

@freezed
abstract class FiltersState with _$FiltersState {
  const factory FiltersState(
      {@Default([]) List<FilterImmovables> order,
      FilterBetweenImmovables? betweenPrice,
      FilterBetweenImmovables? betweenDates}) = _FiltersState;

  const FiltersState._();
}
