// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'filters_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FiltersStateTearOff {
  const _$FiltersStateTearOff();

  _FiltersState call(
      {List<FilterImmovables> order = const [],
      FilterBetweenImmovables? betweenPrice,
      FilterBetweenImmovables? betweenDates}) {
    return _FiltersState(
      order: order,
      betweenPrice: betweenPrice,
      betweenDates: betweenDates,
    );
  }
}

/// @nodoc
const $FiltersState = _$FiltersStateTearOff();

/// @nodoc
mixin _$FiltersState {
  List<FilterImmovables> get order => throw _privateConstructorUsedError;
  FilterBetweenImmovables? get betweenPrice =>
      throw _privateConstructorUsedError;
  FilterBetweenImmovables? get betweenDates =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FiltersStateCopyWith<FiltersState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FiltersStateCopyWith<$Res> {
  factory $FiltersStateCopyWith(
          FiltersState value, $Res Function(FiltersState) then) =
      _$FiltersStateCopyWithImpl<$Res>;
  $Res call(
      {List<FilterImmovables> order,
      FilterBetweenImmovables? betweenPrice,
      FilterBetweenImmovables? betweenDates});
}

/// @nodoc
class _$FiltersStateCopyWithImpl<$Res> implements $FiltersStateCopyWith<$Res> {
  _$FiltersStateCopyWithImpl(this._value, this._then);

  final FiltersState _value;
  // ignore: unused_field
  final $Res Function(FiltersState) _then;

  @override
  $Res call({
    Object? order = freezed,
    Object? betweenPrice = freezed,
    Object? betweenDates = freezed,
  }) {
    return _then(_value.copyWith(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as List<FilterImmovables>,
      betweenPrice: betweenPrice == freezed
          ? _value.betweenPrice
          : betweenPrice // ignore: cast_nullable_to_non_nullable
              as FilterBetweenImmovables?,
      betweenDates: betweenDates == freezed
          ? _value.betweenDates
          : betweenDates // ignore: cast_nullable_to_non_nullable
              as FilterBetweenImmovables?,
    ));
  }
}

/// @nodoc
abstract class _$FiltersStateCopyWith<$Res>
    implements $FiltersStateCopyWith<$Res> {
  factory _$FiltersStateCopyWith(
          _FiltersState value, $Res Function(_FiltersState) then) =
      __$FiltersStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {List<FilterImmovables> order,
      FilterBetweenImmovables? betweenPrice,
      FilterBetweenImmovables? betweenDates});
}

/// @nodoc
class __$FiltersStateCopyWithImpl<$Res> extends _$FiltersStateCopyWithImpl<$Res>
    implements _$FiltersStateCopyWith<$Res> {
  __$FiltersStateCopyWithImpl(
      _FiltersState _value, $Res Function(_FiltersState) _then)
      : super(_value, (v) => _then(v as _FiltersState));

  @override
  _FiltersState get _value => super._value as _FiltersState;

  @override
  $Res call({
    Object? order = freezed,
    Object? betweenPrice = freezed,
    Object? betweenDates = freezed,
  }) {
    return _then(_FiltersState(
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as List<FilterImmovables>,
      betweenPrice: betweenPrice == freezed
          ? _value.betweenPrice
          : betweenPrice // ignore: cast_nullable_to_non_nullable
              as FilterBetweenImmovables?,
      betweenDates: betweenDates == freezed
          ? _value.betweenDates
          : betweenDates // ignore: cast_nullable_to_non_nullable
              as FilterBetweenImmovables?,
    ));
  }
}

/// @nodoc

class _$_FiltersState extends _FiltersState {
  const _$_FiltersState(
      {this.order = const [], this.betweenPrice, this.betweenDates})
      : super._();

  @JsonKey()
  @override
  final List<FilterImmovables> order;
  @override
  final FilterBetweenImmovables? betweenPrice;
  @override
  final FilterBetweenImmovables? betweenDates;

  @override
  String toString() {
    return 'FiltersState(order: $order, betweenPrice: $betweenPrice, betweenDates: $betweenDates)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _FiltersState &&
            const DeepCollectionEquality().equals(other.order, order) &&
            const DeepCollectionEquality()
                .equals(other.betweenPrice, betweenPrice) &&
            const DeepCollectionEquality()
                .equals(other.betweenDates, betweenDates));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(order),
      const DeepCollectionEquality().hash(betweenPrice),
      const DeepCollectionEquality().hash(betweenDates));

  @JsonKey(ignore: true)
  @override
  _$FiltersStateCopyWith<_FiltersState> get copyWith =>
      __$FiltersStateCopyWithImpl<_FiltersState>(this, _$identity);
}

abstract class _FiltersState extends FiltersState {
  const factory _FiltersState(
      {List<FilterImmovables> order,
      FilterBetweenImmovables? betweenPrice,
      FilterBetweenImmovables? betweenDates}) = _$_FiltersState;
  const _FiltersState._() : super._();

  @override
  List<FilterImmovables> get order;
  @override
  FilterBetweenImmovables? get betweenPrice;
  @override
  FilterBetweenImmovables? get betweenDates;
  @override
  @JsonKey(ignore: true)
  _$FiltersStateCopyWith<_FiltersState> get copyWith =>
      throw _privateConstructorUsedError;
}
