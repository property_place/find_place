part of 'filters_provider.dart';

class FiltersNotifier extends StateNotifier<FiltersState> {
  FiltersNotifier() : super(FiltersState()) {
    state = state.copyWith(order: order);
  }

  bool active = false;

  void filterOrder({required String type, required String name}) {
    if (type == TypeFilter.order.name) {
      state = state.copyWith(order: [
        ...state.order.map((data) {
          if (data.name == name) {
            active = !data.active;
            return FilterImmovables(
                name: data.name,
                type: data.type,
                active: active,
                sort: data.sort);
          }
          return FilterImmovables(
            name: data.name,
            type: data.type,
            sort: data.sort,
            active: false,
          );
        })
      ]);
    }

    print(state.order);
  }

  void filterPriceMinimum(String? minimum) {
    if (minimum != null) {
      state = state.copyWith(
        betweenPrice: FilterBetweenImmovables(
          minimum: minimum,
          maximum: state.betweenPrice?.maximum ?? '',
        ),
      );
    }
  }

  void filterPriceMaximum(String? maximum) {
    if (maximum != null) {
      state = state.copyWith(
        betweenPrice: FilterBetweenImmovables(
          maximum: maximum,
          minimum: state.betweenPrice?.minimum ?? '',
        ),
      );
    }
  }

  void filterDateStart(String? dateMinimum) {
    if (dateMinimum != null && dateMinimum != '') {
      state = state.copyWith(
        betweenDates: FilterBetweenImmovables(
          minimum: dateMinimum,
          maximum: state.betweenDates?.maximum ?? '',
        ),
      );
    }
  }

  void filterDateEnd(String? dateMaximum) {
    if (dateMaximum != null && dateMaximum != '') {
      state = state.copyWith(
        betweenDates: FilterBetweenImmovables(
          maximum: dateMaximum,
          minimum: state.betweenDates?.minimum ?? '',
        ),
      );
    }
  }

  void filterApply() {
    print(state);
  }

  void filterReset() {
    active = false;
    state = state.copyWith(
      order: [
        ...state.order.map(
          (e) => FilterImmovables(
            active: false,
            type: e.type,
            name: e.name,
            sort: e.sort
          ),
        ),
      ],
      betweenDates: FilterBetweenImmovables(minimum: '', maximum: ''),
      betweenPrice: FilterBetweenImmovables(minimum: '', maximum: ''),
    );
  }
}
