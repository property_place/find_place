import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../auth/data/models/user_model.dart';

part 'immovables_model.g.dart';

@JsonSerializable()
class ImmovablesModel extends Equatable {
  ImmovablesModel(
      {required this.ok, this.docs, this.totalPages, this.page, this.error});

  factory ImmovablesModel.fromJson(Map<String, dynamic> json) =>
      _$ImmovablesModelFromJson(json);
  Map<String, dynamic> toJson() => _$ImmovablesModelToJson(this);

  final bool ok;
  final List<DocImmovableModel>? docs;
  final int? page;
  final int? totalPages;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocImmovableModel extends Equatable {
  DocImmovableModel({
    required this.immovableId,
    required this.description,
    required this.characteristic,
    required this.detail,
    required this.price,
    required this.state,
    required this.imageId,
    required this.cityId,
    required this.streetId,
    required this.contractId,
    required this.categoryId,
    required this.userId,
  });

  factory DocImmovableModel.fromJson(Map<String, dynamic> json) =>
      _$DocImmovableModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocImmovableModelToJson(this);

  @JsonKey(name: 'immovable_id')
  final String immovableId;
  final String description;
  final String? characteristic;
  final String? detail;
  final String price;
  final String state;
  @JsonKey(name: 'image_id')
  final String imageId;
  @JsonKey(name: 'city_id')
  final String cityId;
  @JsonKey(name: 'street_id')
  final String streetId;
  @JsonKey(name: 'contract_id')
  final String contractId;
  @JsonKey(name: 'category_id')
  final String categoryId;
  @JsonKey(name: 'user_id')
  final String userId;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class ImmovablesExhibitionModel extends Equatable {
  ImmovablesExhibitionModel(
      {required this.ok, this.docs, this.totalPages, this.page, this.error});

  factory ImmovablesExhibitionModel.fromJson(Map<String, dynamic> json) =>
      _$ImmovablesExhibitionModelFromJson(json);
  Map<String, dynamic> toJson() => _$ImmovablesExhibitionModelToJson(this);

  final bool ok;
  final List<DocImmovableExhibitionModel>? docs;
  final int? page;
  final int? totalPages;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocImmovableExhibitionModel {
  DocImmovableExhibitionModel({
    required this.immovableId,
    required this.description,
    required this.price,
    required this.publishedDate,
    required this.state,
    required this.outsideFrontImage,
  });

  factory DocImmovableExhibitionModel.fromJson(Map<String, dynamic> json) =>
      _$DocImmovableExhibitionModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocImmovableExhibitionModelToJson(this);

  @JsonKey(name: 'immovable_id')
  final String immovableId;
  final String description;
  final int price;
  @JsonKey(name: 'published_date')
  final String publishedDate;
  final String state;
  @JsonKey(name: 'outside_front_image')
  final String outsideFrontImage;
}
