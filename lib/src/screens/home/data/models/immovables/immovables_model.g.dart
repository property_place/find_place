// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'immovables_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImmovablesModel _$ImmovablesModelFromJson(Map<String, dynamic> json) =>
    ImmovablesModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>?)
          ?.map((e) => DocImmovableModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int?,
      page: json['page'] as int?,
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ImmovablesModelToJson(ImmovablesModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'page': instance.page,
      'totalPages': instance.totalPages,
      'error': instance.error,
    };

DocImmovableModel _$DocImmovableModelFromJson(Map<String, dynamic> json) =>
    DocImmovableModel(
      immovableId: json['immovable_id'] as String,
      description: json['description'] as String,
      characteristic: json['characteristic'] as String?,
      detail: json['detail'] as String?,
      price: json['price'] as String,
      state: json['state'] as String,
      imageId: json['image_id'] as String,
      cityId: json['city_id'] as String,
      streetId: json['street_id'] as String,
      contractId: json['contract_id'] as String,
      categoryId: json['category_id'] as String,
      userId: json['user_id'] as String,
    );

Map<String, dynamic> _$DocImmovableModelToJson(DocImmovableModel instance) =>
    <String, dynamic>{
      'immovable_id': instance.immovableId,
      'description': instance.description,
      'characteristic': instance.characteristic,
      'detail': instance.detail,
      'price': instance.price,
      'state': instance.state,
      'image_id': instance.imageId,
      'city_id': instance.cityId,
      'street_id': instance.streetId,
      'contract_id': instance.contractId,
      'category_id': instance.categoryId,
      'user_id': instance.userId,
    };

ImmovablesExhibitionModel _$ImmovablesExhibitionModelFromJson(
        Map<String, dynamic> json) =>
    ImmovablesExhibitionModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>?)
          ?.map((e) =>
              DocImmovableExhibitionModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int?,
      page: json['page'] as int?,
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ImmovablesExhibitionModelToJson(
        ImmovablesExhibitionModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'page': instance.page,
      'totalPages': instance.totalPages,
      'error': instance.error,
    };

DocImmovableExhibitionModel _$DocImmovableExhibitionModelFromJson(
        Map<String, dynamic> json) =>
    DocImmovableExhibitionModel(
      immovableId: json['immovable_id'] as String,
      description: json['description'] as String,
      price: json['price'] as int,
      publishedDate: json['published_date'] as String,
      state: json['state'] as String,
      outsideFrontImage: json['outside_front_image'] as String,
    );

Map<String, dynamic> _$DocImmovableExhibitionModelToJson(
        DocImmovableExhibitionModel instance) =>
    <String, dynamic>{
      'immovable_id': instance.immovableId,
      'description': instance.description,
      'price': instance.price,
      'published_date': instance.publishedDate,
      'state': instance.state,
      'outside_front_image': instance.outsideFrontImage,
    };
