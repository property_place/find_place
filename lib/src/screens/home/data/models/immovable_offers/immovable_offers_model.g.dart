// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'immovable_offers_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImmovableOffersModel _$ImmovableOffersModelFromJson(
        Map<String, dynamic> json) =>
    ImmovableOffersModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>?)
          ?.map((e) => DocOffersModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int?,
      page: json['page'] as int?,
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ImmovableOffersModelToJson(
        ImmovableOffersModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'page': instance.page,
      'totalPages': instance.totalPages,
      'error': instance.error,
    };

DocOffersModel _$DocOffersModelFromJson(Map<String, dynamic> json) =>
    DocOffersModel(
      immovableId: json['immovable_id'] as String,
      description: json['description'] as String,
      offerId: json['offer_id'] as String,
      image: json['image'] as String,
      startDate: json['start_date'] as String,
      endDate: json['end_date'] as String,
      newPrice: json['new_price'] as int,
      outsideFrontImage: json['outside_front_image'] as String,
    );

Map<String, dynamic> _$DocOffersModelToJson(DocOffersModel instance) =>
    <String, dynamic>{
      'immovable_id': instance.immovableId,
      'description': instance.description,
      'offer_id': instance.offerId,
      'image': instance.image,
      'start_date': instance.startDate,
      'end_date': instance.endDate,
      'new_price': instance.newPrice,
      'outside_front_image': instance.outsideFrontImage,
    };
