import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../auth/data/models/user_model.dart';

part 'immovable_offers_model.g.dart';

@JsonSerializable()
class ImmovableOffersModel extends Equatable {
  ImmovableOffersModel(
      {required this.ok, this.docs, this.totalPages, this.page, this.error});

  factory ImmovableOffersModel.fromJson(Map<String, dynamic> json) =>
      _$ImmovableOffersModelFromJson(json);
  Map<String, dynamic> toJson() => _$ImmovableOffersModelToJson(this);

  final bool ok;
  final List<DocOffersModel>? docs;
  final int? page;
  final int? totalPages;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocOffersModel extends Equatable {
  DocOffersModel({
    required this.immovableId,
    required this.description,
    required this.offerId,
    required this.image,
    required this.startDate,
    required this.endDate,
    required this.newPrice,
    required this.outsideFrontImage,
  });

  factory DocOffersModel.fromJson(Map<String, dynamic> json) =>
      _$DocOffersModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocOffersModelToJson(this);

  @JsonKey(name: 'immovable_id')
  final String immovableId;
  final String description;
  @JsonKey(name: 'offer_id')
  final String offerId;
  final String image;
  @JsonKey(name: 'start_date')
  final String startDate;
  @JsonKey(name: 'end_date')
  final String endDate;
  @JsonKey(name: 'new_price')
  final int newPrice;
  @JsonKey(name: 'outside_front_image')
  final String outsideFrontImage;

  @override
  List<Object?> get props => [];
}
