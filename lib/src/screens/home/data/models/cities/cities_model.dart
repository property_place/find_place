import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'cities_model.g.dart';

@JsonSerializable()
class CitiesModel extends Equatable {
  CitiesModel({
    required this.ok,
    required this.docs,
  });

  factory CitiesModel.fromJson(Map<String, dynamic> json) =>
      _$CitiesModelFromJson(json);
  Map<String, dynamic> toJson() => _$CitiesModelToJson(this);

  final bool ok;
  final List<DocCitiesModel> docs;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocCitiesModel extends Equatable {
  DocCitiesModel({
    required this.cityId,
    required this.city,
  });

  factory DocCitiesModel.fromJson(Map<String, dynamic> json) =>
      _$DocCitiesModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocCitiesModelToJson(this);

  @JsonKey(name: 'city_id')
  final String cityId;
  final String city;

  @override
  List<Object?> get props => [];
}
