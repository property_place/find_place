import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../auth/data/models/user_model.dart';

part 'api_cities_model.g.dart';

@JsonSerializable()
class ApiCitiesModel extends Equatable {
  ApiCitiesModel({
    required this.ok,
    this.docs,
    this.totalPages,
    this.page,
    this.error,
  });

  factory ApiCitiesModel.fromJson(Map<String, dynamic> json) =>
      _$ApiCitiesModelFromJson(json);
  Map<String, dynamic> toJson() => _$ApiCitiesModelToJson(this);

  final bool ok;
  final List<DocApiCitiesModel>? docs;
  final int? totalPages;
  final int? page;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocApiCitiesModel extends Equatable {
  DocApiCitiesModel({
    required this.city,
  });

  factory DocApiCitiesModel.fromJson(Map<String, dynamic> json) =>
      _$DocApiCitiesModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocApiCitiesModelToJson(this);

  final String city;
  @override
  List<Object?> get props => [];
}
