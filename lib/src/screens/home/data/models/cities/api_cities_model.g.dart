// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_cities_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiCitiesModel _$ApiCitiesModelFromJson(Map<String, dynamic> json) =>
    ApiCitiesModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>?)
          ?.map((e) => DocApiCitiesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int?,
      page: json['page'] as int?,
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ApiCitiesModelToJson(ApiCitiesModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'totalPages': instance.totalPages,
      'page': instance.page,
      'error': instance.error,
    };

DocApiCitiesModel _$DocApiCitiesModelFromJson(Map<String, dynamic> json) =>
    DocApiCitiesModel(
      city: json['city'] as String,
    );

Map<String, dynamic> _$DocApiCitiesModelToJson(DocApiCitiesModel instance) =>
    <String, dynamic>{
      'city': instance.city,
    };
