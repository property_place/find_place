// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cities_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CitiesModel _$CitiesModelFromJson(Map<String, dynamic> json) => CitiesModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>)
          .map((e) => DocCitiesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CitiesModelToJson(CitiesModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
    };

DocCitiesModel _$DocCitiesModelFromJson(Map<String, dynamic> json) =>
    DocCitiesModel(
      cityId: json['city_id'] as String,
      city: json['city'] as String,
    );

Map<String, dynamic> _$DocCitiesModelToJson(DocCitiesModel instance) =>
    <String, dynamic>{
      'city_id': instance.cityId,
      'city': instance.city,
    };
