import '../../../utils/type_filter.dart';

class FilterImmovables {
  final String? name;
  final String? type;
  final bool active;
  final String? sort;
  FilterImmovables({this.name, this.sort, this.type, this.active = false});

  FilterImmovables copyWith(
      {String? name, String? type, bool? active, String? sort}) {
    return FilterImmovables(
        name: name ?? this.name,
        type: type ?? this.type,
        active: active ?? this.active,
        sort: sort ?? this.sort);
  }
}

class FilterBetweenImmovables {
  FilterBetweenImmovables(
      {this.minimum = '', this.maximum = '', this.sort = ''});
  final String minimum;
  final String maximum;
  final String sort;
}

List<FilterImmovables> order = [
  FilterImmovables(
      name: "Nombre ascendente",
      sort: FilterSort.description.name,
      type: "ASC",
      active: false),
  FilterImmovables(
      name: "Nombre descendente",
      sort: FilterSort.description.name,
      type: "DESC",
      active: false),
  FilterImmovables(
      name: "Menor precio",
      type: "ASC",
      sort: FilterSort.price.name,
      active: false),
  FilterImmovables(
      name: "Mayor precio",
      type: "DESC",
      sort: FilterSort.price.name,
      active: false),
  FilterImmovables(
      name: "Fecha ascendente",
      type: "ASC",
      sort: FilterSort.published_date.name,
      active: false),
  FilterImmovables(
      name: "Fecha descendente",
      type: "DESC",
      sort: FilterSort.published_date.name,
      active: false),
];
