import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'categories_model.g.dart';

@JsonSerializable()
class ListCategoriesModel extends Equatable {
  const ListCategoriesModel({required this.categories});

  factory ListCategoriesModel.fromJson(Map<String, dynamic> json) =>
      _$ListCategoriesModelFromJson(json);
  Map<String, dynamic> toJson() => _$ListCategoriesModelToJson(this);

  final List<CategoriesModel> categories;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class CategoriesModel extends Equatable {
  CategoriesModel({
    required this.ok,
    required this.docs,
  });

  factory CategoriesModel.fromJson(Map<String, dynamic> json) =>
      _$CategoriesModelFromJson(json);
  Map<String, dynamic> toJson() => _$CategoriesModelToJson(this);

  final bool ok;
  final List<DocCategoryModel> docs;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocCategoryModel extends Equatable {
  DocCategoryModel({
    required this.categoryId,
    required this.name,
    required this.image,
  });

  factory DocCategoryModel.fromJson(Map<String, dynamic> json) =>
      _$DocCategoryModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocCategoryModelToJson(this);

  @JsonKey(name: 'category_id')
  final String categoryId;
  final String name;
  final String image;

  @override
  List<Object?> get props => [];
}
