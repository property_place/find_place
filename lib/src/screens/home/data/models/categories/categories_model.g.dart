// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'categories_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListCategoriesModel _$ListCategoriesModelFromJson(Map<String, dynamic> json) =>
    ListCategoriesModel(
      categories: (json['categories'] as List<dynamic>)
          .map((e) => CategoriesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ListCategoriesModelToJson(
        ListCategoriesModel instance) =>
    <String, dynamic>{
      'categories': instance.categories,
    };

CategoriesModel _$CategoriesModelFromJson(Map<String, dynamic> json) =>
    CategoriesModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>)
          .map((e) => DocCategoryModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CategoriesModelToJson(CategoriesModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
    };

DocCategoryModel _$DocCategoryModelFromJson(Map<String, dynamic> json) =>
    DocCategoryModel(
      categoryId: json['category_id'] as String,
      name: json['name'] as String,
      image: json['image'] as String,
    );

Map<String, dynamic> _$DocCategoryModelToJson(DocCategoryModel instance) =>
    <String, dynamic>{
      'category_id': instance.categoryId,
      'name': instance.name,
      'image': instance.image,
    };
