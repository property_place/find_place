import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../../core/global/environment.dart';
import '../models/categories/categories_model.dart';

abstract class ICategoriesRepository {
  Future<CategoriesModel> getCategories();
}

class CategoriesRepository extends ICategoriesRepository {
  CategoriesRepository(this.client);
  final http.Client client;

  @override
  Future<CategoriesModel> getCategories() async {
    try {
      var url = Uri.parse('${EnvironmentApi.linkApi}/categories/getCategories');
      final response = await client.get(url);
      if (response.statusCode == 200) {
        return CategoriesModel.fromJson(json.decode(response.body));
      } else {
        throw Exception();
      }
    } on Exception {
      throw Exception('Error interno');
    }
  }
}
