import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../../core/global/environment.dart';
import '../models/immovable_offers/immovable_offers_model.dart';
import '../models/immovables/immovables_model.dart';

abstract class IImmovablesRepository {
  Future<ImmovablesExhibitionModel> getImmovables(
      int page, int limit, String categoryId, String city, List<Map<String, dynamic>> filters);
  Future<ImmovableOffersModel> getImmovableOffers(
      int page, int limit, String categoryId, String city);
}

class ImmovablesRepository extends IImmovablesRepository {
  ImmovablesRepository(this.client);
  final http.Client client;

  @override
  Future<ImmovablesExhibitionModel> getImmovables(int page, int limit, String categoryId, String city, List<Map<String, dynamic>> filters) async {
    final url = Uri.parse('${EnvironmentApi.linkApi}/immovables/getImmovables/$page/$limit/$categoryId/$city');
    final response = await client.post(url,
    headers: {
      "Accept": "application/json",
      'Content-Type': 'application/json',
    },
     body: json.encode(filters, toEncodable: (value) => value.toList()));
    if (response.statusCode == 200) {
      return ImmovablesExhibitionModel.fromJson(json.decode(response.body));
    } else if (response.statusCode == 500) {
      final data = ImmovablesExhibitionModel.fromJson(json.decode(response.body));
      throw Exception(data.error);
    } else {
      throw Exception('Error');
    }
  }

  @override
  Future<ImmovableOffersModel> getImmovableOffers( int page, int limit, String categoryId, String city) async {
    final url = Uri.parse('${EnvironmentApi.linkApi}/immovables/getImmovableOffers/$page/$limit/$categoryId/$city');
    final response = await client.get(url);
    if (response.statusCode == 200) {
      return ImmovableOffersModel.fromJson(json.decode(response.body));
    } else if (response.statusCode == 500) {
      final data = ImmovablesExhibitionModel.fromJson(json.decode(response.body));
      throw Exception(data.error);
    } else {
      throw Exception('Error');
    }
  }
}
