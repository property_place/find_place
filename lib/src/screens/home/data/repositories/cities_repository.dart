import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../../core/global/environment.dart';
import '../models/cities/api_cities_model.dart';

abstract class ICitiesRepository {
  Future<ApiCitiesModel> getCities(int page, int limit);
  Future<ApiCitiesModel> getSearchCities(String valueSearch);
}

class CitiesRepository extends ICitiesRepository {
  CitiesRepository(this.client);
  final http.Client client;

  @override
  Future<ApiCitiesModel> getCities(int page, int limit) async {
    final url =  Uri.parse('${EnvironmentApi.linkApi}/cities/getCities/$page/$limit');
    final response = await client.get(url);
    if (response.statusCode == 200) {
      return ApiCitiesModel.fromJson(json.decode(response.body));
    } else if (response.statusCode == 500) {
      final data = ApiCitiesModel.fromJson(json.decode(response.body));
      throw Exception(data.error);
    } else {
      throw Exception('Error');
    }
  }

  @override
  Future<ApiCitiesModel> getSearchCities(String valueSearch) async {
    final url = Uri.parse('${EnvironmentApi.linkApi}/cities/getSearchCities/$valueSearch');
    final response = await client.get(url);
    if (response.statusCode == 200) {
      return ApiCitiesModel.fromJson(json.decode(response.body));
    } else if (response.statusCode == 500) {
      final data = ApiCitiesModel.fromJson(json.decode(response.body));
      throw Exception(data.error);
    } else {
      throw Exception('Error');
    }
  }
}
