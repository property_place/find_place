import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

part 'keyboard_emoji_notifier.dart';

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************

final resolverKeyboardEmojiService =
    ChangeNotifierProvider((ref) => ChatKeyboardEmoji());
