part of 'keyboard_emoji_provider.dart';

class ChatKeyboardEmoji extends ChangeNotifier {
  ChatKeyboardEmoji() {
    keyboardVisibility();
  }
  late StreamSubscription<bool> keyboardSubscription;

  bool _isEmojiVisibility = false;

  bool get isEmojiVisibility => _isEmojiVisibility;

  bool _isKeyboardVisible = false;

  bool get isKeyboardVisible => _isKeyboardVisible;

  set isEmojiVisibility(bool value) {
    _isEmojiVisibility = value;
    print(value);
    notifyListeners();
  }

  void keyboardVisibility() {
    var kbVisibilityCTR = KeyboardVisibilityController();
    keyboardSubscription = kbVisibilityCTR.onChange.listen((isKeyboardV) {
      _isKeyboardVisible = isKeyboardV;
      if (_isKeyboardVisible && isEmojiVisibility) {
        isEmojiVisibility = false;
      }
    });
  }

  @override
  void dispose() {
    keyboardSubscription.cancel();
    super.dispose();
  }
}
