import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/models/chat_immovables/chat_immovables_model.dart';

part 'my_immovables_state.freezed.dart';

@freezed
abstract class ChatMyImmovablesState with _$ChatMyImmovablesState {
  const factory ChatMyImmovablesState({
      @Default(1) int page,
      @Default([]) List<DocChatImmovablesModel> docsImmovable,
      @Default(true) bool isLoading,
      @Default('') String isLoadMoreError,
      @Default(false) bool isLoadMoreDone,
      @Default(false) bool isFetching
  }) = _ChatMyImmovablesState;

  const ChatMyImmovablesState._();
}
