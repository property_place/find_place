part of 'my_immovables_provider.dart';

class ChatListImmovable extends StateNotifier<ChatMyImmovablesState> {
  ChatListImmovable(
      {required IChatsRepository ichatRepository,
      required FlutterSecureStorage storage})
      : _chatRepository = ichatRepository,
        _storage = storage,
        super(ChatMyImmovablesState()) {
    _init();
  }

  final IChatsRepository _chatRepository;
  final FlutterSecureStorage _storage;
  final int _limitPage = 10;

  _init({int? initPage}) async {
    final page = initPage ?? state.page;

    state = state.copyWith(isLoading: true);

    final chatListImmovable =
        await _chatRepository.getChatForImmovable(page, _limitPage);

    if (!chatListImmovable.ok) {
      state = state.copyWith(
        isLoading: false,
        isLoadMoreError: chatListImmovable.error!.msg,
      );
      return;
    }
    state = state.copyWith(
        page: page,
        isLoading: false,
        docsImmovable: chatListImmovable.docs!,
        isFetching: false,
        isLoadMoreError: '');
  }

  void loadMoreStores() async {
    if (state.isLoadMoreDone) return;
    if (state.isLoading) return;

    state = state.copyWith(
      isLoading: true,
      isLoadMoreDone: false,
      isLoadMoreError: '',
    );
    final chatListImmovable =
        await _chatRepository.getChatForImmovable(state.page + 1, _limitPage);

    if (!chatListImmovable.ok) {
      state = state.copyWith(
        isLoadMoreError: chatListImmovable.error!.msg,
        isLoading: false,
      );
      return;
    }
    if (chatListImmovable.docs!.isNotEmpty) {
      state = state.copyWith(
        page: state.page + 1,
        isLoading: false,
        isLoadMoreDone:
            !(chatListImmovable.page! < chatListImmovable.totalPages!),
        docsImmovable: [...state.docsImmovable, ...chatListImmovable.docs!],
        isFetching: false,
      );
    } else {
      state = state.copyWith(
        isLoading: false,
        isLoadMoreDone:
            !(chatListImmovable.page! < chatListImmovable.totalPages!),
        isFetching: false,
      );
    }
  }

  Future<void> refresh() async {
    _init(initPage: 1);
  }

  void isPaginate() {
    state = state.copyWith(isFetching: true);
  }

  void newImmovableChat(dynamic payload) {
    final newImmovable = DocChatImmovablesModel(
        immovableId: payload['immovable_id'],
        description: payload['description'],
        outsideFrontImage: payload['outside_front_image'],
        quantity: payload['quantity'],
        state: payload['state']);

    if (state.docsImmovable.contains(newImmovable)) {
      var index = state.docsImmovable.indexOf(newImmovable);
      state = state.copyWith(
        docsImmovable: state.docsImmovable..[index] = newImmovable,
      );
    } else {
      state = state.copyWith(
        docsImmovable: [newImmovable, ...state.docsImmovable],
      );
    }
  }
}
