import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;

import '../../data/models/chat_immovables/chat_immovables_model.dart';
import '../../data/repositories/chats_repository.dart';
import 'my_immovables_state.dart';

part 'my_immovables_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************
final client = http.Client();
final _storage = FlutterSecureStorage();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************

final resolverChatImmovables =
    StateNotifierProvider<ChatListImmovable, ChatMyImmovablesState>(
  (ref) {
    return ChatListImmovable(
      ichatRepository: ref.watch(_chtListImmRepositoryProvider),
      storage: _storage);
  }
);

// *********************************************************************
// => Repository
// *********************************************************************

final _chtListImmRepositoryProvider = Provider<IChatsRepository>(
  (ref) => ChatsRepository(client: client, storage: _storage),
);

// *********************************************************************
// => Data
// *********************************************************************

// final keyProvider = StateProvider<String>((ref) {
//   return '';
// });

final resolverChatListImmService =
    StateProvider<List<DocChatImmovablesModel>>((ref) {
  final chatListImmState = ref.watch(resolverChatImmovables);
  return chatListImmState.docsImmovable;
});

// final seletedCityCRTProvider = StateProvider((ref) => TextEditingController());
