// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'my_immovables_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ChatMyImmovablesStateTearOff {
  const _$ChatMyImmovablesStateTearOff();

  _ChatMyImmovablesState call(
      {int page = 1,
      List<DocChatImmovablesModel> docsImmovable = const [],
      bool isLoading = true,
      String isLoadMoreError = '',
      bool isLoadMoreDone = false,
      bool isFetching = false}) {
    return _ChatMyImmovablesState(
      page: page,
      docsImmovable: docsImmovable,
      isLoading: isLoading,
      isLoadMoreError: isLoadMoreError,
      isLoadMoreDone: isLoadMoreDone,
      isFetching: isFetching,
    );
  }
}

/// @nodoc
const $ChatMyImmovablesState = _$ChatMyImmovablesStateTearOff();

/// @nodoc
mixin _$ChatMyImmovablesState {
  int get page => throw _privateConstructorUsedError;
  List<DocChatImmovablesModel> get docsImmovable =>
      throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  String get isLoadMoreError => throw _privateConstructorUsedError;
  bool get isLoadMoreDone => throw _privateConstructorUsedError;
  bool get isFetching => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ChatMyImmovablesStateCopyWith<ChatMyImmovablesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatMyImmovablesStateCopyWith<$Res> {
  factory $ChatMyImmovablesStateCopyWith(ChatMyImmovablesState value,
          $Res Function(ChatMyImmovablesState) then) =
      _$ChatMyImmovablesStateCopyWithImpl<$Res>;
  $Res call(
      {int page,
      List<DocChatImmovablesModel> docsImmovable,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class _$ChatMyImmovablesStateCopyWithImpl<$Res>
    implements $ChatMyImmovablesStateCopyWith<$Res> {
  _$ChatMyImmovablesStateCopyWithImpl(this._value, this._then);

  final ChatMyImmovablesState _value;
  // ignore: unused_field
  final $Res Function(ChatMyImmovablesState) _then;

  @override
  $Res call({
    Object? page = freezed,
    Object? docsImmovable = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_value.copyWith(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      docsImmovable: docsImmovable == freezed
          ? _value.docsImmovable
          : docsImmovable // ignore: cast_nullable_to_non_nullable
              as List<DocChatImmovablesModel>,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$ChatMyImmovablesStateCopyWith<$Res>
    implements $ChatMyImmovablesStateCopyWith<$Res> {
  factory _$ChatMyImmovablesStateCopyWith(_ChatMyImmovablesState value,
          $Res Function(_ChatMyImmovablesState) then) =
      __$ChatMyImmovablesStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int page,
      List<DocChatImmovablesModel> docsImmovable,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class __$ChatMyImmovablesStateCopyWithImpl<$Res>
    extends _$ChatMyImmovablesStateCopyWithImpl<$Res>
    implements _$ChatMyImmovablesStateCopyWith<$Res> {
  __$ChatMyImmovablesStateCopyWithImpl(_ChatMyImmovablesState _value,
      $Res Function(_ChatMyImmovablesState) _then)
      : super(_value, (v) => _then(v as _ChatMyImmovablesState));

  @override
  _ChatMyImmovablesState get _value => super._value as _ChatMyImmovablesState;

  @override
  $Res call({
    Object? page = freezed,
    Object? docsImmovable = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_ChatMyImmovablesState(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      docsImmovable: docsImmovable == freezed
          ? _value.docsImmovable
          : docsImmovable // ignore: cast_nullable_to_non_nullable
              as List<DocChatImmovablesModel>,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ChatMyImmovablesState extends _ChatMyImmovablesState {
  const _$_ChatMyImmovablesState(
      {this.page = 1,
      this.docsImmovable = const [],
      this.isLoading = true,
      this.isLoadMoreError = '',
      this.isLoadMoreDone = false,
      this.isFetching = false})
      : super._();

  @JsonKey()
  @override
  final int page;
  @JsonKey()
  @override
  final List<DocChatImmovablesModel> docsImmovable;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final String isLoadMoreError;
  @JsonKey()
  @override
  final bool isLoadMoreDone;
  @JsonKey()
  @override
  final bool isFetching;

  @override
  String toString() {
    return 'ChatMyImmovablesState(page: $page, docsImmovable: $docsImmovable, isLoading: $isLoading, isLoadMoreError: $isLoadMoreError, isLoadMoreDone: $isLoadMoreDone, isFetching: $isFetching)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ChatMyImmovablesState &&
            const DeepCollectionEquality().equals(other.page, page) &&
            const DeepCollectionEquality()
                .equals(other.docsImmovable, docsImmovable) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreError, isLoadMoreError) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreDone, isLoadMoreDone) &&
            const DeepCollectionEquality()
                .equals(other.isFetching, isFetching));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(page),
      const DeepCollectionEquality().hash(docsImmovable),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isLoadMoreError),
      const DeepCollectionEquality().hash(isLoadMoreDone),
      const DeepCollectionEquality().hash(isFetching));

  @JsonKey(ignore: true)
  @override
  _$ChatMyImmovablesStateCopyWith<_ChatMyImmovablesState> get copyWith =>
      __$ChatMyImmovablesStateCopyWithImpl<_ChatMyImmovablesState>(
          this, _$identity);
}

abstract class _ChatMyImmovablesState extends ChatMyImmovablesState {
  const factory _ChatMyImmovablesState(
      {int page,
      List<DocChatImmovablesModel> docsImmovable,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching}) = _$_ChatMyImmovablesState;
  const _ChatMyImmovablesState._() : super._();

  @override
  int get page;
  @override
  List<DocChatImmovablesModel> get docsImmovable;
  @override
  bool get isLoading;
  @override
  String get isLoadMoreError;
  @override
  bool get isLoadMoreDone;
  @override
  bool get isFetching;
  @override
  @JsonKey(ignore: true)
  _$ChatMyImmovablesStateCopyWith<_ChatMyImmovablesState> get copyWith =>
      throw _privateConstructorUsedError;
}
