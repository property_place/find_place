import 'package:freezed_annotation/freezed_annotation.dart';

import '../data/models/chat_messages_model.dart';

part 'chats_state.freezed.dart';

@freezed
abstract class ChatsState with _$ChatsState {
  const factory ChatsState(
      {@Default(1) int page,
      List<DocChatMessageRespModel>? chats,
      @Default(true) bool isLoading,
      @Default('') String isLoadMoreError,
      @Default(false) bool isLoadMoreDone,
      @Default(false) bool isFetching}) = _ChatsState;

  const ChatsState._();
}
