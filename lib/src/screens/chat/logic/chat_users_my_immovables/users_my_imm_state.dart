import 'package:freezed_annotation/freezed_annotation.dart';

import '../../data/models/chat_users/chat_users_model.dart';

part 'users_my_imm_state.freezed.dart';

@freezed
abstract class UsersMyImmState with _$UsersMyImmState {
  const factory UsersMyImmState({
    @Default(1) int page,
    List<DocChatUsersModel>? users,
    @Default(true) bool isLoading,
    @Default('') String isLoadMoreError,
    @Default(false) bool isLoadMoreDone,
    @Default(false) bool isFetching,
  }) = _UsersMyImmState;

  const UsersMyImmState._();
}
