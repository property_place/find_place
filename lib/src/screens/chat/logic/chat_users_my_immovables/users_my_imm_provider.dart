import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;

import '../../../auth/data/models/user_model.dart';
import '../../../auth/logic/auth_provider.dart';
import '../../data/models/chat_immovables/chat_immovables_model.dart';
import '../../data/models/chat_users/chat_users_model.dart';
import '../../data/repositories/chat_list_user_repository.dart';
import 'users_my_imm_state.dart';

part 'users_my_imm_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************
final client = http.Client();
final _storage = FlutterSecureStorage();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************
final resolverChatUsersMyImm =
    StateNotifierProvider<ChatUserMyImm, UsersMyImmState>(
  (ref) {

    return ChatUserMyImm(
    ref: ref,
    immovable: ref.watch(rSelectedImmovable),
    iChatListUserRepository: ref.watch(_chtListImmRepositoryProvider),
  );
  }
);

// *********************************************************************
// => Repository
// *********************************************************************

final _chtListImmRepositoryProvider = Provider<IChatListUserRepository>(
  (ref) => ChatListUserRepository(client: client, storage: _storage),
);

final rSelectedImmovable = StateProvider<DocChatImmovablesModel?>((ref) => null);

final chatListUserProvider =
    StateProvider<List<DocChatUsersModel>?>((ref) {
  final chatListImmState = ref.watch(resolverChatUsersMyImm);
  return chatListImmState.users;
});


