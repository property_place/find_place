part of 'users_my_imm_provider.dart';

class ChatUserMyImm extends StateNotifier<UsersMyImmState> {
  ChatUserMyImm(
      {required IChatListUserRepository iChatListUserRepository,
      required Ref ref,
      required DocChatImmovablesModel? immovable})
      : _iChatListUserRepository = iChatListUserRepository,
        _ref = ref,
        _immovable = immovable,
        super(UsersMyImmState());

  final IChatListUserRepository _iChatListUserRepository;
  final Ref _ref;
  final int _limitPage = 10;
  DocChatImmovablesModel? _immovable;
  DocUserModel? _userService;

  void initChatMyImm({int? initPage}) async {
    _userService = await _ref.read(userProvider.notifier).state;

    if (_immovable == null) {
      state = state.copyWith(isLoading: false);
      return;
    }

    final page = initPage ?? state.page;
    state = state.copyWith(isLoading: true);

    final chatImmovables =
        await _iChatListUserRepository.getChatUserForImmovable(
      page,
      _limitPage,
      _immovable!.immovableId,
    );

    if (!chatImmovables.ok) {
      state = state.copyWith(
        isLoading: false,
        isLoadMoreError: chatImmovables.error!.msg,
      );
    } else {
      state = state.copyWith(
          page: page,
          isLoading: false,
          users: chatImmovables.docs!,
          isFetching: false,
          isLoadMoreError: '');
    }
  }

  void loadMoreImmovable() async {
    if (state.isLoadMoreDone) return;
    if (state.isLoading) return;
    if (_immovable == null) return;

    state = state.copyWith(
      isLoading: true,
      isLoadMoreDone: false,
      isLoadMoreError: '',
    );
    final chatListImmovable =
        await _iChatListUserRepository.getChatUserForImmovable(
            state.page + 1, _limitPage, _immovable!.immovableId);

    if (!chatListImmovable.ok) {
      state = state.copyWith(
        isLoadMoreError: chatListImmovable.error!.msg,
        isLoading: false,
      );
      return;
    }
    if (chatListImmovable.docs!.isNotEmpty) {
      state = state.copyWith(
        page: state.page + 1,
        isLoading: false,
        isLoadMoreDone:
            !(chatListImmovable.page! < chatListImmovable.totalPages!),
        users: state.users != null
            ? [...state.users!, ...chatListImmovable.docs!]
            : null,
        isFetching: false,
      );
    } else {
      state = state.copyWith(
        isLoading: false,
        isLoadMoreDone:
            !(chatListImmovable.page! < chatListImmovable.totalPages!),
        isFetching: false,
      );
    }
  }

  Future<void> refresh() async {
    initChatMyImm(initPage: 1);
  }

  void isPaginate() {
    state = state.copyWith(isFetching: true);
  }

  void newOrUpdateChatUser(dynamic payload) async {
    switch (payload['type']) {
      case 0:
        newUserMessage(payload);
        break;
      case 1:
        updateUserLastMessage(payload);
        break;
      default:
        newUserMessage(payload);
    }
  }

  void newUserMessage(dynamic payload) {
    final newUser = DocChatUsersModel(
        userId: payload['user_id'],
        email: payload['email'],
        name: payload['name'],
        pLastName: payload['p_lastname'],
        online: payload['online'],
        lastMessage: payload['last_message'],
        immovableId: payload['immovable_id'],
        chatId: payload['chat_id'],
        time: payload['time'],
        rol: '');

    if (state.users != null) {
      final isExist =
          state.users!.where((element) => element.userId == newUser.userId);
      if (isExist.isEmpty) {
        state = state.copyWith(
          users: [newUser, ...state.users!],
        );
      }
    } else {
      state.copyWith(
        users: [newUser],
      );
    }
  }

  void updateUserLastMessage(dynamic payload) {
    if (payload == null) return;
    if (state.users != null) {
      final user = state.users!
          .firstWhere((element) => element.userId == payload['user_id']);
      final newUser = DocChatUsersModel(
          userId: user.userId,
          email: user.email,
          name: user.name,
          pLastName: user.pLastName,
          online: user.online,
          lastMessage: payload['last_message'],
          immovableId: user.immovableId,
          chatId: user.chatId,
          time: user.time,
          rol: user.rol);
      var index =
          state.users!.indexWhere((item) => item.userId == newUser.userId);
      state = state.copyWith(
        users: state.users!..[index] = newUser,
      );
      state.users!.sort((a, b) => a.time.compareTo(b.time));
    }
  }
}
