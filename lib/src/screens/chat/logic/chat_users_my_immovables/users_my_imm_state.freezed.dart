// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'users_my_imm_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$UsersMyImmStateTearOff {
  const _$UsersMyImmStateTearOff();

  _UsersMyImmState call(
      {int page = 1,
      List<DocChatUsersModel>? users,
      bool isLoading = true,
      String isLoadMoreError = '',
      bool isLoadMoreDone = false,
      bool isFetching = false}) {
    return _UsersMyImmState(
      page: page,
      users: users,
      isLoading: isLoading,
      isLoadMoreError: isLoadMoreError,
      isLoadMoreDone: isLoadMoreDone,
      isFetching: isFetching,
    );
  }
}

/// @nodoc
const $UsersMyImmState = _$UsersMyImmStateTearOff();

/// @nodoc
mixin _$UsersMyImmState {
  int get page => throw _privateConstructorUsedError;
  List<DocChatUsersModel>? get users => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  String get isLoadMoreError => throw _privateConstructorUsedError;
  bool get isLoadMoreDone => throw _privateConstructorUsedError;
  bool get isFetching => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UsersMyImmStateCopyWith<UsersMyImmState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UsersMyImmStateCopyWith<$Res> {
  factory $UsersMyImmStateCopyWith(
          UsersMyImmState value, $Res Function(UsersMyImmState) then) =
      _$UsersMyImmStateCopyWithImpl<$Res>;
  $Res call(
      {int page,
      List<DocChatUsersModel>? users,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class _$UsersMyImmStateCopyWithImpl<$Res>
    implements $UsersMyImmStateCopyWith<$Res> {
  _$UsersMyImmStateCopyWithImpl(this._value, this._then);

  final UsersMyImmState _value;
  // ignore: unused_field
  final $Res Function(UsersMyImmState) _then;

  @override
  $Res call({
    Object? page = freezed,
    Object? users = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_value.copyWith(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      users: users == freezed
          ? _value.users
          : users // ignore: cast_nullable_to_non_nullable
              as List<DocChatUsersModel>?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$UsersMyImmStateCopyWith<$Res>
    implements $UsersMyImmStateCopyWith<$Res> {
  factory _$UsersMyImmStateCopyWith(
          _UsersMyImmState value, $Res Function(_UsersMyImmState) then) =
      __$UsersMyImmStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int page,
      List<DocChatUsersModel>? users,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class __$UsersMyImmStateCopyWithImpl<$Res>
    extends _$UsersMyImmStateCopyWithImpl<$Res>
    implements _$UsersMyImmStateCopyWith<$Res> {
  __$UsersMyImmStateCopyWithImpl(
      _UsersMyImmState _value, $Res Function(_UsersMyImmState) _then)
      : super(_value, (v) => _then(v as _UsersMyImmState));

  @override
  _UsersMyImmState get _value => super._value as _UsersMyImmState;

  @override
  $Res call({
    Object? page = freezed,
    Object? users = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_UsersMyImmState(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      users: users == freezed
          ? _value.users
          : users // ignore: cast_nullable_to_non_nullable
              as List<DocChatUsersModel>?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_UsersMyImmState extends _UsersMyImmState {
  const _$_UsersMyImmState(
      {this.page = 1,
      this.users,
      this.isLoading = true,
      this.isLoadMoreError = '',
      this.isLoadMoreDone = false,
      this.isFetching = false})
      : super._();

  @JsonKey()
  @override
  final int page;
  @override
  final List<DocChatUsersModel>? users;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final String isLoadMoreError;
  @JsonKey()
  @override
  final bool isLoadMoreDone;
  @JsonKey()
  @override
  final bool isFetching;

  @override
  String toString() {
    return 'UsersMyImmState(page: $page, users: $users, isLoading: $isLoading, isLoadMoreError: $isLoadMoreError, isLoadMoreDone: $isLoadMoreDone, isFetching: $isFetching)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _UsersMyImmState &&
            const DeepCollectionEquality().equals(other.page, page) &&
            const DeepCollectionEquality().equals(other.users, users) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreError, isLoadMoreError) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreDone, isLoadMoreDone) &&
            const DeepCollectionEquality()
                .equals(other.isFetching, isFetching));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(page),
      const DeepCollectionEquality().hash(users),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isLoadMoreError),
      const DeepCollectionEquality().hash(isLoadMoreDone),
      const DeepCollectionEquality().hash(isFetching));

  @JsonKey(ignore: true)
  @override
  _$UsersMyImmStateCopyWith<_UsersMyImmState> get copyWith =>
      __$UsersMyImmStateCopyWithImpl<_UsersMyImmState>(this, _$identity);
}

abstract class _UsersMyImmState extends UsersMyImmState {
  const factory _UsersMyImmState(
      {int page,
      List<DocChatUsersModel>? users,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching}) = _$_UsersMyImmState;
  const _UsersMyImmState._() : super._();

  @override
  int get page;
  @override
  List<DocChatUsersModel>? get users;
  @override
  bool get isLoading;
  @override
  String get isLoadMoreError;
  @override
  bool get isLoadMoreDone;
  @override
  bool get isFetching;
  @override
  @JsonKey(ignore: true)
  _$UsersMyImmStateCopyWith<_UsersMyImmState> get copyWith =>
      throw _privateConstructorUsedError;
}
