import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;

import '../../../auth/data/models/user_model.dart';
import '../../../auth/logic/auth_provider.dart';
import '../../data/models/chat_users_imm/chat_users_imm_model.dart';
import '../../data/repositories/chat_users_imm_repository.dart';
import 'chat_users_state.dart';

part 'chat_users_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************
final client = http.Client();
final _storage = FlutterSecureStorage();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************

final resolverChatUsers =
    StateNotifierProvider<ChatUsers, ChatUsersState>((ref) {
  return ChatUsers(
      ref: ref, iChatUsersImmRepository: ref.watch(_chatUsersRepository));
});

// *********************************************************************
// => Repository
// *********************************************************************

final _chatUsersRepository = Provider<IChatUsersImmRepository>(
  (ref) => ChatUsersImmRepository(client: client, storage: _storage),
);

// final selectedImmovable = StateProvider<DocChtListImblModel?>((ref) => null);
final rChatUsers = StateProvider<List<DocChatUsersImmModel>?>((ref) {
  final chatListImmState = ref.watch(resolverChatUsers);
  return chatListImmState.users;
});
