part of 'chat_users_provider.dart';

class ChatUsers extends StateNotifier<ChatUsersState> {
  ChatUsers({
    required IChatUsersImmRepository iChatUsersImmRepository,
    required Ref ref,
  })  : _iChatUsersImmRepository = iChatUsersImmRepository,
        _ref = ref,
        super(ChatUsersState()) {
    userService = ref.read(userProvider);
    _initUsersChat();
  }

  final IChatUsersImmRepository _iChatUsersImmRepository;
  final Ref _ref;
  final int _limitPage = 10;
  DocUserModel? userService;

  void _initUsersChat({int? initPage}) async {
    final page = initPage ?? state.page;

    state = state.copyWith(isLoading: true);

    final chatUsers = await _iChatUsersImmRepository
        .getImmovableInquiryChatUsers(page, _limitPage);

    if (!chatUsers.ok) {
      state = state.copyWith(
        isLoading: false,
        isLoadMoreError: chatUsers.error!.msg,
      );
    } else {
      state = state.copyWith(
          page: page,
          isLoading: false,
          users: chatUsers.docs!,
          isFetching: false,
          isLoadMoreError: '');
    }
  }

  void loadMoreStores() async {
    if (state.isLoadMoreDone) return;
    if (state.isLoading) return;

    state = state.copyWith(
      isLoading: true,
      isLoadMoreDone: false,
      isLoadMoreError: '',
    );
    final chatListImmovable = await _iChatUsersImmRepository
        .getImmovableInquiryChatUsers(state.page + 1, _limitPage);

    if (!chatListImmovable.ok) {
      state = state.copyWith(
        isLoadMoreError: chatListImmovable.error!.msg,
        isLoading: false,
      );
      return;
    }
    if (chatListImmovable.docs!.isNotEmpty) {
      state = state.copyWith(
        page: state.page + 1,
        isLoading: false,
        isLoadMoreDone:
            !(chatListImmovable.page! < chatListImmovable.totalPages!),
        users: [...state.users!, ...chatListImmovable.docs!],
        isFetching: false,
      );
    } else {
      state = state.copyWith(
        isLoading: false,
        isLoadMoreDone:
            !(chatListImmovable.page! < chatListImmovable.totalPages!),
        isFetching: false,
      );
    }
  }

  Future<void> refresh() async {
    _initUsersChat(initPage: 1);
  }

  void isPaginate() {
    state = state.copyWith(isFetching: true);
  }

  Future<void> updateUserLastMessage(dynamic payload) async {
    if (payload == null) return;

    print(state.users);
    if (state.users == null) return;

    final rUser = state.users!.where(
      (element) => element.userId == payload['user_id'],
    );

    if (rUser.isEmpty) return;

    final user = rUser.first;
    final newUser = DocChatUsersImmModel(
        userId: user.userId,
        online: user.online,
        lastMessage: payload['last_message'],
        immovable: user.immovable,
        immovableId: user.immovableId,
        state: user.state,
        outsideFrontImage: user.outsideFrontImage,
        image: user.image,
        name: user.name,
        pLastName: user.pLastName,
        sLastName: user.sLastName,
        email: user.email,
        rol: user.rol,
        chatId: user.chatId,
        time: user.time);

    var index = state.users!.indexWhere(
      (item) => item.userId == newUser.userId,
    );

    state = state.copyWith(
      users: state.users!..[index] = newUser,
    );
    state.users!.sort((a, b) => a.time.compareTo(b.time));
  }
}
