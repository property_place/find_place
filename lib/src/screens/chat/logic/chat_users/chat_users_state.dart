import 'package:freezed_annotation/freezed_annotation.dart';
import '../../data/models/chat_users_imm/chat_users_imm_model.dart';

part 'chat_users_state.freezed.dart';

@freezed
abstract class ChatUsersState with _$ChatUsersState {
  const factory ChatUsersState({
    @Default(1) int page,
    List<DocChatUsersImmModel>? users,
    @Default(true) bool isLoading,
    @Default('') String isLoadMoreError,
    @Default(false) bool isLoadMoreDone,
    @Default(false) bool isFetching,
  }) = _ChatUsersState;

  const ChatUsersState._();
}
