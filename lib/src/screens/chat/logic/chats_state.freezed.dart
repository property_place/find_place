// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'chats_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ChatsStateTearOff {
  const _$ChatsStateTearOff();

  _ChatsState call(
      {int page = 1,
      List<DocChatMessageRespModel>? chats,
      bool isLoading = true,
      String isLoadMoreError = '',
      bool isLoadMoreDone = false,
      bool isFetching = false}) {
    return _ChatsState(
      page: page,
      chats: chats,
      isLoading: isLoading,
      isLoadMoreError: isLoadMoreError,
      isLoadMoreDone: isLoadMoreDone,
      isFetching: isFetching,
    );
  }
}

/// @nodoc
const $ChatsState = _$ChatsStateTearOff();

/// @nodoc
mixin _$ChatsState {
  int get page => throw _privateConstructorUsedError;
  List<DocChatMessageRespModel>? get chats =>
      throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  String get isLoadMoreError => throw _privateConstructorUsedError;
  bool get isLoadMoreDone => throw _privateConstructorUsedError;
  bool get isFetching => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ChatsStateCopyWith<ChatsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatsStateCopyWith<$Res> {
  factory $ChatsStateCopyWith(
          ChatsState value, $Res Function(ChatsState) then) =
      _$ChatsStateCopyWithImpl<$Res>;
  $Res call(
      {int page,
      List<DocChatMessageRespModel>? chats,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class _$ChatsStateCopyWithImpl<$Res> implements $ChatsStateCopyWith<$Res> {
  _$ChatsStateCopyWithImpl(this._value, this._then);

  final ChatsState _value;
  // ignore: unused_field
  final $Res Function(ChatsState) _then;

  @override
  $Res call({
    Object? page = freezed,
    Object? chats = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_value.copyWith(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      chats: chats == freezed
          ? _value.chats
          : chats // ignore: cast_nullable_to_non_nullable
              as List<DocChatMessageRespModel>?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$ChatsStateCopyWith<$Res> implements $ChatsStateCopyWith<$Res> {
  factory _$ChatsStateCopyWith(
          _ChatsState value, $Res Function(_ChatsState) then) =
      __$ChatsStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int page,
      List<DocChatMessageRespModel>? chats,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching});
}

/// @nodoc
class __$ChatsStateCopyWithImpl<$Res> extends _$ChatsStateCopyWithImpl<$Res>
    implements _$ChatsStateCopyWith<$Res> {
  __$ChatsStateCopyWithImpl(
      _ChatsState _value, $Res Function(_ChatsState) _then)
      : super(_value, (v) => _then(v as _ChatsState));

  @override
  _ChatsState get _value => super._value as _ChatsState;

  @override
  $Res call({
    Object? page = freezed,
    Object? chats = freezed,
    Object? isLoading = freezed,
    Object? isLoadMoreError = freezed,
    Object? isLoadMoreDone = freezed,
    Object? isFetching = freezed,
  }) {
    return _then(_ChatsState(
      page: page == freezed
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      chats: chats == freezed
          ? _value.chats
          : chats // ignore: cast_nullable_to_non_nullable
              as List<DocChatMessageRespModel>?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoadMoreError: isLoadMoreError == freezed
          ? _value.isLoadMoreError
          : isLoadMoreError // ignore: cast_nullable_to_non_nullable
              as String,
      isLoadMoreDone: isLoadMoreDone == freezed
          ? _value.isLoadMoreDone
          : isLoadMoreDone // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetching: isFetching == freezed
          ? _value.isFetching
          : isFetching // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ChatsState extends _ChatsState {
  const _$_ChatsState(
      {this.page = 1,
      this.chats,
      this.isLoading = true,
      this.isLoadMoreError = '',
      this.isLoadMoreDone = false,
      this.isFetching = false})
      : super._();

  @JsonKey()
  @override
  final int page;
  @override
  final List<DocChatMessageRespModel>? chats;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final String isLoadMoreError;
  @JsonKey()
  @override
  final bool isLoadMoreDone;
  @JsonKey()
  @override
  final bool isFetching;

  @override
  String toString() {
    return 'ChatsState(page: $page, chats: $chats, isLoading: $isLoading, isLoadMoreError: $isLoadMoreError, isLoadMoreDone: $isLoadMoreDone, isFetching: $isFetching)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ChatsState &&
            const DeepCollectionEquality().equals(other.page, page) &&
            const DeepCollectionEquality().equals(other.chats, chats) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreError, isLoadMoreError) &&
            const DeepCollectionEquality()
                .equals(other.isLoadMoreDone, isLoadMoreDone) &&
            const DeepCollectionEquality()
                .equals(other.isFetching, isFetching));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(page),
      const DeepCollectionEquality().hash(chats),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isLoadMoreError),
      const DeepCollectionEquality().hash(isLoadMoreDone),
      const DeepCollectionEquality().hash(isFetching));

  @JsonKey(ignore: true)
  @override
  _$ChatsStateCopyWith<_ChatsState> get copyWith =>
      __$ChatsStateCopyWithImpl<_ChatsState>(this, _$identity);
}

abstract class _ChatsState extends ChatsState {
  const factory _ChatsState(
      {int page,
      List<DocChatMessageRespModel>? chats,
      bool isLoading,
      String isLoadMoreError,
      bool isLoadMoreDone,
      bool isFetching}) = _$_ChatsState;
  const _ChatsState._() : super._();

  @override
  int get page;
  @override
  List<DocChatMessageRespModel>? get chats;
  @override
  bool get isLoading;
  @override
  String get isLoadMoreError;
  @override
  bool get isLoadMoreDone;
  @override
  bool get isFetching;
  @override
  @JsonKey(ignore: true)
  _$ChatsStateCopyWith<_ChatsState> get copyWith =>
      throw _privateConstructorUsedError;
}
