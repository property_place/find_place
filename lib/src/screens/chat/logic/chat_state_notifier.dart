part of 'chat_provider.dart';

enum ServeStatus { Online, Offine, Conneting }

class ChatService extends StateNotifier<ChatsState> {
  ChatService(
      {required IChatsRepository iChatRepository,
      required String? categoryId,
      required Ref ref,
      required AudioCache audioPlayer})
      : _ref = ref,
        _categoryId = categoryId,
        _iChatRepository = iChatRepository,
        _audioPlayer = audioPlayer,
        super(ChatsState()) {
    // _socketService = ref.read(resolverSocketService.notifier);
    // _selectedUser = ref.read(selectedUser);
    // _selectedImmovable = ref.read(selectedImmovable);
  }

  final IChatsRepository _iChatRepository;
  final String? _categoryId;
  final int _limitPage = 15;
  final Ref _ref;
  final AudioCache _audioPlayer;
  // late final SocketService _socketService;
  // late final DocChatUsersModel? _selectedUser;
  // late final DocChatImmovablesModel? _selectedImmovable;
  DocChatUsersModel? _receiverUser;

  void initChatsMessages({
    int? initPage,
    bool? delay,
    bool? isLoading,
  }) async {
    try {
      _receiverUser = await _ref.read(rSelectedUser.notifier).state;
      if (_categoryId == null && _receiverUser == null) return;
      final page = initPage ?? state.page;
      state = state.copyWith(isLoading: true);

      final chats = await _iChatRepository.getChatMessages(
          page, _limitPage, _receiverUser!.userId);

      state = state.copyWith(
          page: page,
          isLoading: false,
          chats: chats.docs!,
          isFetching: false,
          isLoadMoreError: '');
    } on Exception catch (e) {
      if (delay != null) await Future.delayed(Duration(milliseconds: 2000));
      state = state.copyWith(
        isLoading: false,
        isLoadMoreError: e.toString(),
      );
    }
  }

  void loadMoreChatMessages() async {
    if (_categoryId == null) return;
    if (state.isLoadMoreDone) return;
    if (state.isLoading) return;

    state = state.copyWith(
      isLoadMoreDone: false,
      isLoadMoreError: '',
      isLoading: true,
    );

    final chats = await _iChatRepository.getChatMessages(
        state.page + 1, _limitPage, _receiverUser!.userId);

    if (!chats.ok && chats.docs == null) {
      state = state.copyWith(isLoadMoreError: chats.error!.msg);
    } else if (chats.docs!.isNotEmpty) {
      state = state.copyWith(
          page: state.page + 1,
          isLoadMoreDone: !(chats.page! < chats.totalPages!),
          chats: state.chats != null
              ? [...state.chats!, ...chats.docs!]
              : [...chats.docs!],
          isFetching: false,
          isLoading: false);
    } else {
      state = state.copyWith(
          isLoadMoreDone: !(chats.page! < chats.totalPages!),
          isFetching: false,
          isLoading: false);
    }
  }

  Future<void> refresh() async => initChatsMessages(initPage: 1);

  void isPaginate() => state = state.copyWith(isFetching: true);

  void playAudioMessage() => _audioPlayer.play('ringtonesheartfindplace.mp3');

  @override
  void dispose() {
    _audioPlayer.clearAll();
    super.dispose();
  }
}
