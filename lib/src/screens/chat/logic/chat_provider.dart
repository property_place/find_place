import 'dart:async';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http/http.dart' as http;

import '../../auth/logic/auth_provider.dart';
import '../../home/logic/categories/category_tab_provider.dart';
import '../data/models/chat_messages_model.dart';
import '../data/models/chat_users/chat_users_model.dart';
import '../data/repositories/chats_repository.dart';
import '../view/Widgets/chat_mensaje.dart';
import 'chats_state.dart';

part 'chat_state_notifier.dart';

// *********************************************************************
// =>  Dependency Injection
// *********************************************************************
final _client = http.Client();
final _storage = FlutterSecureStorage();
final _audioPlayer = AudioCache();

// *********************************************************************
// => Logic  / StateNotifier
// *********************************************************************

final resolverChatService =
    StateNotifierProvider.autoDispose<ChatService, ChatsState>((ref) {
  final stateCategory = ref.watch(selectedCategory.notifier).state;
  ref.watch(
      resolverAuthService); // rebuild the CarsController when the user changes
  return ChatService(
      ref: ref,
      iChatRepository: ref.watch(_chatRepositoryProvider),
      categoryId: stateCategory?.categoryId,
      audioPlayer: _audioPlayer);
});

// *********************************************************************
// => Repository
// *********************************************************************

final _chatRepositoryProvider = Provider<IChatsRepository>(
  (ref) => ChatsRepository(client: _client, storage: _storage),
);

// *********************************************************************
// => Data
// *********************************************************************


final rSelectedUser = StateProvider<DocChatUsersModel?>((ref) => null);
// final selectedImmovableView =
//     StateProvider<ImmovableExhibitionViewModel?>((ref) => null);

final listChatProvider = StateProvider.autoDispose<List<DocChatMessageRespModel>?>((ref) {
  final messages = ref.watch(resolverChatService);
  return messages.chats;
});
/*
 * Se utiliza para validar el campo enviar mensajes, 
 * el cual es FALSO, si el campo de mensajes esta vacio,
 * y se activa a TRUE si se ha escrito algo
*/
final rIsWriting = StateProvider.autoDispose((ref) => false);

///  Menu chat
final rIsCheckedEnabledContract = StateProvider.autoDispose((ref) => false);

// *********************************************************************
// => SOCKET
// *********************************************************************

/*
 * Avisa cuando nos estan, escribiendo en el chat
*/
final rIsWritingReceiver = StateProvider.autoDispose((ref) => false);
