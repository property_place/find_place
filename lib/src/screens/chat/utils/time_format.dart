import 'package:intl/intl.dart';

String chatTimeFormat() {
  final now = DateTime.now();
  final formatter = DateFormat.jm();
  return formatter.format(now);
}

String convertChatTimeFormat(DateTime time) {
  final formatter = DateFormat.jm();
  return formatter.format(time);
}

