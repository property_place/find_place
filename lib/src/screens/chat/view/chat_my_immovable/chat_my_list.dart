import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../auth/logic/auth_provider.dart';
import '../../../home/utils/type_rol.dart';
import '../../../widgets/center_indicator.dart';
import '../chat_list_inmueble_and_users/chat_inmmuebles_screen.dart';
import '../chat_users/chat_users.dart';

class ChatListScreen extends ConsumerWidget {
  const ChatListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _userService = ref.watch(userProvider);
    //TODO: VALIDAR => infoOfTheConnectedPerson

    if (_userService == null) {
      return CenterIndicator();
    }

    return Scaffold(
      backgroundColor: primary,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ViewChatAppBarDetail(),
            if (_userService.rol.toLowerCase() == TypeRole.spark.name)
              _ContentTitle(),
            _userService.rol.toLowerCase() == TypeRole.blaze.name
                ? _ChatListMyInmuebleView()
                : Expanded(child: ChatUsersView())
          ],
        ),
      ),
    );
  }
}

class _ChatListMyInmuebleView extends HookWidget {
  _ChatListMyInmuebleView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _controller = useTabController(initialLength: 2);

    return Expanded(
      child: Column(
        children: [
          ColoredBox(
            color: oscuro,
            child: TabBar(
                controller: _controller,
                indicatorColor: activeTab,
                tabs: [
                  Tab(text: 'Mis Chat de inmueble'),
                  Tab(text: 'Chats de inmueble')
                ]),
          ),
          SizedBox(height: 10),
          Expanded(
            child: TabBarView(
                controller: _controller,
                physics: NeverScrollableScrollPhysics(),
                children: [ChatMyImmovable(), ChatUsersView()]),
          )
        ],
      ),
    );
  }
}

class _ContentTitle extends StatelessWidget {
  const _ContentTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: oscuro,
      alignment: Alignment.center,
      margin: const EdgeInsets.only(bottom: 10),
      child: Text(
        'Chats de inmueble',
        style: styleTituloItalicW600.copyWith(fontSize: 15, color: claro),
      ),
    );
  }
}
