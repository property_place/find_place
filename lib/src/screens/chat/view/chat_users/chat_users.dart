import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../core/global/environment.dart';
import '../../../../service/logic/socket_provider.dart';
import '../../../widgets/avater_image_circle.dart';
import '../../../widgets/center_indicator.dart';
import '../../data/models/chat_users/chat_users_model.dart';
import '../../data/models/chat_users_imm/chat_users_imm_model.dart';
import '../../hooks/chat_users_imm_scroll_controller.dart';
import '../../logic/chat_provider.dart';
import '../../logic/chat_users/chat_users_provider.dart';
import '../Widgets/empty_load.dart';
import '../Widgets/error_load.dart';
import '../chat_screen.dart';

class ChatUsersView extends ConsumerStatefulWidget {
  const ChatUsersView({
    Key? key,
  }) : super(key: key);

_ChatUsersViewState createState() => _ChatUsersViewState();
}

class _ChatUsersViewState extends ConsumerState<ChatUsersView> {
  late final SocketService _socketService;
  late double? yOffset;

  @override
  void initState() {
    /**
     * Inicializamos las variables
    */
    _socketService = ref.read(resolverSocketService.notifier);
    /**
     * Inicializamos los inmuebles
    */
    // ref.read(resolverChatUsersImm.notifier).initChatMyImm();

    /**
     * Escucho los mensajes de los usuarios chat
    */
    _socketService.socket?.on(EnvironmentApi.newOrUpdateChatUser,
     ref.read(resolverChatUsers.notifier).updateUserLastMessage);
  
  
  

    super.initState();
  }
 


  @override
  Widget build(BuildContext context) {
  return _ListChatImmovable();
 }
}

class _ListChatImmovable extends HookConsumerWidget {
  _ListChatImmovable({
    Key? key,
  }) : super(key: key);



  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _immovableChats = ref.watch(rChatUsers);
    final _isLoading      = ref.watch(resolverChatUsers).isLoading;
    /**
     * Hook que controla la paginacion de la lista de usuarios
    */
    final _controller     = useChtUsersImmScrollController(ref,
                            oldLength:_immovableChats != null 
                            ? _immovableChats.length : 0);
    final isLoadMoreError = ref.watch(resolverChatUsers).isLoadMoreError;
    final isLoadMoreDone  = ref.watch(resolverChatUsers).isLoadMoreDone;

    if (_immovableChats == null || _immovableChats.isEmpty) {
      // error case
      if (_isLoading == false && isLoadMoreError.isNotEmpty) {
        return ErrorChtListLoad(
          isLoadMoreError,
          onPressed: () =>
              ref.read(resolverChatUsers.notifier).refresh(),
        );
      }
      if (_isLoading == false && !isLoadMoreError.isNotEmpty) {
        return EmptyChtListLoad('No tienes chats por ahora!');
      }

      return const CenterIndicator();
    }
    return NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowIndicator();
          return true;
        },
        child: ListView.separated(
          physics: BouncingScrollPhysics(),
          itemCount: _immovableChats.length + 1,
          controller: _controller,
          itemBuilder: (_, index) {
            if (index == _immovableChats.length) {
              // load more and get error
              if (isLoadMoreError.isNotEmpty) {
                return ErrorChtListLoad(
                  isLoadMoreError,
                  onPressed: () =>
                      ref.read(resolverChatUsers.notifier).refresh(),
                );
              }
              // load more but reached to the last element
              if (isLoadMoreDone) {
                return const Center(
                  child: Text(
                    'Done!',
                    style: TextStyle(color: Colors.green, fontSize: 20),
                  ),
                );
              }
              return _isLoading
                  ? Container(
                      height: _immovableChats.isEmpty
                          ? MediaQuery.of(context).size.height * 0.7
                          : 50,
                      alignment: Alignment.center,
                      child: const CenterIndicator())
                  : Container();
            }
            return _UserListTitle(user: _immovableChats[index]);
          },
          separatorBuilder: (_, i) => Divider(),
        ),
    );
  }
}

class _UserListTitle extends ConsumerWidget {
  const _UserListTitle({
    Key? key,
    required this.user,
  }) : super(key: key);

  final DocChatUsersImmModel user;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ListTile(
      onTap: () {
        ref.read(rSelectedUser.state).state = DocChatUsersModel(
            image: user.image,
            userId: user.userId,
            email: user.email,
            name: user.name,
            pLastName: user.pLastName,
            online: user.online,
            lastMessage: user.lastMessage,
            immovableId: user.immovableId,
            chatId: user.chatId,
            time: user.time,
            rol: user.rol
           );

        const transitionDuration = Duration(milliseconds: 200);
        Navigator.of(context).push(
          PageRouteBuilder(
            transitionDuration: transitionDuration,
            reverseTransitionDuration: transitionDuration,
            pageBuilder: (_, animation, __) {
              return FadeTransition(
                opacity: animation,
                child: ChatScreen(),
              );
            },
          ),
        );
      },
      title: Text('${user.immovable} (${user.name})'),
        subtitle: Text(user.lastMessage != null 
                     ? user.lastMessage != 'null' 
                       ? user.lastMessage!.length > 20 
                          ? '${user.lastMessage}...' 
                          : '${user.lastMessage}'
                       : ''
                     : '' ),
      leading: Container(
        width: 50,
        height: 50,
        child: AvaterCircleImage(
          imageUrl: user.outsideFrontImage,
          childAlternative: CircleAvatar(
            backgroundColor: oscuro.withOpacity(0.6),
            child: Text(
              user.immovable.substring(0, 2).toUpperCase(),
              style: styleTituloItalicW600.copyWith(color: claro, fontSize: 20),
            ),
          ),
        ),
      ),
      trailing: Container(
        width: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 30,
              height: 30,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: grey, borderRadius: BorderRadius.circular(100)),
              child: Text(NumberFormat.compact().format(9),
                  style: styleTituloItalicW600.copyWith(color: claro)),
            ),
            Container(
              width: 10,
              height: 10,
              decoration: BoxDecoration(
                  color: user.online ? chatPersonActive : chatPersonInactive,
                  borderRadius: BorderRadius.circular(100)),
            ),
          ],
        ),
      ),
    );
  }
}
