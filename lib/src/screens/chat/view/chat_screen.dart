import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uuid/uuid.dart';

import '../../../core/framework/colors.dart';
import '../../../core/global/environment.dart';
import '../../../service/logic/socket_provider.dart';
import '../../auth/data/models/user_model.dart';
import '../../auth/logic/auth_provider.dart';
import '../../home/utils/type_rol.dart';
import '../../widgets/center_indicator.dart';
import '../data/models/chat_immovables/chat_immovables_model.dart';
import '../data/models/chat_messages_model.dart';
import '../data/models/chat_users/chat_users_model.dart';
import '../hooks/animation_chat.dart';
import '../hooks/chat_my_users_imm_scroll_controller.dart';
import '../logic/chat_provider.dart';
import '../logic/chat_users/chat_users_provider.dart';
import '../logic/chat_users_my_immovables/users_my_imm_provider.dart';
import '../logic/keyboard_emoji/keyboard_emoji_provider.dart';
import '../utils/time_format.dart';
import 'Widgets/boockeing_annimation.dart';
import 'Widgets/chat_appbar.dart';
import 'Widgets/chat_mensaje.dart';
import 'Widgets/custom_animate_opacity.dart';
import 'Widgets/emoji_picker_keyboard.dart';
import 'Widgets/error_load.dart';
import 'Widgets/input_chat.dart';
import 'Widgets/wtriting_chat_user.dart';

class ChatScreen extends StatefulHookConsumerWidget {
  const ChatScreen({Key? key}) : super(key: key);
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends ConsumerState<ChatScreen>
    with TickerProviderStateMixin {
  late final AnimationController _buttonController;
  late final AnimationController _contentController;

  late final DocChatUsersModel? _selectUser;

  @override
  void initState() {
    /**
     * Inicializamos las diferentes variables 
     * @_sectedUser => obtiene la informacion del usuario
     * que esta selecionado para el chat
    */
    _selectUser = ref.read(rSelectedUser);


     ref.read(resolverChatService.notifier).initChatsMessages();

    /**
      * Inicializamos los controladores para la animaciones 
      * al entrar al chat
      */
    _buttonController = AnimationController(
      duration: const Duration(milliseconds: 750),
      vsync: this,
    );

    _contentController = AnimationController(
      duration: const Duration(milliseconds: 750),
      vsync: this,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _controller = useChatAnimationController(
      contentController: _contentController,
      buttonController: _buttonController,
    );
    final _rIsWriting = ref.watch(rIsWritingReceiver);

    if (_selectUser == null ) {
      return Scaffold(
          body: ErrorChtListLoad(
        'Error',
        btnTitle: 'Regresar',
        onPressed: () => Navigator.of(context),
      ));
    }

    return LayoutBuilder(builder: (context, constraints) {
      final w = constraints.maxWidth;
      final h = constraints.maxHeight;

      return Scaffold(
        backgroundColor: primary,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(_rIsWriting ? 80 : 60),
          child: CustomAnimatedOpacity(
              animation: _controller.topOpacityAnimation,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    AppBarChatInitDetail(_selectUser!),
                    WritingInChatUser(controller: _controller),
                  ],
                ),
              )),
        ),
        body: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            Consumer(
              builder: (context, ref, child) {
                return SafeArea(
                  child: CustomAnimatedOpacity(
                    animation: _controller.topOpacityAnimation,
                    child: Chat(
                      w: w,
                      h: h,
                      selectedUser: _selectUser,
                    ),
                  ),
                );
              },
            ),
            _AnimationWelcome(controller: _controller, w: w, h: h),
          ],
        ),
      );
    });
  }

}

class _AnimationWelcome extends StatelessWidget {
  const _AnimationWelcome({
    Key? key,
    required BookingPageAnimationController controller,
    required this.w,
    required this.h,
  })  : _controller = controller,
        super(key: key);

  final BookingPageAnimationController _controller;
  final double w;
  final double h;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: AnimatedBuilder(
        animation: _controller.buttonController,
        builder: (_, child) {
          final sizeC = _controller
              .buttonSizeAnimation(
                Size(w * .0, h * .06),
                Size(w * 1.5, h * 1.1),
              )
              .value;
          final margin = _controller.buttonMarginAnimation(h * .03).value;
          return Container(
            width: sizeC.width,
            height: sizeC.height,
            margin: EdgeInsets.only(bottom: margin),
            alignment: Alignment.center,
            decoration: const BoxDecoration(
              color: oscuro,
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
          );
        },
      ),
    );
  }
}

class Chat extends ConsumerStatefulWidget {
  Chat(
      {Key? key,
      required this.w,
      required this.h,
      DocChatUsersModel? selectedUser,
       DocChatImmovablesModel? selectedImmovable,
    })
      : _selectedUser = selectedUser,
       _selectedImmovable = selectedImmovable,
        super(key: key);

  final double w;
  final double h;

  final DocChatUsersModel? _selectedUser;
  final DocChatImmovablesModel? _selectedImmovable;


  ChatState createState() => ChatState();
}

class ChatState extends ConsumerState<Chat> with TickerProviderStateMixin {
  final _textController = TextEditingController();
  final _focusNode = FocusNode();

  late final SocketService _socketService;
  late final DocUserModel? _authService;

  @override
  void initState() {
    /**
     * Inicializamos las diferentes variables 
     * @_socketService => servicio de socket, permite enviar y escuchar chat
     * @_authService => obtiene la informacion del usuario conectado
     */
    _socketService = ref.read(resolverSocketService.notifier);
    _authService = ref.read(userProvider);
    /**
     * Escucho los mensajes de los usuarios chat
     */
    _socketService.socket
        ?.on(EnvironmentApi.onMessageReceibed, _onChangeMessage);

    /**
     * Escucho los diferentes eventos del teclado, se utiliza,
     * para ocultar el teclado si la opcion emoji esta activa
     */
    ref.read(resolverKeyboardEmojiService);

    super.initState();
  }

  /*
   * Metodo que esucha los diferentes mensajes que llegan al usuario
  */
  void _onChangeMessage(dynamic data) {
    final date =  DateTime.parse(data['time']);
    final time = convertChatTimeFormat(date);
 
    final newMessage = DocChatMessageRespModel(
      senderID: data['senderID'],
      chatId: data['chatID'],
      message: data['message'],
      time: time,
      timeMsg: '${date.millisecondsSinceEpoch}',
      receiverID: widget._selectedUser!.userId,
      immovableID: widget._selectedUser!.immovableId,
      animation: AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 300),
      ),
    );
    
     ref.read(listChatProvider.notifier).state = 
     [newMessage, ...ref.read(listChatProvider.notifier).state!];

    newMessage.animation!.forward();
  }


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final isEmojiVisibility = ref.watch(resolverKeyboardEmojiService).isEmojiVisibility;
    final isKeyboardVisible = ref.watch(resolverKeyboardEmojiService).isKeyboardVisible;


    if (_authService == null) {
      return Scaffold(
          body: ErrorChtListLoad(
        'Error',
        onPressed: () {},
      ));
    }

    return WillPopScope(
      onWillPop: onBackPress,
      child: Container(
        width: size.width,
        height: size.height,
        child: Column(
          children: [
            Expanded(
              child: GestureDetector(
                onTap: () => FocusScope.of(context).unfocus(),
                child:  _ListChatDetaild()
              ),
            ),
            Consumer(
              builder: (context, ref, child) {
                final _isWriting = ref.watch(rIsWriting);
                return Container(
                  height: 55,
                  margin: const EdgeInsets.only(bottom: 10),
                  child: InputChat(
                      w: widget.w,
                      textCRT: _textController,
                      focusNode: _focusNode,
                      isEmojiVisible: isEmojiVisibility,
                      isKeyboardVisible: isKeyboardVisible,
                      sendMessage: _isWriting
                          ? () =>
                              _hanledSubmit(_textController.text.trim(), ref: ref)
                          : null,
                      onChanged: onChangeMesssage,
                      prefixIconOnPressed: _handleEmoji),
                );
              },
            ),
            Offstage(
              child: EmojiPickerWidget(onEmojiSelected: _onEmojiSelected,
               onBackspacePressed: _onBackspacePressed),
              offstage: !isEmojiVisibility,
            ),
          ],
        ),
      ),
    );
  }

  void _hanledSubmit(String message, {required WidgetRef ref}) async {
    if (message.isEmpty && _authService == null) return;

    /**
     * Limpiamos el campo de texto del chat
     */
    _textController.clear();


    /**
     * Uuid para generar id
     */
    var uuid = Uuid();
    var uid = uuid.v4();

    /**
     * Obtenemos la hora que se envia y le damos forma, 
     * para obtener solo el tiempo , pero a la BD se envia
     * la fecha y hora completa
     */
    final now = DateTime.now();
    final time = convertChatTimeFormat(now);

    /**
     * Obtenemos la informacion y la agregamos a la lista de chat
     */
    final newMessage = DocChatMessageRespModel(
      senderID: _authService!.userId,
      chatId: uid,
      message: message,
      time: time,
      timeMsg: '${now.millisecondsSinceEpoch}',
      receiverID: widget._selectedUser!.userId,
      immovableID: widget._selectedUser!.immovableId,
      animation: AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 300),
      ),
    );

     ref.read(listChatProvider.notifier).state = 
     [newMessage, ...ref.read(listChatProvider.notifier).state!];

    /**
     * Uuid para generar id
     * Socket, enviamos el mensaje al servidor para ser 
     * agregado a la lista de mensajes del chat
     */
    final data = {
      "chatID": uid,
      "receiverID": widget._selectedUser!.userId,
      "senderID": _authService!.userId,
      "message": message,
      "time": DateTime.now().toString(),
      "immovableID": widget._selectedUser!.immovableId,
    };
    _socketService.emit(EnvironmentApi.onMessageReceibed, data);
    ref.read(rIsWriting.notifier).state = false;  

    /**
     * Comenzamos la animacion de los mensajes
    */
    newMessage.animation?.forward();

    /**
     * Actualizamos el chat, donde se esta realizando la conversacion, 
     * con el ultimo mensaje en la lista de chats
    */
    if(_authService!.rol.toLowerCase() ==  TypeRole.blaze.name){
      ref.read(resolverChatUsersMyImm.notifier).updateUserLastMessage({
        'user_id': widget._selectedUser!.userId,
        'last_message': message
      }) ;
    }else{
      ref.read(resolverChatUsers.notifier).updateUserLastMessage({
          'user_id': widget._selectedUser!.userId,
          'last_message': message
      });
    } 


  }

  void onChangeMesssage(String value){
    if (value.trim().isNotEmpty) {
       if(!ref.read(rIsWriting.notifier).state && value.length > 1){
         _socketService.socket?.emit(EnvironmentApi.isWriting, {
            "writing": true, 
            "chatID": widget._selectedUser!.chatId,
            "receiverID": widget._selectedUser!.userId, 
         });
         ref.read(rIsWriting.notifier).state = true;
       }
     } else {
       if(ref.read(rIsWriting.notifier).state){
         _socketService.socket?.emit(EnvironmentApi.isWriting, {
            "writing": false, 
            "chatID": widget._selectedUser!.chatId,
            "receiverID": widget._selectedUser!.userId, 
         });                          
         ref.read(rIsWriting.notifier).state = false;
       }
     }
  }

  /*
   * Metodo concatena los mensajes mas los emoji 
  */

  void _onEmojiSelected(String emoji) {
  
      if (_textController.text.trim().isNotEmpty || emoji.isNotEmpty) {
         if(!ref.read(rIsWriting.notifier).state){
           _socketService.socket?.emit(EnvironmentApi.isWriting, {
              "writing": true, 
              "chatID": widget._selectedUser!.chatId,
              "receiverID": widget._selectedUser!.userId, 
           });
           ref.read(rIsWriting.notifier).state = true;
         }
       } else {
         if(ref.read(rIsWriting.notifier).state){
           _socketService.socket?.emit(EnvironmentApi.isWriting, {
              "writing": false, 
              "chatID": widget._selectedUser!.chatId,
              "receiverID": widget._selectedUser!.userId, 
           });                          
           ref.read(rIsWriting.notifier).state = false;
         }
       }

     _textController.text = _textController.text + emoji;

  }

  void _onBackspacePressed() {
    _textController
      ..text = _textController.text.characters.skipLast(1).toString()
      ..selection = TextSelection.fromPosition(
      TextPosition(offset: _textController.text.length));
  }

  

  /*
   * Metodo que inicia los emoji
  */

  void _handleEmoji() async {
    if (ref.read(resolverKeyboardEmojiService.notifier).isEmojiVisibility) {
      _focusNode.requestFocus();
    } else if (ref.read(resolverKeyboardEmojiService.notifier).isEmojiVisibility) {
      await SystemChannels.textInput.invokeMethod('TextInput.hide');
      await Future.delayed(Duration(milliseconds: 100));
      
    }
    toggleEmojiKeyboard();
  }

  /*
   * Metodo que inicia los emoji junto con el metodo 
   * _handleEmoji
  */

  Future toggleEmojiKeyboard() async {
    if (ref.read(resolverKeyboardEmojiService.notifier).isKeyboardVisible) {
      FocusScope.of(context).unfocus();
    }
   ref.read(resolverKeyboardEmojiService.notifier).isEmojiVisibility =
   !ref.read(resolverKeyboardEmojiService.notifier).isEmojiVisibility;
  }

  /*
   * Metodo que actua a los diferentes cambios, 
   * entre el emoji y el teclado
  */

  Future<bool> onBackPress() {
    if (ref.read(resolverKeyboardEmojiService.notifier).isEmojiVisibility) {
      toggleEmojiKeyboard();
    } else {
      Navigator.pop(context);
    }

    return Future.value(false);
  }

  @override
  void dispose() {
    _socketService.socket?.off(EnvironmentApi.onMessageReceibed);
    super.dispose();
  }


}

class _ListChatDetaild extends HookConsumerWidget {
  const _ListChatDetaild({
    Key? key,
  }) :
        super(key: key);


  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _message = ref.watch(listChatProvider);
    final _isLoading        = ref.watch(resolverChatService).isLoading;
    /**
     * Hook que controla la paginacion de los mensajes del chat
    */
    final _controller       = useChtUsersMyImmScrollController(ref, 
                              oldLength: _message != null 
                              ? _message.length : 0);
    final _isLoadMoreError  = ref.watch(resolverChatService).isLoadMoreError;
    final _isLoadMoreDone    = ref.watch(resolverChatService).isLoadMoreDone;

    /**
     * datos de inicio o error
     */
    if (_message == null) {
      // error case
      if (_isLoading == false) {
        return Center(
          child: Text('error'),
        );
      }
      return const CenterIndicator();
    }
    return NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowIndicator();
            return true;
          },
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        reverse: true,
        padding: const EdgeInsets.symmetric(vertical: 20),
        itemCount: _message.length,
        controller: _controller,
        itemBuilder: (_, index) {
          /**
           * último elemento (barra de progreso, error o '¡Hecho!' 
           * si llegó al último elemento)
          */
          if (index == _message.length) {
            /**
               * cargar más y obtener error
               */
            if (_isLoadMoreError != '') {
              return Center(
                child: Text('Error'),
              );
            }
            /**
               * carga más pero llega al último elemento
               */
            if (_isLoadMoreDone) {
              return Center(
                child: Text(
                  'Done!',
                  style: TextStyle(color: Colors.green, fontSize: 20),
                ),
              );
            }
            return CenterIndicator();
          }
    
          return ChatMessages(_message[index]);
        },
      ),
    );
  }
}


class ChatMessages extends StatefulWidget {
  const ChatMessages(this.newMessage, {Key? key}) : super(key: key);
  final DocChatMessageRespModel newMessage;

  @override
  State<ChatMessages> createState() => _ChatMessagesState();
}

class _ChatMessagesState extends State<ChatMessages> {

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, child) {
        return ChatMessage(
          chatId: widget.newMessage.chatId,
          texto: widget.newMessage.message,
          uid: widget.newMessage.senderID,
          time: widget.newMessage.time,
          animationController: widget.newMessage.animation,
        );
      },
    
    );
  }

  @override
  void dispose() {
    widget.newMessage.animation?.dispose();  
    super.dispose();
  }
}