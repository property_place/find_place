import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';

class EmptyChtListLoad extends StatelessWidget {
  const EmptyChtListLoad(this.title, {Key? key}) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FadeIn(
      child: Stack(
        children: [
          Container(
              width: size.width,
              height: size.height * 0.7,
              alignment: Alignment.center,
              child: SvgPicture.asset(
                'assets/empty.svg',
                height: size.height * .2,
              )),
          Positioned(
            left: 0,
            right: 0,
            child: Container(
              color: claro,
              width: size.width,
              height: 40,
              alignment: Alignment.center,
              child: Text(
                title,
                style: styleTituloItalicW600.copyWith(
                    fontSize: 15, fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
