import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../core/framework/colors.dart';
import '../../logic/chat_provider.dart';

class OnLineUserChat extends ConsumerWidget {
  const OnLineUserChat({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _selectUser = ref.read(rSelectedUser);
    return Container(
      width: 10,
      height: 10,
      decoration: BoxDecoration(
          color: _selectUser != null
              ? _selectUser.online
                  ? chatPersonActive
                  : chatPersonInactive
              : chatPersonInactive,
          borderRadius: BorderRadius.circular(100)),
    );
  }
}
