import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../widgets/custom_imput_chat.dart';

class InputChat extends StatelessWidget {
  InputChat(
      {Key? key,
      required this.w,
      required Function()? sendMessage,
      required TextEditingController textCRT,
      required FocusNode focusNode,
      required dynamic Function(String)? onChanged,
      this.prefixIconOnPressed,
      required this.isEmojiVisible,
      required this.isKeyboardVisible})
      : _sendMessage = sendMessage,
        _textCRT = textCRT,
        _focusNode = focusNode,
        _onChanged = onChanged,
        super(key: key);
  final double w;
  final void Function()? _sendMessage;
  final TextEditingController _textCRT;
  final FocusNode _focusNode;
  final dynamic Function(String)? _onChanged;
  final void Function()? prefixIconOnPressed;
  final bool isEmojiVisible;
  final bool isKeyboardVisible;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: w,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Flexible(
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(
                  color: oscuro.withOpacity(0.3),
                ),
              ),
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: Consumer(builder: (context, ref, child) {
                  return LayoutBuilder(builder: (context, size) {
                    var text = TextSpan(
                      // text: _textCRT.text,
                      // style: yourTextStyle,
                      children: [
                        TextSpan(text: _textCRT.text, style: styleTituloItalicW600.copyWith(fontSize: 24)),
                      ]
                    );

                    var tp = TextPainter(
                      text: text,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.left,
                    );
                    tp.layout(maxWidth: size.maxWidth);

                    var lines =
                        (tp.size.height / tp.preferredLineHeight).ceil();
                    var maxLines = 10;
                    return TextFieldChat(
                      controller: _textCRT,
                      name: 'Mensaje',
                      type: TextInputType.text,
                      maxLines: lines < maxLines ? null : maxLines,
                      focusNode: _focusNode,
                      onSubmitted: (text) => _sendMessage!(),
                      onChanged: _onChanged,
                      prefixIcon: Padding(
                        padding: EdgeInsetsDirectional.only(bottom: 0),
                        child: IconButton(
                          onPressed: prefixIconOnPressed,
                          icon: Icon(
                            isEmojiVisible
                                ? Icons.keyboard_rounded
                                : Icons.emoji_emotions_outlined,
                            size: 30,
                            color: oscuro,
                          ),
                        ),
                      ),
                    );
                  });
                }),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 5, bottom: 5, right: 5),
            child: Platform.isIOS
                ? CupertinoButton(
                    child: Text(
                      'Enviar',
                      style: styleTituloNormal.copyWith(color: secundary),
                    ),
                    onPressed: _sendMessage)
                : IconTheme(
                    data: IconThemeData(color: secundary),
                    child: IconButton(
                      onPressed: _sendMessage,
                      splashColor: colorDisabled,
                      highlightColor: colorDisabled,
                      icon: Icon(Ionicons.send_outline),
                    ),
                  ),
          )
        ],
      ),
    );
  }
}
