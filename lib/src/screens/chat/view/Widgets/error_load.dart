import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../core/framework/tipografia.dart';
import '../../../home/view/widgets/button_ retry.dart';

class ErrorChtListLoad extends ConsumerWidget {
  const ErrorChtListLoad(this._error,
      {Key? key, required this.onPressed, this.btnTitle = 'Reintentar'})
      : super(key: key);
  final String _error;
  final void Function() onPressed;
  final String btnTitle;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      width: size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText(_error,
              style: styleTituloItalicW600.copyWith(
                  fontSize: 15, fontWeight: FontWeight.w700),
              maxLines: 2,
              stepGranularity: 3),
          SizedBox(height: 20.0),
          Container(
            width: size.width * 0.5,
            height: 50,
            child: ButtonRetryLoad(btnTitle: btnTitle, onPressed: onPressed),
          )
        ],
      ),
    );
  }
}
