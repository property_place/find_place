import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../core/global/environment.dart';
import '../../../../service/logic/socket_provider.dart';
import '../../logic/chat_provider.dart';
import 'boockeing_annimation.dart';
import 'custom_animate_opacity.dart';

class WritingInChatUser extends ConsumerStatefulWidget {
  const WritingInChatUser(
      {Key? key, required BookingPageAnimationController controller})
      : _controller = controller,
        super(key: key);
  final BookingPageAnimationController _controller;

  _WritingInChatUserState createState() => _WritingInChatUserState();
}

class _WritingInChatUserState extends ConsumerState<WritingInChatUser>
    with TickerProviderStateMixin {
  late final SocketService _socketService;

  @override
  void initState() {
    /**
      * Inicializamos las diferentes variables 
      * @_socketService => servicio de socket, permite enviar y escuchar chat
    */
    _socketService = ref.read(resolverSocketService.notifier);

    /**
     * Escucho los mensajes de los usuarios chat
    */
    _socketService.socket?.on(EnvironmentApi.isWriting, _onChangeWriting);

    super.initState();
  }

  void _onChangeWriting(dynamic data) {
    ref.read(rIsWritingReceiver.notifier).state = data['writing'];
  }

  @override
  Widget build(BuildContext context) {
    final _rIsWriting = ref.watch(rIsWritingReceiver);
    return CustomAnimatedOpacity(
      animation: widget._controller.topOpacityAnimation,
      child: AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeIn,
          alignment: Alignment.center,
          width: MediaQuery.of(context).size.width,
          height: _rIsWriting ? 20 : 0,
          color: primary,
          child: Text(
            'Escribiendo...',
            style: styleTituloItalicW600.copyWith(fontSize: 12),
          )),
    );
  }

  @override
  void dispose() {
    _socketService.socket?.off(EnvironmentApi.isWriting);
    super.dispose();
  }
}
