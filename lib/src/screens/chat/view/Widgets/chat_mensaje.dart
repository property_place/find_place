import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../auth/logic/auth_provider.dart';

class ChatMessage extends ConsumerWidget {
  const ChatMessage(
      {Key? key,
      required this.chatId,
      required this.texto,
      required this.uid,
      required this.time,
      this.animationController})
      : super(key: key);
  final String texto;
  final String uid;
  final String chatId;
  final String time;
  final AnimationController? animationController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _authService = ref.watch(userProvider);

    //TODO: VALIDAR EL ERROR => _infoCreatorOfTheProperty
    if (_authService == null) return Container();

    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onLongPress: () {
        print(chatId);
      },
      child: animationController != null
          ? FadeTransition(
              opacity: animationController!,
              child: SizeTransition(
                sizeFactor: CurvedAnimation(
                  parent: animationController!,
                  curve: Curves.easeInOut,
                ),
                child: Container(
                  child: uid == _authService.userId
                      ? _MyMessage(
                          chatId: chatId,
                          texto: texto,
                          time: time,
                        )
                      : _OnMyMessage(
                          texto: texto,
                          time: time,
                        ),
                ),
              ),
            )
          : Container(
              child: uid == _authService.userId
                  ? Container(
                      child: _MyMessage(
                        chatId: chatId,
                        texto: texto,
                        time: time,
                      ),
                    )
                  : _OnMyMessage(
                      texto: texto,
                      time: time,
                    ),
            ),
    );
  }
}

class _OnMyMessage extends StatelessWidget {
  const _OnMyMessage({
    Key? key,
    required this.texto,
    required this.time,
  }) : super(key: key);

  final String texto;
  final String time;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        padding: const EdgeInsets.all(4.0),
        margin: const EdgeInsets.only(
          bottom: 5.0,
          right: 50.0,
          left: 5.0,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Container(
                margin: const EdgeInsets.only(left: 10, right: 5),
                child: Text(
                  texto,
                  style: styleTituloItalicW600.copyWith(
                      color: claro.withOpacity(0.87), fontSize: 12),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                padding: const EdgeInsets.only(top: 15, left: 10, right: 10),
                child: Text(
                  time,
                  textAlign: TextAlign.right,
                  style:
                      styleTituloItalicW600.copyWith(color: claro, fontSize: 8),
                ),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          color: oscuro.withOpacity(0.7),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)),
        ),
      ),
    );
  }
}

class _MyMessage extends StatelessWidget {
  const _MyMessage(
      {Key? key, required this.texto, required this.time, required this.chatId})
      : super(key: key);

  final String texto;
  final String time;
  final String chatId;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        padding: const EdgeInsets.all(4.0),
        margin: const EdgeInsets.only(
          bottom: 5.0,
          left: 50.0,
          right: 5.0,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Container(
                margin: const EdgeInsets.only(left: 10, right: 5),
                child: Text(
                  texto,
                  style: styleTituloItalicW600.copyWith(
                      color: claro.withOpacity(0.87), fontSize: 12),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                padding: const EdgeInsets.only(top: 15, left: 10, right: 10),
                child: Text(
                  time,
                  textAlign: TextAlign.right,
                  style:
                      styleTituloItalicW600.copyWith(color: claro, fontSize: 8),
                ),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          color: secundary.withOpacity(0.7),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)),
        ),
      ),
    );
  }
}

