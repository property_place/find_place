import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../routes/app_routes.dart';
import '../../../auth/logic/auth_provider.dart';
import '../../../home/utils/type_rol.dart';
import '../../../widgets/avater_image_circle.dart';
import '../../data/models/chat_users/chat_users_model.dart';
import '../../data/models/menu_item_chat/menu_item_chat.dart';
import '../../data/repositories/data_menu_items_chat.dart';
import '../../logic/chat_provider.dart';
import 'online_chat_user.dart';

class AppBarChatInitDetail extends ConsumerWidget {
  const AppBarChatInitDetail(
    this._receiverUser, {
    Key? key,
  }) : super(key: key);
  final DocChatUsersModel _receiverUser;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        AppBar(
          backgroundColor: oscuro,
          title: Column(
            children: [
              Container(
                width: 30,
                height: 30,
                child: _receiverUser.image == null
                    ? CircleAvatar(
                        backgroundColor: secundary.withOpacity(0.8),
                        child: Text(
                          _receiverUser.name.substring(0, 2),
                          style: styleTituloItalicW600.copyWith(
                            color: claro,
                          ),
                        ),
                      )
                    : AvaterCircleImage(
                        imageUrl: _receiverUser.image!,
                        childAlternative: CircleAvatar(
                          backgroundColor: secundary.withOpacity(0.8),
                          child: Text(
                            _receiverUser.name.substring(0, 2),
                            style: styleTituloItalicW600.copyWith(
                              color: claro,
                            ),
                          ),
                        ),
                      ),
              ),
              SizedBox(height: 3),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "${_receiverUser.name} ${_receiverUser.pLastName} ${_receiverUser.sLastName ?? ''} ",
                    style: styleTituloNormal.copyWith(fontSize: 12),
                  ),
                  OnLineUserChat()
                ],
              ),
            ],
          ),
          centerTitle: true,
          elevation: 1,
          actions: [
            Consumer(
              builder: (context, ref, child) => PopupMenuButton<MenuItemChat>(
                  onSelected: (item) => onSelectedPop(context, item, ref),
                  icon: Icon(Ionicons.ellipsis_vertical),
                  itemBuilder: (context) => [
                        if (ref
                                .read(userProvider.state)
                                .state!
                                .rol
                                .toLowerCase() ==
                            TypeRole.blaze.name)
                          ...DataMenuItems.itemsMenuRolBlazeFirt
                              .map(buildItemMenuActive)
                              .toList(),
                        if (ref
                                .read(userProvider.state)
                                .state!
                                .rol
                                .toLowerCase() ==
                            TypeRole.blaze.name)
                          PopupMenuDivider(),
                        if (ref
                                .read(userProvider.state)
                                .state!
                                .rol
                                .toLowerCase() ==
                            TypeRole.blaze.name)
                          ...DataMenuItems.itemsMenuRolBlazeSecond
                              .map(buildItemMenu)
                              .toList(),
                        if (ref
                                .read(userProvider.state)
                                .state!
                                .rol
                                .toLowerCase() ==
                            TypeRole.spark.name)
                          ...DataMenuItems.itemsMenuRolSpark
                              .map(buildItemMenu)
                              .toList()
                      ]),
            ),
          ],
        ),
        Consumer(
          builder: (context, ref, child) {
            final _isLoading = ref.watch(resolverChatService).isLoading;
            return _isLoading
                ? LinearProgressIndicator(
                    color: coloPpaginationChat,
                    backgroundColor: oscuro,
                    minHeight: 1.5,
                  )
                : Container();
          },
        )
      ],
    );
  }

  PopupMenuItem<MenuItemChat> buildItemMenuActive(MenuItemChat item) =>
      PopupMenuItem(
        value: item,
        child: Row(
          children: [
            Icon(item.icon, color: oscuro, size: 20),
            SizedBox(width: 20),
            Expanded(
              child: Text(
                item.text,
                style: styleTituloItalicW600,
              ),
            ),
            Consumer(
              builder: (context, ref, child) {
                final isCheckedContract = ref.watch(rIsCheckedEnabledContract);
                return Checkbox(
                    value: isCheckedContract,
                    checkColor: oscuro,
                    activeColor: secundary_orange,
                    onChanged: (val) => ref
                        .read(rIsCheckedEnabledContract.notifier)
                        .state = val!);
              },
            ),
          ],
        ),
      );
  PopupMenuItem<MenuItemChat> buildItemMenu(MenuItemChat item) => PopupMenuItem(
        value: item,
        child: Row(
          children: [
            Icon(item.icon, color: oscuro, size: 20),
            SizedBox(width: 20),
            Text(
              item.text,
              style: styleTituloItalicW600,
            )
          ],
        ),
      );

  void onSelectedPop(BuildContext context, MenuItemChat item, WidgetRef ref) {
    switch (item) {
      case DataMenuItems.itemsSetting:
        Navigator.pushNamed(context, Routes.chat_settings_message);
        break;
      case DataMenuItems.itemEnableContract:
        ref.read(rIsCheckedEnabledContract.notifier).state =
            !ref.read(rIsCheckedEnabledContract.state).state;
        break;
      case DataMenuItems.itemContract:
        Navigator.pushNamed(context, Routes.chat_contract_message);
        break;
    }
  }
}
