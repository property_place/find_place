import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../core/global/environment.dart';
import '../../../../service/logic/socket_provider.dart';
import '../../../home/view/widgets/button_ retry.dart';
import '../../../widgets/avater_image_circle.dart';
import '../../../widgets/center_indicator.dart';
import '../../data/models/chat_immovables/chat_immovables_model.dart';
import '../../data/models/chat_users/chat_users_model.dart';
import '../../hooks/chat_list_user_scroll_controller.dart';
import '../../logic/chat_provider.dart';
import '../../logic/chat_users_my_immovables/users_my_imm_provider.dart';
import '../Widgets/empty_load.dart';
import '../Widgets/error_load.dart';
import '../chat_screen.dart';

class ChatListUsersScreen extends ConsumerStatefulWidget {
  ChatListUsersScreen({Key? key}) : super(key: key);

  @override
  _ChatListUsersScreenState createState() => _ChatListUsersScreenState();
}

class _ChatListUsersScreenState extends ConsumerState<ChatListUsersScreen> {
  late final DocChatImmovablesModel? _selectedImmovable;
  late final SocketService _socketService;

  @override
  void initState() {
    /**
     * Inicializamos las variables
    */
    _selectedImmovable = ref.read(rSelectedImmovable);
    _socketService = ref.read(resolverSocketService.notifier);

    /**
     * Inicializamos los inmuebles
    */
    ref.read(resolverChatUsersMyImm.notifier).initChatMyImm();

    /**
     * Escucho los mensajes de los usuarios chat
    */
    _socketService.socket?.on(EnvironmentApi.newOrUpdateChatUser,
     ref.read(resolverChatUsersMyImm.notifier).newOrUpdateChatUser);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    if (_selectedImmovable == null) {
      return Scaffold(body: _ErrorChtListImmLoad('Error'));
    }

    return Scaffold(
      backgroundColor: primary,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          backgroundColor: oscuro,
          elevation: 1,
          title: Text(
            _selectedImmovable!.description,
            style: styleTituloNormal.copyWith(color: claro),
          ),
          leading: ElevatedButton(
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.transparent),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ))),
            onPressed: () => Navigator.pop(context),
            child: Icon(Ionicons.chevron_back),
          ),
          actions: [
            Consumer(builder: (context, ref, child) {
              final socketService = ref.watch(resolverSocketService);
              return Container(
                margin: const EdgeInsets.only(right: 10),
                child: socketService.serveStatus == ServeStatusConnection.Online
                    ? Icon(Icons.offline_bolt, color: chatPersonActive)
                    : Icon(Icons.check_circle, color: chatPersonInactive),
              );
            }),
          ],
        ),
      ),
      body: ListViewUsers(),
    );
  }
}

class ListViewUsers extends HookConsumerWidget {
  const ListViewUsers({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //OBTENEMOS LA CATEGORIA ATRAVES DE LA CATEGORIA SELECCIONADA
    final _userList       = ref.watch(chatListUserProvider);
    final _isLoading      = ref.watch(resolverChatUsersMyImm).isLoading;

    final _controller     = useChtListUserScrollController(ref, 
                            oldLength: _userList != null 
                            ? _userList.length : 0);
    final isLoadMoreError = ref.watch(resolverChatUsersMyImm).isLoadMoreError;
    final isLoadMoreDone  = ref.watch(resolverChatUsersMyImm).isLoadMoreDone;

    if (_userList == null) {
      // error case
      if (_isLoading == false && isLoadMoreError.isNotEmpty) {
        return ErrorChtListLoad(
          isLoadMoreError,
          onPressed: () => ref.read(resolverChatUsersMyImm.notifier).refresh(),
        );
      } else if (_isLoading == false && !isLoadMoreError.isNotEmpty) {
        return Container(
          padding: const EdgeInsets.all(10),
          child: EmptyChtListLoad('No tienes chats por ahora!'));
      }

      return const CenterIndicator();
    }
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: ListView.separated(
          physics: BouncingScrollPhysics(),
          controller: _controller,
          itemBuilder: (_, index) {
            if (index == _userList.length) {
              // load more and get error
              if (isLoadMoreError.isNotEmpty) {
                return ErrorChtListLoad(
                  isLoadMoreError,
                  onPressed: () =>
                      ref.read(resolverChatUsersMyImm.notifier).refresh(),
                );
              }
              // load more but reached to the last element
              if (isLoadMoreDone) {
                return const Center(
                  child: Text(
                    'Done!',
                    style: TextStyle(color: Colors.green, fontSize: 20),
                  ),
                );
              }
              return _isLoading
                  ? Container(
                      height: _userList.isEmpty
                          ? MediaQuery.of(context).size.height * 0.7
                          : 50,
                      alignment: Alignment.center,
                      child: const CenterIndicator())
                  : Container();
            }
            return _UserListTitle(user: _userList[index]);
          },
          separatorBuilder: (_, i) => Divider(),
          itemCount: _userList.length + 1),
    );
  }
}

class _UserListTitle extends ConsumerWidget {
  const _UserListTitle({
    Key? key,
    required this.user,
  }) : super(key: key);

  final DocChatUsersModel user;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ListTile(
      onTap: () {

        ref.read(rSelectedUser.notifier).state = user;

        const transitionDuration = Duration(milliseconds: 200);
        Navigator.of(context).push(
          PageRouteBuilder(
            transitionDuration: transitionDuration,
            reverseTransitionDuration: transitionDuration,
            pageBuilder: (_, animation, __) {
              return FadeTransition(
                opacity: animation,
                child: ChatScreen(),
              );
            },
          ),
        );
      },
      title: Text('${user.name} ${user.pLastName}'),
      subtitle: Text(user.lastMessage != null 
                     ? user.lastMessage != 'null' 
                       ? user.lastMessage!.length > 20 
                          ? '${user.lastMessage}...' 
                          : '${user.lastMessage}'
                       : ''
                     : '' ),
      leading: Container(
        width: 50,
        height: 50,
        child: user.image == null
            ? CircleAvatar(
                backgroundColor: oscuro.withOpacity(0.6),
                child: Text(
                  user.name[0].toUpperCase(),
                  style: styleTituloItalicW600.copyWith(
                      color: claro, fontSize: 20),
                ),
              )
            : AvaterCircleImage(
                imageUrl: user.image!,
                childAlternative: CircleAvatar(
                  backgroundColor: oscuro.withOpacity(0.6),
                  child: Text(
                    user.name[0].toUpperCase(),
                    style: styleTituloItalicW600.copyWith(
                        color: claro, fontSize: 20),
                  ),
                ),
              ),
      ),
      trailing: Container(
        width: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 30,
              height: 30,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: grey, borderRadius: BorderRadius.circular(100)),
            child: Text( NumberFormat.compact().format(9), 
            style: styleTituloItalicW600.copyWith(color: claro)),
            ),
            Container(
              width: 10,
              height: 10,
              decoration: BoxDecoration(
                  color: user.online ? chatPersonActive : chatPersonInactive,
                  borderRadius: BorderRadius.circular(100)),
            ),
          ],
        ),
      ),
    );
  }
}

class _ErrorChtListImmLoad extends ConsumerWidget {
  const _ErrorChtListImmLoad(this._error, {Key? key}) : super(key: key);
  final String _error;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText(_error,
              style: styleTituloItalicW600.copyWith(
                  fontSize: 15, fontWeight: FontWeight.w700),
              maxLines: 2,
              stepGranularity: 3),
          SizedBox(height: 20.0),
          Container(
            width: size.width * 0.5,
            height: 50,
            child: ButtonRetryLoad(onPressed: () => Navigator.pop(context)),
          )
        ],
      ),
    );
  }
}
