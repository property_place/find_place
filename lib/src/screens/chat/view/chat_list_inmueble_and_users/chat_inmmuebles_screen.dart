import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../core/framework/colors.dart';
import '../../../../core/framework/tipografia.dart';
import '../../../../core/global/environment.dart';
import '../../../../routes/app_routes.dart';
import '../../../../service/logic/socket_provider.dart';
import '../../../utils/type_state_immovable.dart';
import '../../../widgets/avater_image_circle.dart';
import '../../../widgets/center_indicator.dart';
import '../../../widgets/custom_imput_search.dart';
import '../../data/models/chat_immovables/chat_immovables_model.dart';
import '../../hooks/scroll_controller.dart';

import '../../logic/chat_my_immovables/my_immovables_provider.dart';
import '../../logic/chat_users_my_immovables/users_my_imm_provider.dart';
import '../Widgets/empty_load.dart';
import '../Widgets/error_load.dart';


class ChatMyImmovable extends ConsumerStatefulWidget {
  ChatMyImmovable({
    Key? key,
  }) : super(key: key);

  _ChatMyInmmubleState createState() => _ChatMyInmmubleState();
}

class _ChatMyInmmubleState extends ConsumerState<ChatMyImmovable> {
  late final SocketService _socketService;

  @override
  void initState() {
    /**
     * Inicializamos las diferentes variables 
     * @_socketService => servicio de socket, permite enviar y escuchar chat
     */
    _socketService = ref.read(resolverSocketService.notifier);

    /**
      * Escucho los diferentes cambio de los usuarios que hallan escrito
    */
    _socketService.socket?.on(EnvironmentApi.newChatImmovableUser,
        ref.read(resolverChatImmovables.notifier).newImmovableChat);

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return RefreshIndicator(
       color: oscuro,
       strokeWidth: 2,
       onRefresh: () {
         return ref.read(resolverChatImmovables.notifier).refresh();
       },
      child: Container(
        width: size.width,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Consumer(
          builder: (context, ref, child) => _ListChatImmovable()),
      ),
    );
  }
}

class _ListChatImmovable extends HookConsumerWidget {
  const _ListChatImmovable({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    /**
     * Obtenemos los chat disponible del usuario
    */
    final _immovableChats  = ref.watch(resolverChatListImmService);
    final _isLoading       = ref.watch(resolverChatImmovables).isLoading;
    /**
     * Hook que controla la paginacion de la lista de usuarios
    */
    final _controller       = useChtListImmScrollController(ref, oldLength: _immovableChats.length);
    final _isLoadMoreError  = ref.watch(resolverChatImmovables).isLoadMoreError;
    final _isLoadMoreDone   = ref.watch(resolverChatImmovables).isLoadMoreDone;

    if (_immovableChats.isEmpty) {
      // error case
      if (_isLoadMoreError.isNotEmpty) {
        return ErrorChtListLoad(
          _isLoadMoreError,
          onPressed: () =>
              ref.read(resolverChatImmovables.notifier).refresh(),
        );
      }
      if (!_isLoadMoreError.isNotEmpty) {
        return EmptyChtListLoad('No tienes chats por ahora!');
      }

      return const CenterIndicator();
    }
    return NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowIndicator();
            return true;
          },
          child: ListView.builder(
            physics: ClampingScrollPhysics(),
            itemCount: _immovableChats.length + 1,
            controller: _controller,
            itemBuilder: (_, index) {
              /**
               * último elemento (barra de progreso, error o '¡Hecho!' 
               * si llegó al último elemento)
              */
              if (index == _immovableChats.length) {
                /**
                 * cargar más y obtener error
                */
                if (_isLoadMoreError.isNotEmpty) {
                  return ErrorChtListLoad(
                    _isLoadMoreError,
                    onPressed: () =>
                        ref.read(resolverChatImmovables.notifier).refresh(),
                  );
                }
                /**
                  * carga más pero llega al último elemento
                 */
                if (_isLoadMoreDone) {
                  return const Center(
                    child: Text(
                      'Done!',
                      style: TextStyle(color: Colors.green, fontSize: 20),
                    ),
                  );
                }
                return _isLoading
                    ? Container(
                        height: _immovableChats.isEmpty
                            ? MediaQuery.of(context).size.height * 0.7
                            : 50,
                        alignment: Alignment.center,
                        child: const CenterIndicator())
                    : Container();
              }
              return _ChatListTitllteInmueble(item: _immovableChats[index]);
            },
        ),
    );
  }
}

class _ChatListTitllteInmueble extends ConsumerWidget {
  const _ChatListTitllteInmueble({
    Key? key,
    required this.item,
  }) : super(key: key);

  final DocChatImmovablesModel item;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        ref.read(rSelectedImmovable.notifier).state = item;
        Navigator.pushNamed(context, Routes.chat_list_users);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          width: size.width,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Stack(
            children: [
              Row(
                children: [
                  Container(
                      width: size.height * 0.06,
                      height: size.height * 0.1,
                      child: AvaterCircleImage(
                        imageUrl: item.outsideFrontImage,
                      )),
                  SizedBox(width: 10),
                  Container(
                    width: size.width * 0.6,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _InmuebleStatus(item: item),
                        Text(
                          item.description,
                          style: styleTituloItalicW600,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'Chats  ',
                                style: styleTituloNormal,
                              ),
                              SizedBox(width: 10),
                              Container(
                                child: FittedBox(
                                  fit: BoxFit.fitWidth,
                                  child: Text(
                                    '${item.quantity}',
                                    style: styleTituloW600,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _InmuebleStatus extends StatelessWidget {
  const _InmuebleStatus({
    Key? key,
    required this.item,
  }) : super(key: key);

  final DocChatImmovablesModel item;

  @override
  Widget build(BuildContext context) {
    final color = item.state == TypeStateImmovable.disponible.name
        ? activeState
        : item.state == TypeStateImmovable.alquilado.name
            ? rentedState
            : stopState;
    return Row(
      children: [
        Text(
          'inmueble',
          style: styleTituloItalicW600.copyWith(
              color: oscuro.withOpacity(0.3), fontSize: 13),
        ),
        SizedBox(width: 10),
        Text(
          item.state,
          style: styleTituloNormal.copyWith(color: color, fontSize: 14),
        ),
      ],
    );
  }
}

class ViewChatAppBarDetail extends StatelessWidget {
  const ViewChatAppBarDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primary,
      height: 100,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          BackButton(
            onPressed: () => Navigator.pop(context),
          ),
          Expanded(child: SearchViewCityProduct())
        ],
      ),
    );
  }
}

class SearchViewCityProduct extends StatelessWidget {
  SearchViewCityProduct({
    Key? key,
  }) : super(key: key);

  final emailCRTL = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (ctx, ref, child) {
      return Container(
        height: 100,
        padding: const EdgeInsets.only(left: 20, top: 15),
        child: TextFieldSearch(
            controller: emailCRTL,
            name: 'Buscar',
            prefixIcon: const Icon(Ionicons.search, color: oscuro),
            maxLines: 1,
            type: TextInputType.text,
            padding: 5,
            color: oscuro,
            colorOP: 1,
            radius: 50,
            onChanged: (value) => {}
            // ref.read(citiesNotifierProvider.notifier).loadSearchCities(value),
            ),
      );
    });
  }
}
