import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../logic/chat_users/chat_users_provider.dart';



ScrollController useChtUsersImmScrollController(WidgetRef ref, {int oldLength = 0}) {
  return use(_ScrollControllerHook(ref, oldLength: oldLength));
}

class _ScrollControllerHook extends Hook<ScrollController> {
  const _ScrollControllerHook(this.ref, {required this.oldLength});
  final int oldLength;
  final WidgetRef ref;

  @override
  _ScrollControllerHookState createState() => _ScrollControllerHookState();
}

class _ScrollControllerHookState
    extends HookState<ScrollController, _ScrollControllerHook> {
  late final ScrollController _controller;

  @override
  void initHook() {
    _controller = ScrollController();
    _controller.addListener(() async {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels != 0) {
          if (hook.oldLength ==
              hook.ref.read(resolverChatUsers).users?.length) {
            if (hook.ref.read(resolverChatUsers).isFetching) {
              return;
            }
            hook.ref.read(resolverChatUsers.notifier).isPaginate();
            hook.ref.read(resolverChatUsers.notifier).loadMoreStores();
          }
        }
      }
    });
  }

  @override
  ScrollController build(BuildContext context) => _controller;

  @override
  void dispose() => _controller.dispose();
}
