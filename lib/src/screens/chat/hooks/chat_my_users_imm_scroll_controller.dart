import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../logic/chat_provider.dart';

ScrollController useChtUsersMyImmScrollController(WidgetRef ref,
    {int oldLength = 0}) {
  return use(_ScrollControllerHook(ref, oldLength: oldLength));
}

class _ScrollControllerHook extends Hook<ScrollController> {
  const _ScrollControllerHook(this.ref, {required this.oldLength});
  final int oldLength;
  final WidgetRef ref;

  @override
  _ScrollControllerHookState createState() => _ScrollControllerHookState();
}

class _ScrollControllerHookState
    extends HookState<ScrollController, _ScrollControllerHook> {
  late final ScrollController _controller;

  @override
  void initHook() {
    _controller = ScrollController();
    _scrollListener();
  }

  void _scrollListener() {
    _controller.addListener(() async {
      if (_controller.offset >= _controller.position.maxScrollExtent &&
          !_controller.position.outOfRange) {
        if (hook.oldLength == hook.ref.read(resolverChatService).chats?.length) {
          if (hook.ref.read(resolverChatService).isFetching) {
            return;
          }
          hook.ref.read(resolverChatService.notifier).isPaginate();
          hook.ref.read(resolverChatService.notifier).loadMoreChatMessages();
        }
      }
    });
  }

  @override
  ScrollController build(BuildContext context) => _controller;

  @override
  void dispose() {
    _controller.dispose();
  }
}
