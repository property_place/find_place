import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../view/Widgets/boockeing_annimation.dart';

BookingPageAnimationController useChatAnimationController(
    {required AnimationController buttonController,
    required AnimationController contentController}) {
  return use(_ScrollControllerHook(
      buttonController: buttonController,
      contentController: contentController));
}

class _ScrollControllerHook extends Hook<BookingPageAnimationController> {
  const _ScrollControllerHook(
      {required this.buttonController, required this.contentController});
  final AnimationController buttonController;
  final AnimationController contentController;

  @override
  _ScrollControllerHookState createState() => _ScrollControllerHookState();
}

class _ScrollControllerHookState
    extends HookState<BookingPageAnimationController, _ScrollControllerHook> {
  late final BookingPageAnimationController _controller;

  @override
  void initHook() {
    _controller = BookingPageAnimationController(
      buttonController: hook.buttonController,
      contentController: hook.contentController,
    );
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      await _controller.buttonController.forward();
      await _controller.buttonController.reverse();
      await _controller.contentController.forward();
    });
  }

  @override
  BookingPageAnimationController build(BuildContext context) => _controller;

  @override
  void dispose() => _controller.dispose();
}
