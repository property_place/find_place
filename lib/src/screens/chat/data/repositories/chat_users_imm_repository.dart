import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../core/excepction/exception.dart';
import '../../../../core/global/environment.dart';
import '../models/chat_users_imm/chat_users_imm_model.dart';

abstract class IChatUsersImmRepository {
  Future<ChatUsersImmModel> getImmovableInquiryChatUsers(int page, int limit);
}

class ChatUsersImmRepository extends IChatUsersImmRepository {
  ChatUsersImmRepository({required this.client, required this.storage});
  final http.Client client;
  final FlutterSecureStorage storage;

  @override
  Future<ChatUsersImmModel> getImmovableInquiryChatUsers(
      int page, int limit) async {
    try {
      final token = await storage.read(key: 'token');

      final url = Uri.parse(
          '${EnvironmentApi.linkApi}/chat/getImmInQuiryChatUsers/$page/$limit');
      final response = await client.get(
        url,
        headers: {
          "Accept": "application/json",
          'Content-Type': 'application/json',
          'x-token': token.toString()
        },
      );
      if (response.statusCode == 200) {
        return ChatUsersImmModel.fromJson(json.decode(response.body));
      } else {
        return ChatUsersImmModel.fromJson(json.decode(response.body));
      }
    } on CustomExceptionResponse {
      rethrow;
    }
  }
}
