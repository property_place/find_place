import 'package:ionicons/ionicons.dart';

import '../models/menu_item_chat/menu_item_chat.dart';

// ignore: avoid_classes_with_only_static_members
class DataMenuItems {
  static const List<MenuItemChat> itemsMenuRolBlazeFirt = [
    itemEnableContract,
  ];
  static const List<MenuItemChat> itemsMenuRolBlazeSecond = [
    itemsSetting
  ];

  static const List<MenuItemChat> itemsMenuRolSpark = [
    itemContract,
    itemsSetting
  ];

  static const MenuItemChat itemContract =
      MenuItemChat(text: 'contrato', icon: Ionicons.receipt_outline);

  static const MenuItemChat itemEnableContract =
      MenuItemChat(text: 'habilitar contrato', icon: Ionicons.receipt_outline);

  static const MenuItemChat itemsSetting =
      MenuItemChat(text: 'Configuraciones', icon: Ionicons.settings_outline);
}
