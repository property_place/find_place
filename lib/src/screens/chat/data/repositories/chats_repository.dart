import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../core/excepction/exception.dart';
import '../../../../core/global/environment.dart';
import '../models/chat_immovables/chat_immovables_model.dart';
import '../models/chat_messages_model.dart';
import '../models/chat_model.dart';

abstract class IChatsRepository {
  Future<ChatModel> getChat(int page, int limit, String categoryId,
      List<Map<String, dynamic>> filters);

  Future<ChatImmovablesModel> getChatForImmovable(int page, int limit);

  Future<ChatMessageResponseModel> getChatMessages(
      int page, int limit, String receiverID);
}

class ChatsRepository extends IChatsRepository {
  ChatsRepository({required this.client, required this.storage});
  final http.Client client;
  final FlutterSecureStorage storage;

  @override
  Future<ChatModel> getChat(int page, int limit, String categoryId,
      List<Map<String, dynamic>> filters) async {
    final url = Uri.parse(
        '${EnvironmentApi.linkApi}/immovables/getImmovables/$page/$limit/$categoryId');
    final response = await client.post(url,
        headers: {
          "Accept": "application/json",
          'Content-Type': 'application/json',
        },
        body: json.encode(filters, toEncodable: (value) => value.toList()));
    if (response.statusCode == 200) {
      return ChatModel.fromJson(json.decode(response.body));
    } else if (response.statusCode == 500) {
      final data = ChatModel.fromJson(json.decode(response.body));
      throw Exception(data.error);
    } else {
      throw Exception('Error');
    }
  }

  @override
  Future<ChatImmovablesModel> getChatForImmovable(
      int page, int limit) async {
    try {
      final token = await storage.read(key: 'token');

      final url = Uri.parse(
          '${EnvironmentApi.linkApi}/chat/getChatForImmovable/$page/$limit');
      final response = await client.get(
        url,
        headers: {
          "Accept": "application/json",
          'Content-Type': 'application/json',
          'x-token': token.toString()
        },
      );
      if (response.statusCode == 200) {
        return ChatImmovablesModel.fromJson(json.decode(response.body));
      } else {
        return ChatImmovablesModel.fromJson(json.decode(response.body));
      }
    } on CustomExceptionResponse {
      rethrow;
    }
  }

  @override
  Future<ChatMessageResponseModel> getChatMessages(
      int page, int limit, String receiverID) async {
    try {
      final token = await storage.read(key: 'token');

      final url = Uri.parse(
          '${EnvironmentApi.linkApi}/chat/getChats/$page/$limit/$receiverID');
      final response = await client.get(
        url,
        headers: {
          "Accept": "application/json",
          'Content-Type': 'application/json',
          'x-token': token.toString()
        },
      );
      if (response.statusCode == 200) {
        return ChatMessageResponseModel.fromJson(json.decode(response.body));
      } else {
        return ChatMessageResponseModel.fromJson(json.decode(response.body));
      }
    } on CustomExceptionResponse {
      rethrow;
    }
  }
}
