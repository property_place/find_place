import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../core/excepction/exception.dart';
import '../../../../core/global/environment.dart';
import '../../../auth/data/models/user_model.dart';
import '../models/chat_users/chat_users_model.dart';

abstract class IChatListUserRepository {
  Future<ChatUsersModel> getChatUserForImmovable(
      int page, int limit, String immovableId);

  Future<ChatUsersModel> giveAccessToTheContract(
      String contractId, String selectedUserId);
}

class ChatListUserRepository extends IChatListUserRepository {
  ChatListUserRepository({required this.client, required this.storage});
  final http.Client client;
  final FlutterSecureStorage storage;

  @override
  Future<ChatUsersModel> getChatUserForImmovable(
      int page, int limit, String immovableId) async {
    try {
      final token = await storage.read(key: 'token');

      final url = Uri.parse(
          '${EnvironmentApi.linkApi}/chat/getChatUserForImmovable/$page/$limit/$immovableId');
      final response = await client.get(
        url,
        headers: {
          "Accept": "application/json",
          'Content-Type': 'application/json',
          'x-token': token.toString()
        },
      );
      if (response.statusCode == 200) {
        return ChatUsersModel.fromJson(json.decode(response.body));
      } else {
        return ChatUsersModel.fromJson(json.decode(response.body));
      }
    } on CustomExceptionResponse {
      rethrow;
    }
  }

  @override
  Future<ChatUsersModel> giveAccessToTheContract(
      String contractId, String selectedUserId) async {
    try {
      final token = await storage.read(key: 'token');

      final url =
          Uri.parse('${EnvironmentApi.linkApi}/chat/giveAccessToTheContract');
      final response = await client.post(
        url,
        headers: {
          "Accept": "application/json",
          'Content-Type': 'application/json',
          'x-token': token.toString()
        },
        body: {
          'contractId' : contractId,
          'userId': selectedUserId
        }
      );
      if (response.statusCode == 200) {
        return ChatUsersModel.fromJson(json.decode(response.body));
      } else {
        return ChatUsersModel.fromJson(json.decode(response.body));
      }
    } on CustomExceptionResponse {
      rethrow;
    }
  }
}
