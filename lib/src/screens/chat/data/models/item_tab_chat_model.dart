import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class ItemTab {
  final String title;
  final IconData? icon;
  ItemTab({required this.title,  this.icon});
}

final tabs = [
  ItemTab(title: 'CHAT', ),
  ItemTab(title: 'CONTRACTO', icon: Ionicons.contract),
];