import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../auth/data/models/user_model.dart';

part 'chat_immovables_model.g.dart';

@JsonSerializable()
class ChatImmovablesModel extends Equatable {
  ChatImmovablesModel({
    required this.ok,
    this.docs,
    this.totalPages,
    this.page,
    this.error,
  });

  factory ChatImmovablesModel.fromJson(Map<String, dynamic> json) =>
      _$ChatImmovablesModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatImmovablesModelToJson(this);

  final bool ok;
  final List<DocChatImmovablesModel>? docs;
  final int? totalPages;
  final int? page;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocChatImmovablesModel extends Equatable {
  DocChatImmovablesModel({
    required this.immovableId,
    required this.state,
    required this.description,
    required this.outsideFrontImage,
    this.quantity = '1',
  });

  factory DocChatImmovablesModel.fromJson(Map<String, dynamic> json) =>
      _$DocChatImmovablesModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocChatImmovablesModelToJson(this);

  @JsonKey(name: 'immovable_id')
  final String immovableId;
  final String state;
  final String description;
  @JsonKey(name: 'outside_front_image')
  final String outsideFrontImage;
  final String quantity;

  @override
  List<Object?> get props => [];
}
