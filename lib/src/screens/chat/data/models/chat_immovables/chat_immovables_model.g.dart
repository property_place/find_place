// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_immovables_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatImmovablesModel _$ChatImmovablesModelFromJson(Map<String, dynamic> json) =>
    ChatImmovablesModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>?)
          ?.map(
              (e) => DocChatImmovablesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int?,
      page: json['page'] as int?,
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatImmovablesModelToJson(
        ChatImmovablesModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'totalPages': instance.totalPages,
      'page': instance.page,
      'error': instance.error,
    };

DocChatImmovablesModel _$DocChatImmovablesModelFromJson(
        Map<String, dynamic> json) =>
    DocChatImmovablesModel(
      immovableId: json['immovable_id'] as String,
      state: json['state'] as String,
      description: json['description'] as String,
      outsideFrontImage: json['outside_front_image'] as String,
      quantity: json['quantity'] as String? ?? '1',
    );

Map<String, dynamic> _$DocChatImmovablesModelToJson(
        DocChatImmovablesModel instance) =>
    <String, dynamic>{
      'immovable_id': instance.immovableId,
      'state': instance.state,
      'description': instance.description,
      'outside_front_image': instance.outsideFrontImage,
      'quantity': instance.quantity,
    };
