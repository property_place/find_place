import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../auth/data/models/user_model.dart';
import '../../../../auth/data/models/user_p_model.dart';

part 'chat_users_model.g.dart';

@JsonSerializable()
class ChatUsersModel extends Equatable {
  ChatUsersModel({
    required this.ok,
    this.docs,
    this.totalPages,
    this.page,
    this.error,
  });

  factory ChatUsersModel.fromJson(Map<String, dynamic> json) =>
      _$ChatUsersModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatUsersModelToJson(this);

  final bool ok;
  final List<DocChatUsersModel>? docs;
  final int? totalPages;
  final int? page;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocChatUsersModel extends UserPModel {
  DocChatUsersModel(
      {required this.userId,
      required this.email,
      this.image,
      required this.name,
      required this.pLastName,
      this.sLastName,
      required this.online,
      required this.lastMessage,
      required this.immovableId,
      required this.chatId,
      required this.time,
      required this.rol})
      : super(
          userId: userId,
          email: email,
          image: image,
          name: name,
          pLastName: pLastName,
          sLastName: sLastName,
        );

  factory DocChatUsersModel.fromJson(Map<String, dynamic> json) =>
      _$DocChatUsersModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocChatUsersModelToJson(this);

  @JsonKey(name: 'user_id')
  final String userId;
  final String email;
  final String? image;
  final String name;
  @JsonKey(name: 'p_lastname')
  final String pLastName;
  @JsonKey(name: 's_lastname')
  final String? sLastName;
  final bool online;
  @JsonKey(name: 'last_message')
  final String? lastMessage;
  @JsonKey(name: 'immovable_id')
  final String immovableId;
  @JsonKey(name: 'chat_id')
  final String chatId;
  final String time;
  final String rol;
  @override
  List<Object?> get props => [];
}
