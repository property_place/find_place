// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_users_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatUsersModel _$ChatUsersModelFromJson(Map<String, dynamic> json) =>
    ChatUsersModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>?)
          ?.map((e) => DocChatUsersModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int?,
      page: json['page'] as int?,
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatUsersModelToJson(ChatUsersModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'totalPages': instance.totalPages,
      'page': instance.page,
      'error': instance.error,
    };

DocChatUsersModel _$DocChatUsersModelFromJson(Map<String, dynamic> json) =>
    DocChatUsersModel(
      userId: json['user_id'] as String,
      email: json['email'] as String,
      image: json['image'] as String?,
      name: json['name'] as String,
      pLastName: json['p_lastname'] as String,
      sLastName: json['s_lastname'] as String?,
      online: json['online'] as bool,
      lastMessage: json['last_message'] as String?,
      immovableId: json['immovable_id'] as String,
      chatId: json['chat_id'] as String,
      time: json['time'] as String,
      rol: json['rol'] as String,
    );

Map<String, dynamic> _$DocChatUsersModelToJson(DocChatUsersModel instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'email': instance.email,
      'image': instance.image,
      'name': instance.name,
      'p_lastname': instance.pLastName,
      's_lastname': instance.sLastName,
      'online': instance.online,
      'last_message': instance.lastMessage,
      'immovable_id': instance.immovableId,
      'chat_id': instance.chatId,
      'time': instance.time,
      'rol': instance.rol,
    };
