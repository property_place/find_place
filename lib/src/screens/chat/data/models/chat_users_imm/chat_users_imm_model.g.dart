// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_users_imm_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatUsersImmModel _$ChatUsersImmModelFromJson(Map<String, dynamic> json) =>
    ChatUsersImmModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>?)
          ?.map((e) => DocChatUsersImmModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int?,
      page: json['page'] as int?,
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatUsersImmModelToJson(ChatUsersImmModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'totalPages': instance.totalPages,
      'page': instance.page,
      'error': instance.error,
    };

DocChatUsersImmModel _$DocChatUsersImmModelFromJson(
        Map<String, dynamic> json) =>
    DocChatUsersImmModel(
      userId: json['user_id'] as String,
      email: json['email'] as String,
      outsideFrontImage: json['outside_front_image'] as String,
      image: json['image'] as String?,
      name: json['name'] as String,
      pLastName: json['p_lastname'] as String,
      sLastName: json['s_lastname'] as String?,
      online: json['online'] as bool,
      lastMessage: json['last_message'] as String?,
      immovableId: json['immovable_id'] as String,
      chatId: json['chat_id'] as String,
      immovable: json['immovable'] as String,
      state: json['state'] as String,
      rol: json['rol'] as String,
      time: json['time'] as String,
    );

Map<String, dynamic> _$DocChatUsersImmModelToJson(
        DocChatUsersImmModel instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'email': instance.email,
      'outside_front_image': instance.outsideFrontImage,
      'image': instance.image,
      'name': instance.name,
      'p_lastname': instance.pLastName,
      's_lastname': instance.sLastName,
      'online': instance.online,
      'last_message': instance.lastMessage,
      'immovable_id': instance.immovableId,
      'immovable': instance.immovable,
      'state': instance.state,
      'rol': instance.rol,
      'chat_id': instance.chatId,
      'time': instance.time,
    };
