import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../auth/data/models/user_model.dart';
import '../../../../auth/data/models/user_p_model.dart';

part 'chat_users_imm_model.g.dart';

@JsonSerializable()
class ChatUsersImmModel extends Equatable {
  ChatUsersImmModel({
    required this.ok,
    this.docs,
    this.totalPages,
    this.page,
    this.error,
  });

  factory ChatUsersImmModel.fromJson(Map<String, dynamic> json) =>
      _$ChatUsersImmModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatUsersImmModelToJson(this);

  final bool ok;
  final List<DocChatUsersImmModel>? docs;
  final int? totalPages;
  final int? page;
  final ErrorModel? error;
  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocChatUsersImmModel extends UserPModel {
  DocChatUsersImmModel(
      {required this.userId,
      required this.email,
      required this.outsideFrontImage,
      this.image,
      required this.name,
      required this.pLastName,
      this.sLastName,
      required this.online,
      this.lastMessage,
      required this.immovableId,
      required this.chatId,
      required this.immovable,
      required this.state,
      required this.rol,
      required this.time})
      : super(
            email: email,
            name: name,
            pLastName: pLastName,
            sLastName: sLastName,
            userId: userId,
            image: image);

  factory DocChatUsersImmModel.fromJson(Map<String, dynamic> json) =>
      _$DocChatUsersImmModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocChatUsersImmModelToJson(this);

  @JsonKey(name: 'user_id')
  final String userId;
  final String email;
  @JsonKey(name: 'outside_front_image')
  final String outsideFrontImage;
  final String? image;
  final String name;
  @JsonKey(name: 'p_lastname')
  final String pLastName;
  @JsonKey(name: 's_lastname')
  final String? sLastName;
  final bool online;
  @JsonKey(name: 'last_message')
  final String? lastMessage;
  @JsonKey(name: 'immovable_id')
  final String immovableId;
  final String immovable;
  final String state;
  final String rol;
  @JsonKey(name: 'chat_id')
  final String chatId;
  final String time;

  @override
  List<Object?> get props => [];
}
