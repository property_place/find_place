import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'chat_model.g.dart';

@JsonSerializable()
class ChatModel extends Equatable {
  const ChatModel(
      {required this.ok,
      required this.docs,
      required this.totalPages,
      required this.page,
      required this.error});

  factory ChatModel.fromJson(Map<String, dynamic> json) =>
      _$ChatModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatModelToJson(this);

  final bool ok;
  final List<DocChatModel> docs;
  final int page;
  final int totalPages;
  final String error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocChatModel extends Equatable {
  DocChatModel({
    required this.userId,
    required this.message,
    required this.time,
  });

  factory DocChatModel.fromJson(Map<String, dynamic> json) =>
      _$DocChatModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocChatModelToJson(this);

  @JsonKey(name: 'user_id')
  final String userId;
  final String message;
  final String time;
  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class ChatlistUsersItemModel extends Equatable {
  const ChatlistUsersItemModel(
      {required this.userId,
      required this.email,
      required this.image,
      required this.name,
      required this.plastname,
      required this.onLine});

  factory ChatlistUsersItemModel.fromJson(Map<String, dynamic> json) =>
      _$ChatlistUsersItemModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatlistUsersItemModelToJson(this);

  @JsonKey(name: 'user_id')
  final String userId;
  final String email;
  final String image;
  final String name;
  @JsonKey(name: 'p_lastname')
  final String plastname;
  @JsonKey(name: 'online')
  final bool onLine;

  @override
  List<Object?> get props => [];
}
