// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_messages_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatMessageResponseModel _$ChatMessageResponseModelFromJson(
        Map<String, dynamic> json) =>
    ChatMessageResponseModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>?)
          ?.map((e) =>
              DocChatMessageRespModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int?,
      page: json['page'] as int?,
      error: json['error'] == null
          ? null
          : ErrorModel.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ChatMessageResponseModelToJson(
        ChatMessageResponseModel instance) =>
    <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'totalPages': instance.totalPages,
      'page': instance.page,
      'error': instance.error,
    };

DocChatMessageRespModel _$DocChatMessageRespModelFromJson(
        Map<String, dynamic> json) =>
    DocChatMessageRespModel(
      chatId: json['chat_id'] as String,
      receiverID: json['receiver_id'] as String,
      senderID: json['sender_id'] as String,
      message: json['message'] as String,
      time: json['time'] as String,
      timeMsg: json['time_msg'] as String,
      immovableID: json['immovable_id'] as String,
    );

Map<String, dynamic> _$DocChatMessageRespModelToJson(
        DocChatMessageRespModel instance) =>
    <String, dynamic>{
      'chat_id': instance.chatId,
      'receiver_id': instance.receiverID,
      'sender_id': instance.senderID,
      'message': instance.message,
      'time': instance.time,
      'time_msg': instance.timeMsg,
      'immovable_id': instance.immovableID,
    };
