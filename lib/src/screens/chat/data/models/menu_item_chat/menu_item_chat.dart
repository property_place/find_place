import 'package:flutter/material.dart';

class MenuItemChat {
  const MenuItemChat({required this.text, required this.icon});
  final String text;
  final IconData icon;
}