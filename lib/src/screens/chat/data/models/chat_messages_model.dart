import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../auth/data/models/user_model.dart';

part 'chat_messages_model.g.dart';

@JsonSerializable()
class ChatMessageResponseModel extends Equatable {
  ChatMessageResponseModel(
      {required this.ok, this.docs, this.totalPages, this.page, this.error});
  factory ChatMessageResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ChatMessageResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChatMessageResponseModelToJson(this);

  final bool ok;
  final List<DocChatMessageRespModel>? docs;
  final int? totalPages;
  final int? page;
  final ErrorModel? error;

  @override
  List<Object?> get props => [];
}

@JsonSerializable()
class DocChatMessageRespModel extends Equatable {
  DocChatMessageRespModel(
      {required this.chatId,
      required this.receiverID,
      required this.senderID,
      required this.message,
      required this.time,
      required this.timeMsg,
      required this.immovableID,
      this.animation});
      
  factory DocChatMessageRespModel.fromJson(Map<String, dynamic> json) =>
      _$DocChatMessageRespModelFromJson(json);
  Map<String, dynamic> toJson() => _$DocChatMessageRespModelToJson(this);

  @JsonKey(name: 'chat_id')
  final String chatId;
  @JsonKey(name: 'receiver_id')
  final String receiverID;
  @JsonKey(name: 'sender_id')
  final String senderID;
  final String message;
  final String time;
  @JsonKey(name: 'time_msg')
  final String timeMsg;
  @JsonKey(name: 'immovable_id')
  final String immovableID;
  final AnimationController? animation;

  @override
  List<Object?> get props => [];
}
