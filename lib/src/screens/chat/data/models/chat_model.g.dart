// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatModel _$ChatModelFromJson(Map<String, dynamic> json) => ChatModel(
      ok: json['ok'] as bool,
      docs: (json['docs'] as List<dynamic>)
          .map((e) => DocChatModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalPages: json['totalPages'] as int,
      page: json['page'] as int,
      error: json['error'] as String,
    );

Map<String, dynamic> _$ChatModelToJson(ChatModel instance) => <String, dynamic>{
      'ok': instance.ok,
      'docs': instance.docs,
      'page': instance.page,
      'totalPages': instance.totalPages,
      'error': instance.error,
    };

DocChatModel _$DocChatModelFromJson(Map<String, dynamic> json) => DocChatModel(
      userId: json['user_id'] as String,
      message: json['message'] as String,
      time: json['time'] as String,
    );

Map<String, dynamic> _$DocChatModelToJson(DocChatModel instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'message': instance.message,
      'time': instance.time,
    };

ChatlistUsersItemModel _$ChatlistUsersItemModelFromJson(
        Map<String, dynamic> json) =>
    ChatlistUsersItemModel(
      userId: json['user_id'] as String,
      email: json['email'] as String,
      image: json['image'] as String,
      name: json['name'] as String,
      plastname: json['p_lastname'] as String,
      onLine: json['online'] as bool,
    );

Map<String, dynamic> _$ChatlistUsersItemModelToJson(
        ChatlistUsersItemModel instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'email': instance.email,
      'image': instance.image,
      'name': instance.name,
      'p_lastname': instance.plastname,
      'online': instance.onLine,
    };
